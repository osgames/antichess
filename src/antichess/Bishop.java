/*
 AntiChess, an antichess game

 Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
 (For copyright info please see COPYRIGHT file under the base directory)
 
 For bugs, contact info:
 barisy@mit.edu or hongping@mit.edu
*/
package antichess;


import java.util.*;
import java.lang.*;

/**
 * This class contains the rules that apply to Bishop.
 **/

public class Bishop extends Piece{
  /** Creates a new piece.
   * @param letter the character representation of the piece:
   * @param board the board the piece will be assigned to
   * @param row starting row of piece
   * @param col starting row of piece
   * @param color color
   * @requires letter = {P | p | K | k | N | n | R | r | Q | q | B | b}
   **/
  public Bishop (char letter, Board b, int col, int row, int color){
    super (Piece.BISHOP, letter, b, col, row, color);
    int[] myDCol = { -1, -1,  1,  1};
    int[] myDRow = { -1,  1, -1,  1};
    dcol = myDCol;
    drow = myDRow;
    slide = true;
    offsets = 4;
  }

  /**
   * Creates a copy to a new board
   */
  public Piece copyPiece (Board b){
    Piece p = new Bishop (this.getChar(), b, this.getCol(), this.getRow(), this.getColor());
    p.setLastLocation (this.getLastCol(), this.getLastRow());
    p.setMoved (this.getMoved());
    return p;
  }
  
  /** overrides Piece.canAttackLocation **/
  public boolean canAttackLocation (int targetCol, int targetRow){

    // If cylinder, we cannot really calculate diagonal
    if (this.getBoard().getBoardType() == Board.CYLINDER)
      return super.canAttackLocation (targetCol, targetRow);

    // If not cylinder, dx = dy means same diagonal
    int rowDif = Math.abs(this.getRow() - targetRow);
    int colDif = Math.abs(this.getCol() - targetCol);

    // Must be same diagonal
    if (rowDif != colDif)
      return false;
    return super.canAttackLocation (targetCol, targetRow);
  }
}
