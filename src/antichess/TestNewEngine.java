/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;
import junit.framework.*;
import java.io.*;
import java.util.*;

/** Tests NewEngine
 */
public class TestNewEngine
  extends TestCase{
//   String testfilesFile = "src/test/tests";
//   String testfilesDir = "src/test/";
  String testfilesFile = "src/test/newEngineTests";
  String testfilesDir = "src/test/";
    Map boardTable=new Hashtable();  
  public TestNewEngine(String name) {
    super(name);
  }


  // HELPER FUNCTIONS

  /** Converts a string of line into a List of sorted strings
   **/
  private List linesToSortedList (String s){
    StringTokenizer st = new StringTokenizer (s, "\n");
    List l = new ArrayList ();
    while (st.hasMoreElements()){
      String str  = (String) st.nextElement();
      if (str.length()>0)
	if(str.charAt(str.length()-1) == ';')
	  str = str.substring (0, str.length()-1);
      l.add (str);
    }
    Collections.sort (l);
    //System.out.println (l);
    return l;
  }
  
  // FILE FUNCTION

  /** This function only returns all text before the first ";"
   *  and sets comments to contain all text after the first ";"
   **/
  
  private String fileRead(String filename, boolean readAll) {
    if (filename == null)
      throw new RuntimeException("No file specified");


    String answer = new String();

    try {
      BufferedReader in = new BufferedReader(new FileReader(filename));
      // read each line until the end of the file and parse it into the script
      for (String line = in.readLine(); line != null; line = in.readLine()) {
	
	answer += line;

	if (line.length() > 0)
	  if (line.charAt (line.length()-1) == ';' && !readAll){
	    break;
	  }

	answer += "\n";


      }
      
    }
    catch (Exception e) {
      throw new RuntimeException("File not accessible\n" + filename + "\n" + e);
    }
    
    //System.out.println ("File read:\n" + answer);
    return answer;
  }
  
  protected void setUp(){
  }


  /**
   * Tests evaluator
   **/
  public void testEvaluator(){

    StringTokenizer st1 = new StringTokenizer (fileRead (testfilesFile, false), "\n");
    while (st1.hasMoreElements()){
      String filename = (String) st1.nextElement();
      System.out.println ("------- TESTING ENGINE ------");
      System.out.println ("------- Reading " + filename + " ------");
      
      String filecontents = fileRead (testfilesDir + filename, true);
      StringTokenizer st = new StringTokenizer (filecontents, ";");
      System.out.println ("Testing " + filename);

      String boardinfo = (String) st.nextElement();
      System.out.println ("file contents : \n" + boardinfo);
      if (!st.hasMoreElements())
	throw new RuntimeException ("Nothing found after first ;");

      String alphaStr = (String) st.nextElement();
      alphaStr = alphaStr.substring(1); // get rid ofpreceeding \n
      int alpha = Integer.parseInt (alphaStr);
      System.out.println ("Alpha: "  + alpha);

      String betaStr = (String) st.nextElement();
      betaStr = betaStr.substring(1); // get rid ofpreceeding \n
      int beta = Integer.parseInt (betaStr);
      System.out.println ("beta: "  + beta);
      
      String depthStr = (String) st.nextElement();
      depthStr = depthStr.substring(1); // get rid ofpreceeding \n
      int depth = Integer.parseInt (depthStr);
      System.out.println ("depth: "  + depth);
      
      String qualityStr = (String) st.nextElement();
      qualityStr = qualityStr.substring(1); // get rid ofpreceeding \n
      System.out.println ("Quality string: "  + qualityStr);
      int quality = Integer.parseInt(qualityStr);
      
      String expStr = (String) st.nextElement();
      String expected = expStr.substring(1); 
      System.out.println ("Expected: " + expected);
      
      GameController gc = new GameController (testfilesDir + filename); 
      Board b = gc.getBoard();
      int nextTurn = gc.getNextTurn();
      System.out.println ("Switched: " + b.getSpecial(Piece.WHITE)
			  + " " + b.getSpecial(Piece.BLACK));
      System.out.println ("Board:\n" + b);

      if (depth == 1) {
	System.out.println ("Testcase skipped");
	continue;
      }

      Move move = NewEngine.alphaBetaHash(b, boardTable,depth, nextTurn, alpha, beta);
      System.out.println ("Actual value: "  + move);

      assertTrue ("newEngine test failed", (new Move(expected)).equals (move));
    }
  }

  // Tell JUnit what order to run the tests in
  public static Test suite()
  { 
    TestSuite suite = new TestSuite(); 
    return suite;
  }
}
