/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import java.util.*;
/**
 * Piece represents a piece in the antichess game. 
 *
 * @specfield last-piece-moved: boolean // whether a 
 * @specfield has-moved: boolean
 * @specfield last-pos: [row, col]
 * @specfield cur-pos: [row, col]
 * @specfield piece-type: King | Knight | Bishop | Queen | Pawn | Rook
 * @specfield color: Black | White
 * @specfield board: Board // the board the piece is on
 */
public abstract class Piece{


  /**********************************************************/
  /**********************************************************/
  // GENERAL FIELDS FOR PIECES
  /**********************************************************/
  /**********************************************************/

 
  //PRIVATE FIELDS
  
  private Board board;               //Board that the piece is on
  private int col; //Current col of the piece 
  private int row; //Current row of the piece 
  private int last_col;    //Last col of the piece
  private int last_row;    //Last row of the piece
  private boolean  moved; // if the piece has moved
  private int color;      // the color of the piece  
  private int type;       // pieceType of the piece
  private char letter;    // letter representation of the piece
  
  //PUBLIC CONSTANTS

  /** Strings for the player color **/
  public static String[] playerColorString = {"White", "Black", "Red", "Purple"};
  /** 
   * Constant for the white color 
   */
  public static final int WHITE=0;
  
  /** 
   * Constant for the color black 
   */
  public static final int BLACK=1;
  /** 
   * Constant for the color black 
   */
  public static final int RED=2;
  /** 
   * Constant for the color black 
   */
  public static final int BLUE=3;
 
  /**
   * Constant for first color index
   */
  public static final int FIRSTCOLOR=WHITE;
 
 
  /**
   * Constant for last color index
   */

  public static int LASTCOLOR = BLACK;  // This assumes two player game
  //public static final int LASTCOLOR=BLACK;

     
  /**
   *
  /** 
   * Constant value for the pieceType Pawn
   **/
  public static  final int PAWN=0;
  
  /** 
   * Constant value for the pieceType Rook
   **/
  public static  final int ROOK=1;
  /** 
   * Constant value for the pieceType Knight
   **/
  public static  final int KNIGHT=2;
  
  /** 
   * Constant value for the pieceType Bishop
   **/
  public static  final int BISHOP=3;
  /** 
   * Constant value for the pieceType Queen
   **/
  public static  final int QUEEN=4;
  
  /** 
   * Constant value for the piecetype King
   **/
  public static  final int KING=5;
  
  /**
   * Constant value for the pieceType Empty
   */
  public static  final int EMPTY=6;
  
  /** 
   * Constant value for the first piece index
   **/
  public static  final int FIRSTPIECE=PAWN;
  
  /** 
   * Constant value for the last piece index
   **/
  public static  final int LASTPIECE=KING;
  /** 
   * Constant value for the number of pieces
   **/
  public static final int PIECECOUNT=LASTPIECE-FIRSTPIECE+1;
  /** 
   * Constant value for the value of pieces
   **/
  public static final int[] PIECEVALUE={15,50,30,30,90};
  
  //PRIVATE CONSTANTS
  private static final int COLORMASK=1;
  
  /**********************************************************/
  /**********************************************************/
  // CONSTRUCTOR
  /**********************************************************/
  /**********************************************************/
  

  /**
   * Takes in a piece type and board and assigns the current piece to it.
   * @param type piecetype
   * @param letter character representing piece
   * @param board board the piece is on
   * @param row starting row of piece 
   * @param col starting row of piece
   * @param color color
   * @requires type = {Piece.PAWN | Piece.BISHOP | Piece.KNIGHT | 
   * Piece.ROOK | Piece.QUEEN |Piece.KING|Piece.EMPTY } &&
   * letter = {P | p | K | k | N | n | R | r | Q | q | B | b} &&
   * MINROW <=row <=MAXROW && MINCOL<= col <=MAXCOL 
   * 
   * <b>NOTE: has to be taken using row and col. 
   * They start from MINROW, which is 0 generally)</b>
   **/
  public Piece (int type, char letter, Board b, int col, int row, int color){
    this.type = type;
    this.letter = letter;
    //if (b == null)
    //  throw new RuntimeException ("Board is null");
    this.board = b;
    this.col = col;
    this.row = row;
    this.last_col = col;
    this.last_row = row;
    this.color = color;

    //System.out.println ("Adding piece: " + letter + " at col " + col + " row " + row);
    if (b!=null)
      b.registerPiece (col, row, this);
  }

  /**
   * This constructor is only for empty pieces.
   * @param type piecetype
   * @param letter character representing piece
   * @requires type = Piece.EMPTY
   **/
  //    public Piece (int type, char letter, int col, int row){
  //      this.type = type;
  //      this.letter = letter;
  //      this.row = row;
  //      this.col = col;
  //    }

  /**
   * Creates a copy to a new board
   * To be overriden by each class
   */
  public Piece copyPiece (Board b){
    throw new RuntimeException ("copyPiece should be overriden");
  }
  /**********************************************************/
  /**********************************************************/
  // OBSERVERS
  /**********************************************************/
  /**********************************************************/
  /**Gets if the piece has been moved
   * @returns true if the piece has been moved
   */
  public boolean getMoved(){
    return(moved);
  }
  
  /** Gets the last location column of the piece
   * See the game file format for more details about this field.
   * @returns a column
   **/
  public int getLastCol(){
    
    return last_col;
  }
  
  /** Gets the last location row of the piece
   * See the game file format for more details about this field.
   * @returns a row
   **/
  public int getLastRow(){
    
    return last_row;
  }
  
  /** Gets the current column of the piece
   * @returns a column
   **/
  public int getCol(){
    return col;
  }
  
  /** Gets the current row of the piece
   * @returns a row
   **/
  public int getRow(){
    return row;
  }
  
  /**Gets the color of the piece 
   * @returns {Piece.WHITE | Piece.BLACK}
   */
  public int getColor(){
    return color;
  }
  
  /**Gets the type of the piece 
   * @returns {Piece.PAWN | Piece.BISHOP | Piece.KNIGHT | 
   * Piece.ROOK | Piece.QUEEN |Piece.KING | Piece.EMPTY }
   */
  public int getType(){
    return type;
  }
  
  /** Gets the opponent color,
   *  checking LASTCOLOR to see if it is a 2player game or 4.
   * @requires color == {Piece.WHITE | Piece.BLACK} | RED/BLUE if 4player
   * @return {Piece.WHITE | Piece.BLACK} | RED/BLUE if 4player
   **/
  static public int getOpponentColor (int color){
    if (LASTCOLOR == BLACK) // 2-player
      return Piece.COLORMASK-color;
    else {
      if (color == WHITE){
	return RED;
      } else if (color == RED) {
	return BLACK;
      } else if (color == BLACK) {
	return BLUE;
      } else if (color == BLUE) {
	return WHITE;
      }
      throw new RuntimeException ("Invalid color in getOpponentColor: " + color);

    }
    //return Piece.COLORMASK-color;
  }

  /** Gets the opponent color of a piece
   * @returns {Piece.WHITE | Piece.BLACK}
   **/
  public int getOpponentColor (){
    return Piece.getOpponentColor (this.color);
    //return Piece.COLORMASK-color;
  }

  /** Gets the character representing the piece
   * @returns letter representation corresponding to the pieceType
   **/
  public char getChar(){ 
    return letter;
  }
  /** Gets the board the Piece is on
   * @returns board the piece is on
   **/
  public Board getBoard(){
    return board;
  }

  /** String representation
   **/
  public String toString (){
    Piece piece = this;
    return ("piece-info: " + piece.getChar() + "\n" +
	    "currow: " + piece.getRow() + " " +
	    "curcol: " + piece.getCol() + "\n" +
	    "lastrow: " + piece.getLastRow() + " " +
	    "lastcol: " + piece.getLastCol() + "\n" +
	    "moved: " + piece.getMoved() + " " +
	    "");
    
  }

  /**Adds empty pieces to the rest of the board
   * @effects adds new Empty pieces to the rest of the board.
   * @requires topology has to be zero-based
   * board!=null
   * @throws RuntimeException if board==null
   */
  public static void addEmpty(Board board){
    
    for (int i=board.MINCOL;i<=board.MAXCOL;i++)
      for (int j=board.MINROW;j<=board.MAXROW;j++)
	if (board.getUsable(i,j) && board.getPieceAt(i,j)==null)
	  new EmptyPiece(board, i,j);
  }


  /**********************************************************/
  /**********************************************************/
  // MUTATORS
  /**********************************************************/
  /**********************************************************/

  /** Sets the board for this piece
   **/
  public void setBoard (Board b){
    this.board = b;
  }
  
  /**Set if the piece has moved
   * @modifies this
   * @effects sets has-moved as bool
   */
  public void setMoved(boolean bool){
    this.moved=bool; 
  }
   
  /** Sets the location of a piece
   * @requires a location | 1 <= row, col <= Board.numberOfRows/Cols
   * @modifies this
   * @effects sets the location of a piece
   **/
  public void setLocation(int col, int row){
    this.col=col;
    this.row=row;

  }


  /**  
   * Sets the last location of a piece
   * @requires a location | 1 <= row, col <= Board.numberOfRows/Cols && moved == true
   * @modifies this
   * @effects sets the location of a piece
   **/
  public void setLastLocation(int col, int row){
    this.last_row=row;
    this.last_col=col;

  }

  /**Sets the color of the piece 
   * @requires color = {Piece.WHITE | Piece.BLACK}
   * @modifies this
   * @effects sets color of the piece to be color
   */
  public void setColor(int color){
    this.color=color;
  }


  /** Checks for equality. <br>
   * Overrides Object.equals
   * @requires o instanceof Piece 
   * @returns true if object is same as this
   **/
  public boolean equals (Object o){
    if (o == null) return false;
    if (o instanceof Piece)
      return equals ((Piece) o);
    else
      return false;
  }

  public boolean equals (Piece other){
    if (other == null) return false;
    return (other.getType()==this.type
	    && other.getColor()==this.color
	    && other.getMoved()==this.moved
	    && other.getRow()==this.row
	    && other.getCol()==this.col
	    && other.getLastRow()==this.last_row
	    && other.getLastCol()==this.last_col
	    && other.getBoard()==this.board);
  }




  /**********************************************************/
  /**********************************************************/
  // MOVE GENERATION AND RULES FOR PIECES
  /**********************************************************/
  /**********************************************************/

  // FIELDS

  /** whether multiple steps are allowed*/
  protected boolean slide; 

  /** number of directions the piece can move*/
  protected int offsets; 

  /** col vector offset **/
  protected int[] dcol;
  /** row vector offset **/
  protected int[] drow;

  // METHODS
  
  /**********************************************************/
  /**********************************************************/
  // END GAME CONDITIONS
  /**********************************************************/
  /**********************************************************/

  /** Returns if the king is the only piece left for a side
   * @requires board!=null, nextturn = {WHITE | BLACK}
   **/
  static private boolean onlyKingLeft (Board b, int nextTurn){
    for (int i = FIRSTPIECE; i <= LASTPIECE; i ++){
      if (i == KING)
	continue;
      if (b.getAliveCount (nextTurn, i) > 0)
	return false;
    }
    return true;
  }

  /**
   * Returns if a game is over given a board,
   * with nextTurn being the next player to move
   * Note that this method will recompute all the valid moves in order
   * to determine whether the game is over or not.
   * To prevent recomputation, use getValidMoves, store the returned list
   * and pass it into isGameOver(Board b, int nextTurn, List validMoves)
   *@requires board!= null, nextturn = {WHITE | BLACK}
   *@returns true/false
   */
  static public boolean isGameOver(Board b, int nextTurn){
    boolean debug = false;
    
    boolean tie = isTie(b, nextTurn);
    boolean win = isWin(b, nextTurn);
    if (debug) System.out.println ("Tie: " + tie);
    if (debug) System.out.println ("win: " + win);
    return (isTie (b, nextTurn) ||
	    isWin (b, nextTurn));
  }
  
  /**
   * Returns if a game is over given a board,
   * the nextTurn being the next player to move
   * and the list of valid moves.
   *@requires board!= null&&  nextturn = {WHITE | BLACK} &&
   * validMoves is a list of valid moves returned by getValidMoves
   *@returns true/false
   */
  static public boolean isGameOver(Board b, int nextTurn, List validMoves){
    boolean debug = false;
    if (debug) System.out.println ("isGAmeOVER!");

    boolean tie = isTie(b, nextTurn, validMoves);
    boolean win = isWin(b, nextTurn, validMoves);

    if (debug) System.out.println ("Tie: " + tie);
    if (debug) System.out.println ("win: " + win);
    return (tie ||
	    win);
  }
  
  /**
   * Returns if a game is draw given a board,
   with nextTurn being the next player to move
   * Note that this method will recompute all the valid moves in order
   * to determine whether the game is a tie or not.
   * To prevent recomputation, use getValidMoves, store the returned list
   * and pass it into isTie(Board b, int nextTurn, List validMoves)
   *@requires board!= null && isGameOver == true
   *@returns true/false
   */
  static public boolean isTie(Board b, int nextTurn){
    boolean debug = false;
    
    // Check that nextturn is not in win, and that
    // has no more moves
    if (debug) System.out.println ("is tie possible moves: " /*+ getValidMoves(b, nextTurn)*/);
    return (!isWin(b, nextTurn) &&
	    getValidMoves(b, nextTurn).isEmpty());
  }
  
  /**
   * Returns if a game is draw given a board,
   * the nextTurn being the next player to move
   * and the list of valid moves.
   *@requires board!= null&&  nextturn = {WHITE | BLACK} &&
   * validMoves is a list of valid moves returned by getValidMoves
   *@returns true/false
   */
  static public boolean isTie(Board b, int nextTurn, List validMoves){
    boolean debug = false;
    
    // Check that nextturn is not in win, and that
    // has no more moves
    if (debug) System.out.println ("possible moves: " + validMoves);
    return (!isWin(b, nextTurn, validMoves) &&
	    validMoves.isEmpty());
  }

  /**
   * Returns if a particular side has won.
   * Note that this method will recompute all the valid moves in order
   * to determine whether the game is a win or not.
   * To prevent recomputation, use getValidMoves, store the returned list
   * and pass it into isWin(Board b, int nextTurn, List validMoves)
   *@requires board != null && king is on board
   *@returns true/false
   */
  static public boolean isWin(Board b, int nextTurn){
    boolean debug = false;
    // Check if all pieces lost except king
    if (onlyKingLeft(b, nextTurn))
      return true;

    // check if king is checkmated
    if (debug) System.out.println ("iswin possible moves: \n");
    if (getValidMoves(b,nextTurn).isEmpty() &&
	kingInCheck(b, nextTurn))
      return true;

    return false;
	
  }
  
  /**
   * Returns if a particular side has won, given a board,
   * the nextTurn being the next player to move
   * and the list of valid moves.
   *@requires board!= null&&  nextturn = {WHITE | BLACK} &&
   * validMoves is a list of valid moves returned by getValidMoves
   *@returns true/false
   */
  static public boolean isWin(Board b, int nextTurn, List validMoves){

    // Check if all pieces lost except king
    if (onlyKingLeft(b, nextTurn))
      return true;

    // check if king is checkmated
    if (validMoves.isEmpty() &&
	kingInCheck(b, nextTurn))
      return true;

    return false;
	
  }
  
  /**********************************************************/
  /**********************************************************/
  // MOVE GENERATION / CHECKING METHODS
  /**********************************************************/
  /**********************************************************/

  static public boolean kingInCheck (Board b, int color){
    // Check if king is in check
    Piece king = b.getKing(color);
    if (king == null)
      return false;
    return((King)king).inCheck();
  }

  /** Finds all legal moves given a board and a color to move.
   * @requires b!=null color = {WHITE | BLACK}
   * @returns a List of legal moves
   **/
  static public List getValidMoves (Board b, int color){
    boolean debug = false;
    List captureMoves = new ArrayList();
    List nonCaptureMoves = new ArrayList();
    List tmp;

    if (b== null)
      throw new RuntimeException ("Board is null!");

    
    // Iterate through all pieces

    if (debug)System.out.println("Iterating through pieces... color " + color + "\n"+b);
    for (int i = Piece.FIRSTPIECE; i <= Piece.LASTPIECE; i++){
      int pieceCount = b.getPieceCount (color, i);
      for (int j = 0; j < pieceCount; j++){
	if (debug)System.out.println ("getting piece at " +  i + " " + j );
	Piece pieces[][] = b.getPiecesOnBoard(color);
	Piece p = pieces[i][j];
	
	// Check if it is a piece
	if (p == null)
	  continue;

	p.getPossibleMoves (captureMoves, nonCaptureMoves);
      }
    }

    if (debug)System.out.println ("Done iterating through pieces\n" + b);


    if (debug)System.out.println ("Removing check moves from capture moves");
    removeCheckMoves (b, captureMoves);
    if (debug) System.out.println ("getValidMoves: after removeCheckMoves:" + captureMoves);
    if (debug) System.out.println ("after removecheck: \n" + b);
    if (!captureMoves.isEmpty())
      return captureMoves;

    if (debug)System.out.println ("Removing check moves from non capture moves");
    nonCaptureMoves.addAll (getPossibleSwitchMoves(b, color));
    removeCheckMoves (b, nonCaptureMoves);
    return nonCaptureMoves;
  }

  
  /** Finds all legal moves, including switch moves in both directions
   *  eg: both a1-a2 and a2-a1 will be included.
   * @requires b!=null 
   * @returns a List of legal moves
   **/
  static public List getAllValidMoves(Board b, int color){
    boolean debug = false;
    List captureMoves = new ArrayList();
    List nonCaptureMoves = new ArrayList();
    List tmp;

    if (b== null)
      throw new RuntimeException ("Board is null!");

    
    // Iterate through all pieces

    if (debug)System.out.println("Iterating through pieces... color " + color + "\n"+b);
    for (int i = Piece.FIRSTPIECE; i <= Piece.LASTPIECE; i++){
      int pieceCount = b.getPieceCount (color, i);
      for (int j = 0; j < pieceCount; j++){
	if (debug)System.out.println ("getting piece at " +  i + " " + j );
	Piece pieces[][] = b.getPiecesOnBoard(color);
	Piece p = pieces[i][j];
	
	// Check if it is a piece
	if (p == null)
	  continue;

	p.getPossibleMoves (captureMoves, nonCaptureMoves);
      }
    }

    if (debug)System.out.println ("Done iterating through pieces\n" + b);


    if (debug)System.out.println ("Removing check moves from capture moves");
    removeCheckMoves (b, captureMoves);
    if (debug) System.out.println ("getValidMoves: after removeCheckMoves:" + captureMoves);
    if (debug) System.out.println ("after removecheck: \n" + b);
    if (!captureMoves.isEmpty())
      return captureMoves;

    if (debug)System.out.println ("Removing check moves from non capture moves");
    nonCaptureMoves.addAll (getPossibleSwitchMoves(b, color));

    // add in the reversed switch moves
    List switchMoves = new ArrayList();
    List reversedSwitchMoves = new ArrayList();
    extractSwitchMoves (nonCaptureMoves, switchMoves, reversedSwitchMoves);
    nonCaptureMoves.addAll (reversedSwitchMoves);

    // remove any moves that result in a check
    removeCheckMoves (b, nonCaptureMoves);
    return nonCaptureMoves;

  }

  /** Finds all legal moves given a board and a color to move,
   *  EXCLUDING switch moves.
   * @requires b!=null color = {WHITE | BLACK}
   * @returns a List of legal moves
   **/
  static public List getStandardValidMoves(Board b, int color){
    boolean debug = false;
    List captureMoves = new ArrayList();
    List nonCaptureMoves = new ArrayList();
    List tmp;

    if (b== null)
      throw new RuntimeException ("Board is null!");

    // Iterate through all pieces

    if (debug)System.out.println("Iterating through pieces...\n"+b);
    for (int i = Piece.FIRSTPIECE; i <= Piece.LASTPIECE; i++){
      int pieceCount = b.getPieceCount (color, i);
      for (int j = 0; j < pieceCount; j++){
	if (debug)System.out.println ("getting piece at " +  i + " " + j );
	Piece pieces[][] = b.getPiecesOnBoard(color);
	Piece p = pieces[i][j];
	
	// Check if it is a piece
	if (p == null)
	  continue;

	p.getPossibleMoves (captureMoves, nonCaptureMoves);
      }
    }

    if (debug)System.out.println ("Done iterating through pieces\n" + b);


    if (debug)System.out.println ("Removing check moves from capture moves");
    removeCheckMoves (b, captureMoves);
    if (debug) System.out.println ("getValidMoves: after removeCheckMoves:" + captureMoves);
    if (debug) System.out.println ("after removecheck: \n" + b);
    if (!captureMoves.isEmpty())
      return captureMoves;

    if (debug)System.out.println ("Removing check moves from non capture moves");

    // WE DO NOT ADD ANY SWITCH MOVES HERE
    //nonCaptureMoves.addAll (getPossibleSwitchMoves(b, color));
    
    removeCheckMoves (b, nonCaptureMoves);
    return nonCaptureMoves;

  }
  
  /** Checks if a move is legal given a board and the color to move.
   *  If the move is a switch move, it will be modified to be in alphanumeric order.
   * Modifies move
   * @returns true | false
   **/
  static public boolean isValidMove(Board b, Move move, int color){
    boolean debug = false;
    if (debug)System.out.println("Checking move: " + move.getStartCol() + " " + move.getStartRow());

    // Check if the move string is a valid one
    if (move.toString().length()!=5)
      return false;

    int destRow = move.getDestRow();
    int destCol = move.getDestCol();
    int startRow = move.getStartRow();
    int startCol = move.getStartCol();
    
    if (destRow > b.MAXROW ||
	destRow < b.MINROW ||
	destCol > b.MAXCOL ||
	destCol < b.MINCOL)
      return false;
      
    if (startRow > b.MAXROW ||
	startRow < b.MINROW ||
	startCol > b.MAXCOL ||
	startCol < b.MINCOL)
      return false;

    if (debug)System.out.println ("Getting piece at " + startCol + " " + startRow);
    Piece p = b.getPieceAt (move.getStartCol(), move.getStartRow());
    if (p == null)
      return false;
    if (p.getType()==p.EMPTY)
      return false;
    if (p.getColor()!=color)
      return false;

    if (debug)
      System.out.println ("Getting valid moves... ");
    List moves = getValidMoves(b, color);
    
    //     System.out.println ("Valid moves: " + moves);
    //     Move m = (Move)moves.get(0);
    
    //     System.out.println (move);
    //     System.out.println (m);
    //     System.out.println (m.equals (move));
    //     System.out.println (moves.contains(move));
    //     System.out.println (moves.get(0).equals(move));
    //     System.out.println (((Move)moves.get(0)).equals(move));

    List switchMoves = new ArrayList();
    List reversedSwitchMoves = new ArrayList();
    extractSwitchMoves (moves, switchMoves, reversedSwitchMoves);
    if (switchMoves.contains(move))
      return true;
    if (reversedSwitchMoves.contains(move)){
      if (debug) System.out.println ("Move to be reversed: " + move);
      int index = reversedSwitchMoves.indexOf (move);
      Move m = (Move) switchMoves.get(index);
      move.copyFrom (m);
      if (debug) System.out.println ("Move reversed: " + move);
    }
    return moves.contains (move);
  }

  /** Extracts the switch moves and make a reversed copy of them **/
  static private void extractSwitchMoves (List moves, List switchMoves, List reversedSwitchMoves){
    int i;
    for (i=0;i<moves.size();i++){
      Move move = (Move) moves.get(i);
      if (move.getSpecial()){
	int destRow = move.getDestRow();
	int destCol = move.getDestCol();
	int startRow = move.getStartRow();
	int startCol = move.getStartCol();

	// add original move
	switchMoves.add (move);

	Move newmove = new Move(destCol, destRow, startCol, startRow);
	newmove.setSpecial (true);
	reversedSwitchMoves.add (newmove);
      }
    }
      
  }

  
  
  /** Find all possible (not necesarily legal) Moves a particular piece can make. 
   * The Moves are not necessarily legal. 
   * @param captureMoves the possible captureMoves will be stored here
   * @param nonCaptureMoves the possible nonCaptureMoves will be stored here
   * @requires non null
   * @effects captureMoves and nonCaptureMoves will contain the respective moves 
   **/
  private void getPossibleMoves (List captureMoves, List nonCaptureMoves){

    List switchMoves;
    
    //     System.out.println ("possiblemoves");
    //     System.out.println ((this instanceof King));
    
    this.getNormalMoves (captureMoves, nonCaptureMoves);

    nonCaptureMoves.addAll (this.getOtherMoves());

  }

  /** To be overridden by subclasses if any other special moves are
   * to be made 
   * @returns empty List**/
  public List getOtherMoves(){
    
    //     System.out.println ((this instanceof King));
    //     System.out.println ("piece.getothermoves");
    
    return new ArrayList();
  }

  /** Returns a list of valid switch moves.
   * @requires b!= null && color == {WHITE | BLACK}
   *  @return list of moves representing the switches the piece can make
   **/
  static public List getSwitchMoves (Board b, int color){
    List validSwitchMoves = Piece.getPossibleSwitchMoves (b, color);
    removeCheckMoves (b, validSwitchMoves);
    return validSwitchMoves;
  }


  /** Returns a list of possible switch (not necesarily legal) moves a piece can make
   * The Moves are not necessarily legal.
   * @requires b!= null && color == {WHITE | BLACK}
   *  @return list of moves representing the switches the piece can make
   **/
  static private List getPossibleSwitchMoves (Board b, int color){
    // First check if special move has been made.
    // Go through all locations on the board.
    // For each location, see if it contains a piece of the same color.
    // If so, add the move.

    // if 4-players, no special move
    if (b.getBoardType() == b.FOURPLAYERS){
      return new ArrayList();
    }
    
    int startRow;
    int startCol;
    int type;

    int destRow;
    int destCol;

    List specialMoves = new ArrayList();
    Move m;

    Piece p1, p2;

    if (b== null)
      throw new RuntimeException ("Board is null!");
    Piece pieces[][] = b.getPiecesOnBoard(color);
    
    
    // check if special move already has been used
    // if so, just return an empty arraylist
    if (b.getSpecial (color))
      return specialMoves;
    
    // Iterate through all pieces

    for (int i = Piece.FIRSTPIECE; i <= Piece.LASTPIECE; i++){
      int pieceCount = b.getPieceCount (color, i);
      if (pieces[i] == null) continue;
      for (int j = 0; j < pieceCount; j++){

	if (pieces[i][j] == null) continue;
	if (!nonEmptyPiece (pieces[i][j], color))
	  continue;
	p1 = pieces[i][j];
	
	//for (int ii = Piece.FIRSTPIECE; ii <= Piece.LASTPIECE; ii++){
	for (int ii = i; ii <= Piece.LASTPIECE; ii++){
	  int pieceCount2 = b.getPieceCount (color, ii);
	  if (pieces[ii] == null) continue;
      	  //for (int jj = 0; jj < pieceCount2; jj++){
	  int jjstart;
	  if (i ==ii)
	    jjstart=j+1;
	  else
	    jjstart=0;
	  for (int jj = jjstart; jj < pieceCount2; jj++){
	      
	    // System.out.println (ii + " " + jj);
	    if (pieces[ii][jj] == null) continue;

	    if (!nonEmptyPiece (pieces[ii][jj], color))
	      continue;
	    p2 = pieces[ii][jj];	

	    startRow = p1.getRow();
	    startCol = p1.getCol();
	    destRow = p2.getRow();
	    destCol = p2.getCol();

	    // different location
	    if (destRow != startRow || destCol != startCol){

	      // If the destination square contains a pawn,
	      // check that the starting square is not the first or
	      // last rank
	      
	      if (p2.getType() == Piece.PAWN){
		if ((startRow  == b.MINROW) || 
		    (startRow  == b.MAXROW)){
		  continue; // skip adding this move
		}
	      }
	      
	      // If the starting square contains a pawn,
	      // check that the destination square is not the first or
	      // last rank
	      if (p1.getType() == Piece.PAWN){
		if ((destRow == b.MINROW) || 
		    (destRow == b.MAXROW)){
		  continue; // skip adding this move
		}
	      }
	      
	      // Create and add the move

	      // switch move should be alphanumeric 
	      if (destCol > startCol ||
		  (destCol == startCol && destRow > startRow)){
		m = new Move (startCol, startRow, destCol, destRow);
	      } else {
		m = new Move (destCol, destRow, startCol, startRow);
	      }
	      m.setSpecial (true);
	      specialMoves.add (m);
	    }
	  }

	}
      }
    }

    return specialMoves;
  }

  /**
   * To check that a piece is not null, not empty, and of a required color
   **/
  static private boolean nonEmptyPiece (Piece p, int color){
    if (p == null)
      return false;

    if (p.getType() == Piece.EMPTY)
      return false;

    if (p.getColor() != color)
      return false;

    return true;
  }
  
  /** 
   * Gets possible normal (includes capture and noncapture, 
   * but not necessarily legal) moves. 
   * The Moves are not necessarily legal. 
   *  @param nonCaptureMoves = List of nonCaptureMoves found
   *  @param captureMoves = List of captureMoves found
   *  @requires non null
   *  @modifies moves, captureMoves
   *  @effects nonCaptureMoves will contain list of non-capture moves. 
   *           && captureMoves will contain a list of capture moves
   **/
  public void getNormalMoves (List captureMoves, List nonCaptureMoves){
    // This method makes use of the direction vectors dcol and drow.
    // The number of directions a piece can move is stored in the variable offsets.
    // Whether a piece can move more than one step at a time is indicated by the variable slide.
    // For each direction, we offset the starting location by the vector, 
    // and check if that location is empty.
    // If the location is empty, then this is a nonCaptureMove.
    // If the location has a piece of same color, we are blocked, and done for this direction.
    // If the location has a piece of another color, we can capture and this is a captureMove.
    // If the piece can slide, we will repeat until we encounter another piece in the direction.
    // Otherwise, we only need to check one step.

    boolean debug = false;
    
    Board b = this.board;
    int startRow = this.row;
    int startCol = this.col;
    int type = this.type;
    int color = this.color;

    int destRow;
    int destCol;

    int i;
    Piece p = null;
    Move m;


    // iterate through all possible directions
    for (i = 0; i < offsets; i++){

      // reset the destination row and column to the starting location of the piece
      destRow = startRow;
      destCol = startCol;

      if (debug) {
	System.out.println ("Checking startCol " + startCol);
	System.out.println ("Checking startRow " + startRow);
      }
      for (;;){

	// offset the destination row and column using the offset vector
	destRow += drow[i];
	destCol += dcol[i];

	// check if we are out of the board
	// 	if (destRow > b.MAXROW ||
	// 	    destRow < b.MINROW ||
	// 	    destCol > b.MAXCOL ||
	// 	    destCol < b.MINCOL)
	// 	  break;


	if (b.getBoardType() == b.CYLINDER){
	  // Cylinder board: we need to wrap around columns

	  if (destCol < b.MINCOL)
	    destCol += b.NUMCOLS;
	  if (destCol > b.MAXCOL)
	    destCol -= b.NUMCOLS;

	  if (destRow > b.MAXROW ||
	      destRow < b.MINROW ||
	      (destCol == startCol && destRow == startRow))
	    break;
	} else {
	  if (destRow > b.MAXROW ||
	      destRow < b.MINROW ||
	      destCol > b.MAXCOL ||
	      destCol < b.MINCOL)
	    break;
	}

	if (debug) {
	  System.out.println ("Checking destCol " + destCol);
	  System.out.println ("Checking destRow " + destRow);
	}
	p = b.getPieceAt(destCol, destRow);
	// check if the square is valid
	if (p == null) {
	  // continue;
	  break;
	}

	// Check if a piece is on the destination location
	if (p.getType() != Piece.EMPTY){
	  // location has a piece
	  // Get the piece

	  // Check if the piece is the same color
	  if (p.getColor() == color){
	    // If same color, we are blocked.
	    // Done with this direction as we are blocked

	  } else {
	    // If different color, we can capture.
	    // Add move to captureMoves
	    m = new Move(startCol, startRow, destCol, destRow);
	    m.setCapture(true);
	    captureMoves.add (m);
	    if (debug) System.out.println ("cap" + m);

	  }
	  // Since this location has a piece, we can either capture it if
	  // it is of a different color, or we are blocked if it is the
	  // same color. So we need not check further in this direction.
	  break;

	} else {

	  // location is empty
	  // Add move to nonCaptureMoves
	  m = new Move(startCol, startRow, destCol, destRow);
	  nonCaptureMoves.add (m);
	  if (debug) System.out.println ("nocap: " +m);
	}

	// see if multiple steps are allowed
	if (!slide){
	  break; // can only move one step, so exit
	}

      }
    }
    
  }

  /** 
   * Checks if a Piece can attack a particular location
   * @returns true if can attack, false otherwise. If the location contains
   * a friendly piece, returns false.
   **/
  public boolean canAttackLocation (int targetCol, int targetRow){
    Board b = this.board;
    int startRow = this.row;
    int startCol = this.col;
    int type = this.type;
    int color = this.color;

    int destRow;
    int destCol;
    int i;

    Piece p = null;
    Move m;


    // iterate through all possible directions
    for (i = 0; i < offsets; i++){

      // reset the destination row and column to the starting location of the piece
      destRow = startRow;
      destCol = startCol;

      for (;;){

	// offset the destination row and column using the offset vector
	destRow += drow[i];
	destCol += dcol[i];

	// Check if we are out of the board
	// 	if (destRow > Board.MAXROW ||
	// 	    destRow < Board.MINROW ||
	// 	    destCol > Board.MAXCOL ||
	// 	    destCol < Board.MINCOL)
	// 	  break;
	
	if (b.getBoardType() == b.CYLINDER){
	  // Cylinder board: we need to wrap around columns

	  if (destCol < b.MINCOL)
	    destCol += b.NUMCOLS;
	  if (destCol > b.MAXCOL)
	    destCol -= b.NUMCOLS;

	  if (destRow > b.MAXROW ||
	      destRow < b.MINROW ||
	      (destCol == startCol && destRow == startRow))
	    break;
	} else {
	  if (destRow > b.MAXROW ||
	      destRow < b.MINROW ||
	      destCol > b.MAXCOL ||
	      destCol < b.MINCOL)
	    break;
	}

	p = b.getPieceAt(destCol, destRow);
	// check if the square is valid
	if (p == null){
	  //continue;
	  break;
	}
	// Check if a piece is on the destination location
	if (p.getType() != Piece.EMPTY){
	  // location has a piece
	  // Get the piece

	  // Check if the piece is the same color
	  if (p.getColor() == color){
	    // If same color, we are blocked.
	    // Done with this direction as we are blocked

	  } else {
	    // If different color, we can capture.
	    // Check if it is the target location
	    if (destRow == targetRow && destCol == targetCol)
	      return true;

	  }
	  // Since this location has a piece, we can either capture it if
	  // it is of a different color, or we are blocked if it is the
	  // same color. So we need not check further in this direction.
	  break;

	} else {

	  // location is empty
	  // We can attack this location
	  if (destRow == targetRow && destCol == targetCol)
	    return true;
	}

	// see if multiple steps are allowed
	if (!slide){
	  break; // can only move one step, so exit
	}

      }
    }
    return false;
    
  }
  /** Removes illegal and duplicate moves from a List
   * @requires non null
   * @modifies moves
   * @effects removes illegal moves and duplicate moves
   **/
  static private void removeCheckMoves (Board b, List moves){

    boolean debug = false;
    int startRow;
    int startCol;
    int destRow;
    int destCol;
    int i;
    int color;

    Piece currentPiece, kingPiece;
    Move m;

    Board tmpBoard = null;
    Iterator iter = moves.iterator();
    Move move;
    Piece pieces[][] = null;
    //List tmp = new ArrayList();

    while (iter.hasNext()){
      if (debug) System.out.println (b);

      // Check for duplicates
      move = (Move)iter.next();
      //if (tmp.contains (move)){
      //iter.remove();
      //continue;
      //}

	

      //System.out.println (move);
      if (debug) System.out.println ("Start row " + move.getStartRow()+
				     "Start col " + move.getStartCol()+
				     "dest row " + move.getDestRow()+
				     "dest col " + move.getDestCol());
      
      if (debug) System.out.println ("removeCheckMoves " + move);
      
      // Create a copy of the board
      //tmpBoard = new Board (b);

      startRow = move.getStartRow();
      startCol = move.getStartCol();

      destRow = move.getDestRow();
      destCol = move.getDestCol();

      //currentPiece = tmpBoard.getPieceAt (startCol, startRow);
      currentPiece = b.getPieceAt (startCol, startRow);
      if (currentPiece == null){
	if (debug)System.out.println (startCol + " " + startRow);
      }
      color = currentPiece.getColor();
      //       currentPiece.makeGeneratedMove (move);
      Piece.makeGeneratedMove (b, move);
      if (debug) System.out.println ("made move:\n"+b);

      //move.resetModifiedPieceCount();

      // Get king and see if it is in check
      //pieces = tmpBoard.getPiecesOnBoard(color);
      pieces = b.getPiecesOnBoard(color);
      
      //if (debug)System.out.println ("Board: \n" + tmpBoard + "\n" + pieces);

      // no king
      //       if (pieces[Piece.KING] == null){

      // 	if (debug)System.out.println ("King array is null");

      // 	Piece.undoMove (b, move);
      // 	move.resetModifiedPieceCount();

      // 	tmp.add (move);
      // 	continue;
      //       }
      //kingPiece = pieces[Piece.KING][0];
      //kingPiece = tmpBoard.getKing(color);
      kingPiece = b.getKing(color);

      // If there is no king
      if (kingPiece == null) {

	if (debug)System.out.println ("King element is null");

	Piece.undoMove (b, move);
	// move.resetModifiedPieceCount();
	if (debug) System.out.println ("undo move\n"+b);
	//tmp.add (move);
	continue;
      }

      boolean result = (((King)kingPiece).inCheck());

      Piece.undoMove (b, move);
      if (debug) System.out.println ("undo move\n"+b);
      //    if (((King)kingPiece).inCheck()){
      if (result){
	// invalid move
	
	if (debug)System.out.println("King is in check");
	
	iter.remove();
      } else{
	
	if (debug)System.out.println ("King is not in check");
	//tmp.add (move);
	
      }
    }
    //System.out.println (b);
  }
  
  /**********************************************************/
  /**********************************************************/
  // MAKE MOVE METHODS
  /**********************************************************/
  /**********************************************************/

  /** Makes a given move on a board, modifying it, recomputing the validMoves.
   *  Note that this will recompute all the valid moves first.
   * Use getValidMoves, store the returned list, and then use
   * makeMove (Board b, Move m, List validMoves) to avoid recomputation
   * @returns true if move was made successfully, false if invalid move
   * @modifies b
   **/
  static public boolean makeMove (Board b, Move m){
    boolean debug = false;

    if (m==null || !m.isValid())
      return false;
    if (debug) System.out.println ("makingmove: " + m);
    if (debug) System.out.println ("In piece.makemove\n" + b);
    int startRow = m.getStartRow();
    int startCol = m.getStartCol();
    if (debug) System.out.println ("Getting piece at to make move: " + startCol + " " + startRow );
    Piece p = b.getPieceAt(startCol, startRow);
    if (p == null)
      return false;
    if (p.getType () == Piece.EMPTY)
      return false;
    if (debug) System.out.println ("piece.makemove .getvalidmoves");
    List validMoves = Piece.getAllValidMoves(b, p.getColor());
    // We need to get the piece again because undo move might have changed it
    if (debug) System.out.println ("gotten validmoves:\n" +b);
    p = b.getPieceAt(startCol, startRow);
    p.makeMove (m, validMoves);
    if (debug) System.out.println ("Piece.makeMove done:\n" +b);
    return true;
    
  }
  
  /** Makes a given move on a board, modifying it, without recomputing the validMoves.
   * @returns true if move was made successfully, false if invalid move
   * @modifies b
   **/
  static public boolean makeMove (Board b, Move m, List validMoves){

    if (m==null || !m.isValid())
      return false;
    
    int startRow = m.getStartRow();
    int startCol = m.getStartCol();
    Piece p = b.getPieceAt(startCol, startRow);
    if (p == null)
      return false;
    if (p.getType () == Piece.EMPTY)
      return false;
    if (!validMoves.contains(m))
      return false;
    p.makeMove (m, validMoves);
    return true;
    
  }

  /** Makes a move, recomputing the validMoves, DEPRECATED: DO NOT USE.
   *  Note that this will recompute all the valid moves first.
   * Use getValidMoves, store the returned list, and then use
   * makeMove (Move m, List validMoves) to avoid recomputation
   * @modifies this && this.board && m
   * @effects this.board is updated
   **/
  public void makeMove (Move m){
    if (1==1)
      throw new RuntimeException ("Not to use this");
    //System.out.println ("moving piece: \n" + this);
    //System.out.println ("Piece.makemove2 getValidMoves");
    List moves = Piece.getValidMoves(this.board, this.color);
    //System.out.println ("Done getting valid moves:\n" + this.board);
    int index = moves.indexOf (m);
    // Not a valid move
    if (index == -1)
      return ;
    
    //System.out.println ("Making move ...");
    //System.out.println ("Switched: " + board.getSpecial(WHITE)
    //			+ " " + board.getSpecial(BLACK));
    Move move = (Move) moves.get(index);

    m.info = move.info;
    //System.out.println ("doing move: " + m + m.info);
    //System.out.println ("generated move: " + move + move.info);
    //System.out.println ("moving piece: \n" + this);
    //System.out.println ("modified piece count: " +move.getModifiedPieceCount());
    // m.resetModifiedPieceCount();
    makeGeneratedMove (m);
    return ;


  }
  
  /** Makes a move, without recomputing the validMoves.
   * @modifies this && this.board
   * @effects this.board is updated
   **/
  public void makeMove (Move m, List validMoves){
    List moves = validMoves; //Piece.getValidMoves(this.board, this.color);
    int index = moves.indexOf (m);
    // Not a valid move
    if (index == -1)
      return ;
    
    //System.out.println ("Making move ...");
    //System.out.println ("Switched: " + board.getSpecial(WHITE)
    //			+ " " + board.getSpecial(BLACK));
    //System.out.println ("board: \n" + this.board);
    Move move = (Move) moves.get(index);
    m.info = move.info;
    // m.resetModifiedPieceCount();
    makeGeneratedMove (m);
    //System.out.println ("made move: \n" + this.board);
    return ;


  }

  /** Makes a computer generated move.
   * This move must be generated from getValidMoves or one of the
   * methods in Piece, and move has information about what kind of move it is.
   * The move from a human player should be passed into makeMove.**/
  static public void makeGeneratedMove (Board b, Move move){
    int startRow = move.getStartRow();
    int startCol = move.getStartCol();
    Piece p = b.getPieceAt(startCol, startRow);
    p.makeGeneratedMove (move);
  }
  
  /** Makes a computer generated move.
   * This move must be generated from getValidMoves or one of the
   * methods in Piece, and move has information about what kind of move it is.
   * The move from a human player should be passed into makeMove.
   * @requires move is a Piece.method generated move
   * @modifies this && this.board
   * @effects this.board is updated
   **/
  public void makeGeneratedMove (Move move){
    if (move.getNormal())
      makeNormalMove(move);
    else if (move.getSpecial())
      makeSwitchMove(move);
    else if (move.getCapture())
      makeCaptureMove(move);
    else 
      makeOtherMove(move);
  }
  /** Makes a move that is unique to a particular piece.
   * To be overridden by subclasses
   **/
  public void makeOtherMove (Move move){
    throw new RuntimeException ("Piece.makeOtherMove should be overridden by subclass");
  }

  /** Undos a move that is unique to a particular piece.
   * To be overridden by subclasses
   **/
  public void undoOtherMove (Move move){
    System.out.println ("move: " + move + "\n" + move.info);
    System.out.println ("Board:\n" + this.board);
    System.exit(0);
    throw new RuntimeException ("Piece.undoOtherMove should be overridden by subclass");
  }


  /** Makes a normal move
   * @modifies this && this.board && dummy piece
   * @effects this.board is updated
   **/
  private void makeNormalMove(Move move){
    // Make normal move simply swaps two pieces
    // For undo, we need to keep info for first piece
    // To undo, then swap them back, and restore first piece info
    
    //System.out.println ("Making normal move ...");

    // Keep track of backup information for undo move
    // 1. Store last pieces info
    move.setLastPieces (this.getBoard());

    // 2. Piece info
    move.storePieceInfo (this, 0);

    // 3. Make the swap
    swapPieces (move.getDestCol(), move.getDestRow());
  }

  /** Undos a normal move
   * @modifies this.board && pieces
   **/
  private void undoNormalMove (Move move){
    int startCol = move.getStartCol();
    int startRow = move.getStartRow();

    // 3. Swap back
    swapPieces (startCol, startRow);

    // 2. restore piece info
    move.restorePieceInfo (this, 0);

    // 1. restore last piece info
    move.restoreLastPieces (this.getBoard());
    
  }



  /** Makes a switch move
   * @modifies this && this.board
   * @effects this.board is updated
   **/
  private void makeSwitchMove (Move move){
    // Make switch move simply swaps two pieces
    // For undo, keep piece info for both pieces
    // To undo, swap them back, and restore piece info
    
    //System.out.println ("Making switch move ...");

    int destCol = move.getDestCol();
    int destRow = move.getDestRow();

    // Keep track of backup information for undo move
    // 1. Store last pieces info
    move.setLastPieces (this.getBoard());

    // 2. First piece
    move.storePieceInfo (this, 0);

    // Second piece
    Piece p2 = this.board.getPieceAt(destCol, destRow);
    move.storePieceInfo (p2, 1);

    // 3. Make the swap
    swapPieces (destCol, destRow);

    // 4. Set switched to be true on board
    getBoard().setSpecial (getColor(), true);
  }

  /** Undo a switch move
   * @modifies this.board && pieces
   **/
  private void undoSwitchMove (Move move){
    int startCol = move.getStartCol();
    int startRow = move.getStartRow();
    int destRow = move.getDestRow();
    int destCol = move.getDestCol();
    
    // 4. Set switched to be false
    getBoard().setSpecial (getColor(), false);
    
    // 3. Make the swap back
    swapPieces (startCol, startRow);

    // 2. Restore piece info for first piece
    move.restorePieceInfo (this, 0);
    
    // Second piece
    Piece p2 = this.board.getPieceAt(destCol, destRow);
    move.restorePieceInfo (p2, 1);

    
    // 1. restore last piece info
    move.restoreLastPieces (this.getBoard());
  }

  /** Makes a capture move
   * @modifies this && this.board
   * @effects this.board is updated
   **/
  public void makeCaptureMove (Move move){
    // To make a capture move, create Empty over Capture, then swap
    // For undo, keep reference to captured piece,
    // and piece-info of capturing piece.
    // To undo, swap Empty and Capturing back, register the Captured over Empty,
    // restore info for Capturing piece.
    
    //System.out.println ("Making capture move ...");

    Board b = board;
    
    int destRow = move.getDestRow();
    int destCol = move.getDestCol();
    
    // Keep track of backup information for undo move
    // 1. Store last pieces info
    move.setLastPieces (b);
    
    // 2. First piece info
    move.storePieceInfo (this, 0);

    // 3. Capture piece Reference
    move.modifiedPieces[0] = b.getPieceAt (destCol, destRow);
    
    // Creates a dummy piece at the location being captured
    Piece dummy = null;
    //System.out.println ("Creating empty piece");

     
    dummy = new EmptyPiece(b, destCol, destRow);
    
    
    // Sets the dummy piece to be at the target location
    //System.out.println ("Setting location of emptypiece");
    getBoard().setPieceAt (destCol, destRow, dummy);
    dummy.setLocation (destCol, destRow);
    
    // Zobrist delete captured piece
    Zobrist z = b.getZobrist();
    if (type!=Piece.EMPTY)
      z.deletePiece (b, type, color, col, row);
    
    // 4. Swap current piece with dummy
    swapPieces (destCol, destRow);
  }

  /** Undos a capture move
   * @modifies this, this.board && other pieces on board
   **/
  public void undoCaptureMove (Move move){
    int startCol = move.getStartCol();
    int startRow = move.getStartRow();
    int destRow = move.getDestRow();
    int destCol = move.getDestCol();
    Board b = this.getBoard();
    Piece captured = move.modifiedPieces[0];
    
    // 4. Swap back
    swapPieces (startCol, startRow);

    // 3. Register captured piece
    //b.registerPiece (destCol, destRow, move.modifiedPieces[0]);
    b.registerPiece (destCol, destRow, captured);

    // 2. Restore piece info for capturing piece
    move.restorePieceInfo (this, 0);
        
    // 1. restore last piece info
    move.restoreLastPieces (this.getBoard());
    
    // Zobrist add back captured piece
    Zobrist z = b.getZobrist();
    if (captured.type!=Piece.EMPTY)
      z.addPiece (b, captured.type, captured.color, captured.col, captured.row);
  }
  
  /** Swaps this piece with another piece at another location
   * @modifies this && this.board && dummy piece
   * @effects this.board is updated. this.lastPieceMoved is updated to this.
   *  piece info for involved pieces are changed as well.
   **/
  protected void swapPieces (int destCol, int destRow){
    boolean debug = false;
    
    // Swap position with the other piece at destination
    
    Board b = getBoard();
    
    // Zobrist
    Zobrist z = b.getZobrist();
    if (z==null)
      throw new RuntimeException ("Zobrist should not be null");
    
    int startRow = this.getRow();
    int startCol = this.getCol();
    
    if (debug) System.out.println ("swapping pieces at " + startCol + " " + startRow +
				   " " + destCol + " " + destRow);

    Piece startPiece, otherPiece;
    //startPiece = b.getPieceAt (startCol, startRow);
    startPiece = this;
    otherPiece = b.getPieceAt (destCol, destRow);

    // If either pieces are null, do nothing
    if (startPiece == null ||
	otherPiece == null) 
      return;

    // Zobrist delete pieces from original position
    if (type!=EMPTY)
      z.deletePiece (b, type, color, col, row);
    if (otherPiece.type!=EMPTY)
      z.deletePiece (b, otherPiece.type, otherPiece.color, otherPiece.col, otherPiece.row);

    // Put the pieces into the new positions
    b.setPieceAt (destCol, destRow, startPiece);
    b.setPieceAt (startCol, startRow, otherPiece);


    // Modify the piece info
    startPiece.setLastLocation (startCol, startRow);
    startPiece.setLocation (destCol, destRow);
    startPiece.setMoved (true);

    otherPiece.setLastLocation (destCol, destRow);
    otherPiece.setLocation (startCol, startRow);
    otherPiece.setMoved (true);

    b.setLastPieceMoved (this);
    if (otherPiece.getType() != Piece.EMPTY){
      b.setLastPieceMoved (this, otherPiece.getColor(), 1);
    }

    // Zobrist add pieces to new positions
    if (type!=EMPTY)
      z.addPiece (b, type, color, col, row);
    if (otherPiece.type!=EMPTY)
      z.addPiece (b, otherPiece.type, otherPiece.color, otherPiece.col, otherPiece.row);
    


  }

  /** Undos a move.
   * @requires move has been applied to the board in the last turn using
   * and main piece to undo is at destCol destRow
   * makeMove/makeGeneratedMove
   **/
  public static void undoMove(Board b, Move move /*m*/){
    int destRow = move.getDestRow();
    int destCol = move.getDestCol();
    Piece p = b.getPieceAt (destCol, destRow);
    
    if (move.getNormal())
      p.undoNormalMove(move);
    else if (move.getSpecial())
      p.undoSwitchMove(move);
    else if (move.getCapture())
      p.undoCaptureMove(move);
    else 
      p.undoOtherMove(move);


    // OLD VERSION OF MAKE MOVE
    
    //     Piece modifiedPieces[] = m.getModifiedPieces();
    //     int modifiedPieceCount = m.getModifiedPieceCount();
    //     Piece p; 
    //     int color=-1;
    //     // restore last piece moved for white
    //     p = m.getLastPiece(WHITE);
    //     if (p != null){
    //       //      p = p.copyPiece (b);
    //       p.setBoard (b);
    //     }
    //     b.setLastPieceMoved (p, WHITE);

    //     // restore last piece moved for black
    //     p = m.getLastPiece(BLACK);
    //     if (p != null){
    //       //      p = p.copyPiece (b);
    //       p.setBoard (b);
    //     }
    //     b.setLastPieceMoved (p, BLACK);
    
    //     for (int i = 0; i < modifiedPieceCount; i ++){

    //       // restore the modified pieces
    //       if (modifiedPieces[i]!=null) {
    // 	p = modifiedPieces[i].copyPiece (b);
    // 	if (color == -1)
    // 	  color = p.getColor();
    //       }
    //       else
    // 	p = null;
    //     }



    //     // restore whether the side has used special move
    //     if (m.getSpecial())
    //       b.setSpecial(color, false);
    //   }
  
  }

  /**********************************************************/
  /**********************************************************/
  // NORMAL CHESS
  /**********************************************************/
  /**********************************************************/

  /** Finds all Normal chess legal moves given a board and a color to move.
   * @requires b!=null color = {WHITE | BLACK}
   * @returns a List of legal moves
   **/
  static public List getValidMovesNormalChess(Board b, int color){
    boolean debug = false;
    List captureMoves = new ArrayList();
    List nonCaptureMoves = new ArrayList();
    List tmp;

    if (b== null)
      throw new RuntimeException ("Board is null!");

    
    // Iterate through all pieces

    if (debug)System.out.println("Iterating through pieces...\n"+b);
    for (int i = Piece.FIRSTPIECE; i <= Piece.LASTPIECE; i++){
      int pieceCount = b.getPieceCount (color, i);
      for (int j = 0; j < pieceCount; j++){
	if (debug)System.out.println ("getting piece at " +  i + " " + j );
	Piece pieces[][] = b.getPiecesOnBoard(color);
	Piece p = pieces[i][j];
	
	// Check if it is a piece
	if (p == null)
	  continue;

	p.getPossibleMoves (captureMoves, nonCaptureMoves);
      }
    }

    if (debug)System.out.println ("Done iterating through pieces\n" + b);

    // remove check moves in capturemoves
    removeCheckMoves (b, captureMoves);

    if (debug)System.out.println ("Removing check moves from non capture moves");
    // remove check moves in switch and non capture moves
    nonCaptureMoves.addAll (getPossibleSwitchMoves(b, color));
    removeCheckMoves (b, nonCaptureMoves);

    // combine capturemoves and non capturemoves
    captureMoves.addAll (nonCaptureMoves);
    return captureMoves;

  }

  /**
   * Returns if a game is over given a board,
   * with nextTurn being the next player to move
   * Note that this method will recompute all the valid moves in order
   * to determine whether the game is over or not.
   * To prevent recomputation, use getValidMoves, store the returned list
   * and pass it into isGameOver(Board b, int nextTurn, List validMoves)
   *@requires board!= null, nextturn = {WHITE | BLACK}
   *@returns true/false
   */
  static public boolean isGameOverNormalChess(Board b, int nextTurn){
    boolean debug = false;
    
    boolean tie = isTieNormalChess(b, nextTurn);
    boolean lose = isLoseNormalChess(b, nextTurn);
    if (debug) System.out.println ("Tie: " + tie);
    if (debug) System.out.println ("Lose: " + lose);
    return (isTieNormalChess (b, nextTurn) ||
	    isLoseNormalChess (b, nextTurn));
  }

  /**
   * Returns if a game is draw given a board,
   with nextTurn being the next player to move
   * Note that this method will recompute all the valid moves in order
   * to determine whether the game is a tie or not.
   * To prevent recomputation, use getValidMoves, store the returned list
   * and pass it into isTie(Board b, int nextTurn, List validMoves)
   *@requires board!= null && isGameOver == true
   *@returns true/false
   */
  static public boolean isTieNormalChess(Board b, int nextTurn){
    boolean debug = false;
    
    // Check that nextturn is not a lose, and that
    // has no more moves
    if (debug) System.out.println ("is tie possible moves: " /*+ getValidMoves(b, nextTurn)*/);
    return (!isLoseNormalChess(b, nextTurn) &&
	    getValidMovesNormalChess(b, nextTurn).isEmpty());
  }
  /**
   * Returns if a particular side has lost.
   * Note that this method will recompute all the valid moves in order
   * to determine whether the game is a loss or not.
   * To prevent recomputation, use getValidMoves, store the returned list
   * and pass it into isWin(Board b, int nextTurn, List validMoves)
   *@requires board != null && king is on board
   *@returns true/false
   */
  static public boolean isLoseNormalChess(Board b, int nextTurn){
    boolean debug = false;

    // check if king is checkmated
    if (debug) System.out.println ("islose possible moves: \n");
    if (getValidMoves(b,nextTurn).isEmpty() &&
	kingInCheck(b, nextTurn))
      return true;

    return false;
	
  }
}

