/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/

package antichess;

import java.util.*;

/**
 * Empty Piece
 **/

public class EmptyPiece extends Piece{
  public EmptyPiece (Board b, int col, int row){
    super (Piece.EMPTY, ' ', b, col, row, -1);
  }


  public EmptyPiece (char letter, Board b, int col, int row){
    super (Piece.EMPTY, letter, b, col, row, -1);
    //super (Piece.EMPTY, letter, b, col, row);
  }
//   public EmptyPiece (){
//     super (Piece.EMPTY, ' ');
//   }


  /**
   * Creates a copy to a new board
   */
  public Piece copyPiece (Board b){
    Piece p = new EmptyPiece (this.getChar(), b, this.getCol(), this.getRow());
    return p;
  }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   public boolean getMoved(){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//     //return false;      

//   }
  
  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   public int getLastCol(){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//     ////return 0;
//   }
  
  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   public int getLastRow(){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//     //return 0;
//   }
  
  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   public int getCol(){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//     //return 0;
//   }

  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   public int getRow(){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//     //return 0;
//   }
  
  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   public int getColor(){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//     //return 0;
//   }
  
  /** <b> DOES NOTHING **DONT USE**</b>
   */
  static public int getOpponentColor (int color){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
    //return 0;
  }

  /** <b> DOES NOTHING **DONT USE**</b>
   */
  public int getOpponentColor (){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
    //return 0;
  }

//   /** <b> DOES NOTHING **DONT USE**</b>
//    */
//   public char getChar(){ 
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//     //return (' ');
//  }

  /** <b> DOES NOTHING **DONT USE**</b>
   */
  public Board getBoard(){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
    //return null;
  }

  /*Overrides piece.tostring*/
  public String toString (){
    Piece piece = this;
    return ("EMPTY PIECE: " + piece.getChar() + "\n" +
	    "currow: " + piece.getRow() + " " +
	    "curcol: " + piece.getCol() + "\n" +
	    "lastrow: " + piece.getLastRow() + " " +
	    "lastcol: " + piece.getLastCol());
  }


  /**********************************************************/
  /**********************************************************/
  // MUTATORS
  /**********************************************************/
  /**********************************************************/
    

  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   public void setMoved(boolean bool){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//   }
   
  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   public void setLocation(int col, int row){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//   }

  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   public void setLastLocation(int col, int row){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//   }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   public void setColor(int color){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//   }

  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   public boolean equals (Object o){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//     //return false;
//   }

  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   public boolean equals (Piece other){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//     //return false;
//   }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   private void removeCheckMoves (List moves){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//   }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
  public void makeCaptureMove (Move move){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
  }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
  private void makeSwitchMove (Move move){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
  }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
  protected void swapPieces (int destCol, int destRow){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
  }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
  public void makeOtherMove (Move move){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
  }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
  private void makeNormalMove(Move move){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
  }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
  public void makeMove (Move move){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
  }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
  public boolean canAttackLocation (int targetCol, int targetRow){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
    //return false;
  }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
  public void getNormalMoves (List captureMoves, List nonCaptureMoves){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
  }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
  public List getOtherMoves(){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
    //return null;
  }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
//   private List getSwitchMoves (){
//     System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
//     System.out.println("THIS IS A NO NO");
//     throw new RuntimeException ("");
//     //return null;

//   }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
  private void getPossibleMoves (List captureMoves, List nonCaptureMoves){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
  }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
  public List getValidMoves(){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
    //return null;
  }


  /** <b> DOES NOTHING **DONT USE**</b>
   */
  public boolean isValidMove(Move move){
    System.out.println("WHAT ARE YOU DOING? YOU ARE ACCESSING TO EMPTY PIECE...");
    System.out.println("THIS IS A NO NO");
    throw new RuntimeException ("");
    //return false;
  }

}
