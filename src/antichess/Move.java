/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import java.util.*;

/**
 * <p>A move consists of a start and destination location, and information about
 * whether the move is a capture move, switch, en passant, a castling move, a
 * pawn moving 2 squares, or a promotion. Move is mutable.
 * A move can be undone using Piece.undoMove. The move must passed into undoMove
 * must be the same move pass into makeMove, because makeMove will store additional
 * information necessary for undo in the move after applying the move.
 * <p>
 * @specfield startCol : int 
 * @specfield startRow : int 
 * @specfield destCol : int 
 * @specfield destRow : int 
 * @specfield capture: boolean // whether move is a capture
 * @specfield castle : boolean // whether move is a castling
 * @specfield special: boolean // whether move is a special
 * @specfield ep     : boolean // whether move is an en passant
 * @specfield pawn   : boolean // whether move is a pawn moving 2 squares
 * @specfield promote: boolean // whether move is a promotion
 * @specfield normal: boolean // whether a move is none of the above
 **/
/*
 * DEPRECATED
 * @specfield startLocation: Location // starting location of a move
 * @specfield destLocation: Location // destination location of a move
 */

public class Move {

  /***************************************************************/
  /**
   * Bits for info:
   * 1: capture
   * 2: castle
   * 3: special
   * 4: ep
   * 5: pawn (DEPRECATED)
   * 6: promote
   **/
  /**********************************************************/
  // STATIC FIELDS
  /**********************************************************/

  static int maxPiecesModified = 4;
  static int captureMASK = 1;
  static int castleMASK = 1<<1;
  static int specialMASK = 1<<2;
  static int epMASK = 1<<3;
  static int pawnMASK = 1<<4;
  static int promoteMASK = 1<<5;
  static int MASK = ~0;

  /**********************************************************/
  // NONSTATIC FIELDS
  /**********************************************************/

  // BASIC MOVE INFO
  /* info about this move */
  public int info;

  /* starting row */
  private int startRow;
  /* starting col */
  private int startCol;
  /* dest row */
  private int destRow;
  /* dest col */
  private int destCol;

  // FIELDS FOR UNDO MOVE
  // Reference to removed pieces
  public Piece modifiedPieces[] = new Piece[maxPiecesModified];

  // Reference to last piece moved on board
  public Piece lastPieces[][] = new Piece[Piece.LASTCOLOR+1][Board.maxLastPieceMoved];

  // Piece info
  private boolean moved[] = new boolean[maxPiecesModified]; // has moved
  private int row[] = new int[maxPiecesModified]; // current row
  private int col[] = new int[maxPiecesModified]; // current col
  private int lastRow[] = new int[maxPiecesModified];
  private int lastCol[] = new int[maxPiecesModified];

  String string;
  


  //Location start, dest;

  /**********************************************************/
  /**********************************************************/
  // CONSTRUCTORS
  /**********************************************************/
  /**********************************************************/

  /** Creates a new move. This will make copies of the locations.
   * @requires string represntation xn-xn, 
   * "xn" | x = {a|b|c|d|e|f|g|h} && n = {1|2|3|4|5|6|7|8|9}
   * @modifies this
   * @effects creates a new move
   **/
  public Move (String str){
    boolean debug = false;

    if (str == null){
      setInvalid();
      return;
    }
    
    string = str;
    // check if board is 4player
    //    if (Piece.LASTCOLOR!=Piece.BLACK){
    if (str.length() > 5){
      if (debug) System.out.println ("4player move");
      StringTokenizer st = new StringTokenizer (str, "-");
      if (!st.hasMoreTokens()) {
	setInvalid();
	return;
      }

      // start
      String s = (String) st.nextToken();

      if (s.length()!=2 && s.length()!=3) {
	System.out.println ("invalid move: stringlengh error");
	setInvalid();
	return;
      }
      
      this.startRow = getRowFromString (s);
      this.startCol = getColFromString (s);

      if (debug){
	System.out.println ("Start string: " + s);
	System.out.println ("Start row: " + startRow);
	System.out.println ("Start col: " + startCol);
      }
	
      if (!st.hasMoreTokens()) {
	System.out.println ("invalid move: no more tokens");
	setInvalid();
	return;
      }

      // destination
      s = (String) st.nextToken();
      if (s.length()!=2 && s.length()!=3) {
	System.out.println ("invalid move: stringlength erro");
	setInvalid();
	return;
      }
      
      this.destRow = getRowFromString (s);
      this.destCol = getColFromString (s);

      if (debug){
	System.out.println ("dest string: " + s);
	System.out.println ("destrow: " + destRow);
	System.out.println ("destcol: " + destCol);
      }
      
      
    } else {

      if (str.length()!=5){
	setInvalid();
	return;
      }
      this.startRow = getRowFromString (str.substring (0, 2));
      this.startCol = getColFromString (str.substring (0, 2));
      this.destRow = getRowFromString (str.substring (3, 5));
      this.destCol = getColFromString (str.substring (3, 5));
    }
  }

  /* Makes coorindates of this move invalid */
  private void setInvalid(){
    this.startRow = -1;
    this.startCol =-1;
    this.destRow = -1;
    this.destCol = -1;
  }
  
  //public Move (String str){
  // this.start = new Location(str.substring (0, 2));
  //this.dest = new Location(str.substring (3, 5));
  //}

  /** Creates a new move from a string representation.
   * @modifies this
   * @effects creates a new move
   **/
  public Move (int startCol, int startRow, int destCol, int destRow){
    this.startRow = startRow;
    this.startCol = startCol;
    this.destRow = destRow;
    this.destCol = destCol;
    string = locationToString(startCol, startRow) + "-" + locationToString (destCol, destRow);
  }
  //public Move (Location start, Location dest){
  //this.start = new Location(start.getRow(), start.getCol());
  //this.dest = new Location(dest.getRow(), dest.getCol());;
  //}

  /** Creates a new move from a given move **/
  public Move (Move m){
    this.startRow = m.startRow;
    this.startCol = m.startCol;
    this.destRow = m.destRow;
    this.destCol = m.destCol;
    this.string = m.string;
  }

  /**********************************************************/
  /**********************************************************/
  // MUTATORS
  /**********************************************************/
  /**********************************************************/
  /** Copy values from a given Move **/
  public void copyFrom (Move m){
    this.startRow = m.startRow;
    this.startCol = m.startCol;
    this.destRow = m.destRow;
    this.destCol = m.destCol;
    this.string = m.string;
    this.info = m.info;
  }

  /** 
   * @modifies this
   * @effects set capture to x
   **/
  public void setCapture (boolean x){
    if (x)
      info |= captureMASK;
    else
      info &= (~captureMASK);

  }

  /** 
   * @modifies this
   * @effects set castle to x
   **/
  public void setCastle (boolean x){
    if (x)
      info |= castleMASK;
    else
      info &= (~castleMASK);
  }

  /**
   * @modifies this
   * @effects set special to x
   **/
  public void setSpecial (boolean x){
    if (x)
      info |= specialMASK;
    else
      info &= (~specialMASK);
  }

  /**
   * @modifies this
   * @effects set ep to x
   **/
  public void setEp (boolean x){
    if (x)
      info |= epMASK;
    else
      info &= (~epMASK);
  }
  
  /**
   * @modifies this
   * @effects set pawn to x
   **/
  public void setPawn (boolean x){
    if (x)
      info |= pawnMASK;
    else
      info &= (~pawnMASK);
  }
  
  /**
   * @modifies this
   * @effects set promote to x
   **/
  public void setPromote (boolean x){
    if (x)
      info |= promoteMASK;
    else
      info &= (~promoteMASK);
  }

  /* Sets the startRow and startCol */
  public void setStart (int col, int row){
    this.startCol = col;
    this.startRow = row;
  }


  /* Sets the destRow and destCol */
  public void setDest (int col, int row){
    this.destCol = col;
    this.destRow = row;
  }

  /**
   * @requires s!=null
   * @modifies this
   * @effects set start = s
   **/
  //public void setStart (Location s){
  //start = s;
  //}
  
  /**
   * @requires s!=null
   * @modifies this
   * @effects set dest = s
   **/
  //public void setDest (Location s){
  //dest = s;
  //}

  /** DEPRECATED Set last piece of color to be p
   * @requires color = {WHITE | BLACK}
   **/
//   public void setLastPiece (Piece p, int color){
//     if (p != null)
//       lastPieces[color ] = p.copyPiece(null);
//     else
//       lastPieces[color] = null;
//   }

  /** Retrieves and stores the last pieces information from a board
   * @requires board != null
  **/
  public void setLastPieces (Board b){
//     for (int i=Piece.FIRSTCOLOR; i<=Piece.LASTCOLOR; i++){
//       lastPieces[i] = b.getLastPieceMoved(i);
//     }
    for (int i=Piece.FIRSTCOLOR; i<=Piece.LASTCOLOR; i++){
      for (int j=0; j<Board.maxLastPieceMoved; j++)
	lastPieces[i][j] = b.getLastPieceMoved(i, j);
    }
    
  }


  /** Stores piece information of p at location i
   * @modifies this
   **/
  public void storePieceInfo (Piece p, int i){
    this.row[i] = (p.getRow());
    this.col[i] = (p.getCol());
    this.lastRow[i] = (p.getLastRow());
    this.lastCol[i] = (p.getLastCol());
    this.moved[i] = (p.getMoved());
  }

  /**********************************************************/
  /**********************************************************/
  // OBSERVERS
  /**********************************************************/
  /**********************************************************/

  public boolean isValid (){
    if (startRow < 0 || startCol < 0 || destRow < 0 || destCol < 0)
      return false;
    return true;
  }
  /** Restores the last pieces information to a board
   * @requires board != null
  **/
  public void restoreLastPieces (Board b){
//     for (int i=Piece.FIRSTCOLOR; i<=Piece.LASTCOLOR; i++){
//       b.setLastPieceMoved(lastPieces[i], i);
//     }
    for (int i=Piece.FIRSTCOLOR; i<=Piece.LASTCOLOR; i++){
      for (int j=0; j<Board.maxLastPieceMoved; j++)
	b.setLastPieceMoved(lastPieces[i][j], i, j);
      
    }
  }
  /** Restore piece information of p at location i
   * @modifies this
   **/
  public void restorePieceInfo (Piece p, int i){
    p.setLocation (col[i], row[i]);
    p.setLastLocation (lastCol[i], lastRow[i]);
    p.setMoved (moved[i]);
    
  }

//   public Piece getLastPiece (int color){
//     return lastPieces[color];
//   }
  public Piece[] getModifiedPieces (){
    return modifiedPieces;
  }
  
  
  /** @returns normal **/
  public boolean getNormal (){
    return (info == 0);
  }

  /**
   * @returns capture
   **/
  public boolean getCapture (){
    return (info & captureMASK) > 0;
  }

  /** 
   * @returns castle
   **/
  public boolean getCastle (){
    return (info &castleMASK) > 0;
  }

  /**
   * @returns special
   **/
  public boolean getSpecial (){
    return (info & specialMASK) > 0;
  }
  
  /**
   * @returns ep
   **/
  public boolean getEp (){
    return (info & epMASK) > 0;
  }
  
  /**
   * @returns pawn
   **/
  public boolean getPawn (){
    return (info & pawnMASK) > 0;
  }
  
  /**
   * @returns promote
   **/
  public boolean getPromote (){
    return (info & promoteMASK) > 0;
  }

  
  /**
   * @returns start
   **/
  //  public Location getStart (){
  //   return start;
  //}
  
  /**
   * @returns dest
   **/
  //public Location getDest (){
  //return dest;
  //}
	
  public int getStartRow(){
    return startRow;
  }

  public int getStartCol(){
    return startCol;
  }
  public int getDestRow(){
    return destRow;
  }
  public int getDestCol(){
    return destCol;
  }

  /**
   * @returns true if m is equal to this
   * Checks only start/dest row/col.
   **/
  public boolean equals (Move m){
    //return (m.getStart().equals(start)) && (m.getDest().equals(dest));
    return (startRow == m.getStartRow() &&
	    startCol == m.getStartCol() &&
	    destRow == m.getDestRow() &&
	    destCol == m.getDestCol() &&
	    string.equals (m.toString()));
  }

  public boolean equals (Object o){
    if (o instanceof Move)
      return equals((Move) o);
    else return false;
  }


  /**
   * @returns string represntation xn-xn, 
   * "xn" | x = {a|b|c|d|e|f|g|h} && n = {1|2|3|4|5|6|7|8|9}
   **/
  public String toString(){
    return string;
  }


  /**********************************************************/
  /**********************************************************/
  // OTHER HELPER METHODS
  /**********************************************************/
  /**********************************************************/
  private String locationToString (int col, int row){
    String colStr = String.valueOf (col+1);
    char colChr = (colStr.length()==1? colStr.charAt(0): colStr.charAt(1));
    colChr -= '1';
    colChr += 'a';
    if (colStr.length()==2)
      colChr += 10;
    return colChr + String.valueOf (row+1);
  }

  /* Gets row number from a string location
   * "xn" | x = {a|b|c|d|e|f|g|h} && n = {1|2|3|4|5|6|7|8|9}
   * @requires str is in notation of alphabet for column and number for row
   **/
  private int getRowFromString (String str){
    if (str.length() == 2)
      return str.charAt (1) - '1';//'0';
    else {
      String st = str.substring (1);
      return Integer.valueOf(st).intValue() - 1;
    }
  }

  /* Gets col number from a string location
   * "xn" | x = {a|b|c|d|e|f|g|h} && n = {1|2|3|4|5|6|7|8|9}
   * @requires str is in notation of alphabet for column and number for row
   **/
  private int getColFromString (String str){
    return str.charAt (0) - 'a';// + 1;
  }
}
