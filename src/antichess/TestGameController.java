/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;
import junit.framework.*;
import java.io.*;
import java.util.*;

public class TestGameController
  extends TestCase{

  
  String testfilesFile = "src/test/tests";
  String testfilesDir = "src/test/";

  
  public TestGameController(String name) {
    super(name);
  }

  

  /**********************************************************/
  /**********************************************************/
  // TEST CASES
  /**********************************************************/
  /**********************************************************/

  /**
   * Reads a file into gameController, and then get the
   * string output from gameController, and compare results
   **/
  public void testLoadFile (String filename){
    GameController gc = new GameController(filename);
    //     System.out.println ("Contents of " + filename + ":");
    //     System.out.println (fileRead(filename,false));
    //     System.out.println ("Board string from gameController: ");
    //     System.out.println (gc.getBoardString());
    assertTrue ("testing " + filename,
		linesToSortedList(fileRead(filename, false)).equals (linesToSortedList(gc.getBoardString())));
    //System.out.println (fileRead (filename, false));
  }

  /**
   * Tests reading from file and generating string representation of board
   * Reads a "test/tests" to get a list of filenames to use
   **/
  public void testGameControllerReadWrite(){

    StringTokenizer st = new StringTokenizer (fileRead (testfilesFile, false), "\n");
    while (st.hasMoreElements()){
      //       if (1==1)break;
      String filename = (String) st.nextElement();
      System.out.println ("------ Testing reading from file and generating output ------\n ------ Reading from: " + filename + "-------------");
      testLoadFile (testfilesDir + filename);
      //      break;
    }
    
  }
  /**********************************************************/
  /**********************************************************/
  // HELPER FUNCTIONS
  /**********************************************************/
  /**********************************************************/

  /** Converts a string of line into a List of sorted strings
   **/
  private List linesToSortedList (String s){
    StringTokenizer st = new StringTokenizer (s, "\n");
    List l = new ArrayList ();
    while (st.hasMoreElements()){
      String str  = (String) st.nextElement();
      if (str.length()>0)
	if(str.charAt(str.length()-1) == ';')
	  str = str.substring (0, str.length()-1);
      l.add (str);
    }
    Collections.sort (l);
    //System.out.println (l);
    return l;
  }
  
  /**********************************************************/
  /**********************************************************/
  // FILE FUNCTION
  /**********************************************************/
  /**********************************************************/

  /** This function only returns all text before the first ";"
   *  and sets comments to contain all text after the first ";"
   **/
  
  private String fileRead(String filename, boolean readAll) {
    if (filename == null)
      throw new RuntimeException("No file specified");


    String answer = new String();

    try {
      BufferedReader in = new BufferedReader(new FileReader(filename));
      // read each line until the end of the file and parse it into the script
      for (String line = in.readLine(); line != null; line = in.readLine()) {
	
	answer += line;

	if (line.length() > 0)
	  if (line.charAt (line.length()-1) == ';' && !readAll){
	    break;
	  }

	answer += "\n";


      }
      
    }
    catch (Exception e) {
      throw new RuntimeException("File not accessible\n" + filename + "\n" + e);
    }
    
    //System.out.println ("File read:\n" + answer);
    return answer;
  }

  /**********************************************************/
  /**********************************************************/
  // JUNIT METHODS
  /**********************************************************/
  /**********************************************************/
  // Tell JUnit what order to run the tests in
  public static Test suite()
  { 
    TestSuite suite = new TestSuite(); 
    suite.addTest(new TestBoard("testGameControllerReadWrite"));
    return suite;
  }

}
