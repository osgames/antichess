/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import java.util.*;
/**
 * A timer to count down a given time.
 *
 * @specfield timeleft: int // milliseconds left
 * @specfield isTiming          : boolean   state of timer (on or off)
 */
public class Timer{


  //PRIVATE FIELDS
  private long timeLeft; // timeleft in milliseconds
  private boolean isTiming;
  private long temp; // the last time retrieved from system

  //CONSTRUCTOR

  /** Initializes a timer with starting time
   * @param startTime starting time in milliseconds
   **/
  public Timer (long startTime){
    timeLeft = startTime;
  }
		
  public Timer (){
  }

  // OBSERVERS
  public long getTimeLeft (){
    if (isTiming){
      long timeUsed = System.currentTimeMillis() - temp;
      return timeLeft - timeUsed;
    } else
      return timeLeft;
  }

  /** See if the timer is running 
   * @returns true if timer running **/
  public boolean getIsTiming (){
    return isTiming;
  }

  // MUTATORS
  public void setTimeLeft (int time){
    timeLeft = time;
  }

  // TIMER FUNCTIONS
  public void startTimer (){
    isTiming = true;
    temp = System.currentTimeMillis();
  }

  public void stopTimer(){
    isTiming = false;
    long timeUsed = System.currentTimeMillis() - temp;
    timeLeft -= timeUsed;
  }

}


