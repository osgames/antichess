/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/

package antichess;

/**
 *<p> 
 * Board represents an antichess board. Board is mutable.
 *
 *<p> 
 * The topology of the board could vary and is supplied as an argument
 *
 */
public class Board{

  /**********************************************************/
  /**********************************************************/
  // FIELDS 
  /**********************************************************/
  /**********************************************************/

  //PRIVATE FIELDS
  private Piece[][] gameBoard;   //board the game is played on
  private Piece[][][] pieceArray; //pieces on the board for each side
  //Piece[Pawn] would be the pawn in the board
  //if there exists one    
  private Piece[][]   lastPieceMoved; // lastPieceMoved[WHITE | BLACK][]
  static int maxLastPieceMoved = 2; // maximum of last pieces moved

  private int[][] pieceCount;  //number of maximum available pieces during anytime in the game
  private int[][] aliveCount;  //number of pieces on the board
  private boolean[] checked;   
  private boolean[] special;	
  private int[][] topology;
  private int boardType;
  private Zobrist zobrist;

  //PUBLIC CONSTANTS
  /** Constant indicating a regular board (default) 
   */
  public static int REGULAR = 0;
  
  /** Constant indicating a cylinder board 
   */
  public static int CYLINDER = 1;

  /** Constant indicate a four player board **/
  public static int FOURPLAYERS = 2;

  /**
   * Constant which holds the number of columns on the board = MAXCOL-MINCOL+1
   */
  public static int NUMCOLS;

  /**
   * Constant which holds the smallest possible row 
   */
  public static int MINROW;

  /**
   * Constant which holds the biggest possible row 
   */
  public static int MAXROW;

  /**
   * Constant which holds the smallest possible row 
   */
  public static int MINCOL;

  /**
   * Constant which holds the biggest possible row 
   */
  public static int MAXCOL;

   

  /**********************************************************/
  /**********************************************************/
  //CONSTRUCTOR
  /**********************************************************/
  /**********************************************************/
  /**Creates an MAXCOLxMAXROW antichess board.
   * The topology of the board is supplied as an argument of two 
   * dimensional integer array.if topology[i][j]!=0 then, 
   * that square is unusable. For topology[i][j]==1, the 
   * square is a usable one.
   * <p>
   * The constructor initialized checked and special to false.
   * pieceCount and aliveCount is initialized to 0 for all peices 
   * for both sides.
   *@requires topology be an immutable integer matrix of 0's and nonzero
   * integers && MINCOL=0 && MINROW=0 && MAXROW>=0 && MAXCOL>=0 
   * @requires topology has to be zero-based
   *@effects creates an antichess Board
   *@throws IndexOutOfBoundsException if !(MINCOL=0 && MINROW=0 && MAXROW>=0 && MAXCOL>=0) 
   */
  public Board(int[][] topology){


    //    * topology is a  integer
    //    * matrix of 0's and nonzero integers if topology[i][j]!=0 then, 
    //    * it means that a square exists at (i,j) if topology==0 then, it means   
    //    * there does not exist a square there.
    //    * The constructor initializes checked and special fields to false.
    //    * pieceCount and alivepieceCount is initialized to 0 for all pieces
    //    * for both sides.

    this.topology=topology;

    //create the board
    MINCOL=MINROW=0;

    MAXCOL=topology.length - 1; // 7
    MAXROW=topology[0].length - 1; // 7
    NUMCOLS = MAXCOL - MINCOL + 1; // 8
    gameBoard=new Piece[MAXCOL+1][MAXROW+1]; // 0 to 7

    //initialize 
    checked=new boolean[Piece.LASTCOLOR+1];
    checked[Piece.WHITE]=checked[Piece.BLACK]=false;
  
    special=new boolean[Piece.LASTCOLOR+1];
    special[Piece.WHITE]=special[Piece.BLACK]=false;
 
    pieceCount=new int[Piece.LASTCOLOR+1][Piece.LASTPIECE+1];
    aliveCount=new int[Piece.LASTCOLOR+1][Piece.LASTPIECE+1];
    for(int i=Piece.FIRSTPIECE;i<=Piece.LASTPIECE;i++)
      {pieceCount[Piece.WHITE][i]=pieceCount[Piece.BLACK][i]=0;
      aliveCount[Piece.WHITE][i]=aliveCount[Piece.BLACK][i]=0;}
      
    pieceArray=new Piece[Piece.LASTCOLOR+1][Piece.LASTPIECE+1][];
    //lastPieceMoved=new Piece[Piece.LASTCOLOR+1];

    // This assumes that the number of last pieces moved for each color is less
    // than Piece.LASTPIECE
    lastPieceMoved = new Piece[Piece.LASTCOLOR+1][maxLastPieceMoved];
    for (int i=Piece.FIRSTCOLOR; i<=Piece.LASTCOLOR; i++){
      lastPieceMoved[i] = new Piece[maxLastPieceMoved];
    }

  }   

  /** Creates a copy of a given board, deep copying every piece 
   *@requires b.topology be an immutable two dimensional array
   */
  public Board(Board b){

    this.topology=b.topology;
    int i,j,k;
    //create the board
    MINCOL=MINROW=0;

    MAXCOL=topology.length - 1; // 7
    MAXROW=topology[0].length - 1; // 7
    NUMCOLS = MAXCOL - MINCOL + 1; // 8
    gameBoard=new Piece[MAXCOL+1][MAXROW+1]; // 0 to 7
    boardType = b.boardType;

    //initialize 
    checked=new boolean[Piece.LASTCOLOR+1];
    checked[Piece.WHITE]=b.checked[Piece.WHITE];
    checked[Piece.BLACK]=b.checked[Piece.BLACK];
  
    special=new boolean[Piece.LASTCOLOR+1];
    special[Piece.WHITE]=b.special[Piece.WHITE];
    special[Piece.BLACK]=b.special[Piece.BLACK];
 
    pieceCount=new int[Piece.LASTCOLOR+1][Piece.LASTPIECE+1];
    aliveCount=new int[Piece.LASTCOLOR+1][Piece.LASTPIECE+1];
    for(i=Piece.FIRSTPIECE;i<=Piece.LASTPIECE;i++)
      {pieceCount[Piece.WHITE][i]=pieceCount[Piece.BLACK][i]=0;
      aliveCount[Piece.WHITE][i]=aliveCount[Piece.BLACK][i]=0;}
      
    pieceArray=new Piece[Piece.LASTCOLOR+1][Piece.LASTPIECE+1][];
    //lastPieceMoved=new Piece[Piece.LASTCOLOR+1];
    
    // This assumes that the number of last pieces moved for each color is less
    // than Piece.LASTPIECE
    lastPieceMoved = new Piece[Piece.LASTCOLOR+1][maxLastPieceMoved];
    for (i=Piece.FIRSTCOLOR; i<=Piece.LASTCOLOR; i++){
      lastPieceMoved[i] = new Piece[maxLastPieceMoved];
    }

    //Piece.addEmpty(this);
    //    for(int dummy=Piece.FIRSTCOLOR;dummy<=Piece.LASTCOLOR;dummy++)
    //{
    //    System.arraycopy(b.pieceCount[dummy],0,this.pieceCount[dummy],0,Piece.LASTPIECE+1);  
    //                                           //number of maximum available pieces during anytime in the game
    //    System.arraycopy(b.aliveCount[dummy],0,this.aliveCount[dummy],0,Piece.LASTPIECE+1);  //number of pieces on the board
    //}   

    // DEBUGGING OUTPUT
    //     System.out.println ("Making a copy of Board");
    //     for (i=0; i<b.gameBoard.length; i++)
    //       for (j=0; j<b.gameBoard[i].length; j++){
    // 	System.out.println ("at gameboard: " + i + " " + j );
    // 	if (b.gameBoard[i][j] == null){
    // 	  System.out.println ("null");
    // 	} else
    // 	  System.out.println (b.gameBoard[i][j]);
    //       }
    
    //Deep copy all the pieces from given board and add to this board
    for (i=0; i<b.gameBoard.length; i++)
      for (j=0; j<b.gameBoard[i].length; j++)
	if (b.gameBoard[i][j] != null){
	  //	  System.out.println ("At " + i + " " + j);
	  Piece p = b.gameBoard[i][j];
	  p.copyPiece(this);
	}
    //    System.out.println ("End of construction");
    
    
    
    //Piece[Pawn] would be the pawn in the board
    //if there exists one

    // this.lastPieceMoved = b.lastPieceMoved;
    
    for (i=0; i<b.lastPieceMoved.length; i++)
      if (b.lastPieceMoved[i] != null){
	for (j=0; j<b.lastPieceMoved[i].length; j++){
	  if (b.lastPieceMoved[i][j]==null) continue;
	  // System.out.println ("last piece:" + b.lastPieceMoved[i]);
	
	  //this.setLastPieceMoved(this.getPieceAt(b.lastPieceMoved[i].getCol(),
	  //				       b.lastPieceMoved[i].getRow())); // lastPieceMoved[WHITE | BLACK]

	  this.lastPieceMoved[i][j] = this.gameBoard[b.lastPieceMoved[i][j].getCol()][b.lastPieceMoved[i][j].getRow()];
						
	}
      }

    // We can do this here because we are completely done
    // creating a new board from the given board
    initZobrist();
  }
  /**********************************************************/
  /**********************************************************/
  //***************HASHING **********************************/
  /**********************************************************/
  /**********************************************************/



  /**SPECIFIC TO 8x8 Boards. 
   *Returns a int[] representation of the board to be used in a hashtable
   *very specific to 8x8 board, starting from row0 and col0
   *It allocates 5bits per square, HasMoved[1bit]+Color[1bit]+PieceType[3bits]
   *It gives an array of 12 ints. 5*6=30, so puts 6 pieces at most into an integer and
   *does not use the first 2 bits of 32bits.
   *<p>There are 64 squares, so we need 11 integers, the 12th integer keeps track
   * of the last piece moved. Col[3bits] + Row[3bits] (starting from low order bits)
   * <p>
   * The structure is so<br>
   * <pre>
   * integer [1]:  2 bits:empty + 5 bits: square6  + 5 bits: square5  + 5 bits: square4  + .. + 5 bits: square1  <br> 
   * integer [2]:  2 bits:empty + 5 bits: square12 + 5 bits: square11 + 5 bits: square10 + .. + 5 bits: square7  <br>
   * ...<br>
   * integer[11]:  2 bits:empty + 5 bits: empty    + 5 bits: empty    + 5 bits: square64  + .. + 5 bits: square61 <br>
   * integer[12]:  20bits:empty + 3 bits: blackCol  + 3 bits: blackRow + 3 bits: whiteCol + 3bits: whiteRow <br>
   *</pre>
   **/
  public int[] hashStr()
  {
    int k;
    int type;
    int[] result=new int[12];
    int N=6;
    
    //write down the coordinates   
    for(int i=MINROW;i<=MAXROW;i++)
      for(int j=MINCOL;j<=MAXCOL;j++)
	{
	  k=i*(MAXROW+1)+j; // index of the element, starts from 0
	  type=gameBoard[j][i].getType();
	  
	  if (type==Piece.EMPTY) //dont hold HasMoved or Color
	    result[k/N+1]=result[k/N+1]|(type<<((k%N)*5));
	  //result[24-i]=result[i]|(type<<(j*4));
	  
	  else
	    {  
	      if ((type==Piece.KING || type==Piece.ROOK || type==Piece.PAWN) &&
		  gameBoard[j][i].getMoved()) //hold HasMoved and Color
		
		{
		  result[k/N+1]=result[k/N+1]|
		    (16+gameBoard[j][i].getColor()*8+type)<<((k%N)*5);
		  
		}
	      else //dont hold LastMoved, but hold Color
		{
		  result[k/N+1]=result[k/N+1]|
		    (gameBoard[j][i].getColor()*8+type)<<((k%N)*5);  
		  
		}
	    }
	}
    return result;
  
  }

    /** Returns a hashCode for this board
     * Currently uses Zobrist.getKey() 
     * @requires zobrist!=null
     */
    public long zobristCode(int nextTurn)
    {
	return zobrist.getKey(this,nextTurn);
    }

    /** Returns a hashCode for this board
     * Currently uses Zobrist.getKey() 
     * @requires zobrist!=null
     */
    public long zobristCode(String boardStr, int nextTurn)
    {
	return zobrist.getKey(boardStr,nextTurn);
    }

  /**********************************************************/
  /**********************************************************/
  //OBSERVERS
  /**********************************************************/
  /**********************************************************/
  /** Gets the board type
   * @returns {REGULAR | CYLINDER}
   **/
  public int getBoardType(){
    return boardType;
  }

  /** Gets the number of maximum available pieces on the board
   * during any time in the game for a particular side.
   * 
   * @returns a two dimensional array of initial number
   *  of pieces on the board for a particular side. 
   *  Entries of this array could be null.
   **/
  public int getPieceCount(int side,int piece){
    return pieceCount[side][piece];
  }


  /** Gets the weighted difference of number of alive pieces 
   * on the board during any time in the game for a particular 
   * side.
   * 
   * @returns the weighted difference of pieces on the board 
   * for a particular side. 
   **/
  public int getDifference(int side){
    int result=0; 
    for(int i=Piece.FIRSTPIECE;i<Piece.LASTPIECE;i++) //last piece is king, we dont need it 
      result+=aliveCount[side][i]*Piece.PIECEVALUE[i];
      
    int otherSide=Piece.getOpponentColor(side);
    for(int i=Piece.FIRSTPIECE;i<Piece.LASTPIECE;i++) //last piece is king, we dont need it
      result-=aliveCount[otherSide][i]*Piece.PIECEVALUE[i];
    return -result;
  }

  /** Gets the weighted count of alive pieces on the board
   * during any time in the game for a particular side.
   * 
   * @returns the number of a particular piece on the board 
   * for a particular side. 
   **/
  public int getAliveCount(int side,int piece){
    return aliveCount[side][piece];
    
  }

  /** Gets the weighted count of alive pieces on the board
   * during any time in the game for a particular side.
   * 
   * @returns the number of a particular piece on the board 
   * for a particular side. 
   **/
  public int getTotalAliveCount(int side){
      int result=0;
      for (int i=Piece.FIRSTPIECE;i<=Piece.LASTPIECE;i++)
           result+=aliveCount[side][i];
      return (result);
  }


  
  /** Checks if a side is in check
   * @requires side == {Piece.WHITE | Piece.BLACK}
   * @returns true if in check, false otherwise
   **/
  public boolean getChecked(int side){
    return checked[side];
  }

  /** Gets the king Piece of a particular side
   * @requires color = {Piece.WHITE | Piece.Black}
   *  && there is only and only one KingPiece from the beginning to 
   *  the end of the game
   * @returns a KingPiece
   **/
  public Piece getKing (int color){
    //     if (aliveCount[color][Piece.KING]==0) return null;
    //     for (int i=0; i<pieceCount[color][Piece.KING]; i++)
    //       if (pieceArray[color][Piece.KING][i]!=null)
    // 	return pieceArray[color][Piece.KING][i];

    // We can have test cases which doesnt have a king,
    // so we have to check if the array is null first
    if (pieceArray[color][Piece.KING] != null)
      return pieceArray[color][Piece.KING][0];
    else
      return null;
    
  }

  /** Checks if a side has used special move
   * @requires side == {Piece.WHITE | Piece.BLACK}
   * @returns true if used special, else false
   **/
  public boolean getSpecial(int side){
    return special[side];
  }

  /** Gets a piece at a particular row and column. 
   * @requires 1<=location.row<=numberOfRows && 1<=location.col<=numberOfCols
   * @returns a piece at the specified location
   **/
  public Piece getPieceAt (int col, int row){
    return gameBoard[col][row];
  }
  /** Gets an array of Piece that are present on the board. 
   * This array contains only Pieces that are on the board.
   * @returns A Piece[] containing the pieces currently on the board
   **/
  public Piece[][] getPiecesOnBoard (int side){
    return pieceArray[side];
  }

	
  /** Gets the last piece move to a piece
   * @returns lastPieceMoved
   **/
  public Piece getLastPieceMoved (int color){
    return lastPieceMoved[color][0];
  }

  /** Gets the last piece move to a piece at an index
   * @returns lastPieceMoved
   **/
  public Piece getLastPieceMoved (int color, int index){
    return lastPieceMoved[color][index];
  }

  /** Checks if a piece is the last piece moved 
   * @requires Piece != null
   **/
  public boolean isLastPieceMoved (Piece p){
    int color = p.getColor();
    if (lastPieceMoved[color] == null)
      return false;

//     System.out.println ("isLastPieceMoved: \n" + p);
    for (int i=0; i<maxLastPieceMoved; i++){
      if (lastPieceMoved[color][i]!=null){
// 	System.out.println ("lastpiecemoved:  " + i + ": \n" + lastPieceMoved[color][i]);
	if (p.equals (lastPieceMoved[color][i]))
	  return true;
      }
    }
    return false;
  }
	
  /** Gets whether given location is usable
   * @returns true if given location is usable
   **/
  public boolean getUsable (int col, int row){
    if (MINROW <=row && row <=MAXROW && MINCOL<=col && col<=MAXCOL)
      if (topology[col][row]!=0) return true;
    return false; 
  }




  /** Returns a string representation of the board
   *@returns a string representing the board
   *unusable squares is a '*'
   **/
  public String toString()
    { String value="";
    for(int i=MAXROW;MINROW<=i;i--)
      {
	for(int j=MINCOL;j<=MAXCOL;j++)
	  if (topology[j][i]!=0)            //check that there is no null square 
	    { if (gameBoard[j][i]!=null) 
	      value=value+(gameBoard[j][i]).getChar();
	    else value=value+" ";}
	  else value=value+"*";
	value=value+"\n";}
    return value;
    }

  /** Returns the zobrist **/
  public Zobrist getZobrist(){
    return zobrist;
  }

  /**********************************************************/
  /**********************************************************/
  //MUTATORS
  /**********************************************************/
  /**********************************************************/


  /**
   * Sets the board type
   * @requires type = {REGULAR | CYLINDER}
   **/
  public void setBoardType (int type){
    boardType = type;
  }
  /** Sets a side to be in check or not to be in check
   * @requires side == {Piece.WHITE | Piece.BLACK}
   * @modifies this
   * @effects sets a side to be in check/not in check
   **/
  public void setChecked(int side, boolean inCheck){
    checked[side]=inCheck;
  }

  /** Sets whether a side has used special move
   * @requires side == {Piece.WHITE | Piece.BLACK}
   * @modifies this
   * @effects sets the side to have used / have not used special move
   **/
  public void setSpecial(int side, boolean usedSpecial){
    special[side]=usedSpecial;
  }

  /** Modify a Piece at a location
   * @requires 1<=row<=numberOfRows && 1<=col<=numberOfCols
   * @effects Sets the Piece at (row, col) to point to p
   **/
  public void setPieceAt (int col,int row, Piece p){
    gameBoard[col][row]=p;
  }

  /** Sets the last piece moved 
   * @effects Sets lastPieceMoved[color][0] to be p, lastPieceMoved[color][1] to be null
   **/
  public void setLastPieceMoved (Piece p){
    if (p==null)
      return;
    lastPieceMoved[p.getColor()][0]=p;
    lastPieceMoved[p.getColor()][1]=null;
  }
  /** Sets the last pieces moved 
   * @effects Sets lastPieceMoved to be p1 and p2
   **/
  public void setLastPieceMoved (Piece p1, Piece p2, int color){
    //lastPieceMoved[p.getColor()]=p;
    lastPieceMoved[color][0] = p1;
    lastPieceMoved[color][1] = p2;
  }

  /** Sets the last piece move to a piece
   * @requires piece is on board
   * @effects Sets lastPieceMoved[color][0] to be p, lastPieceMoved[color][1] to be null
   **/
  public void setLastPieceMoved (Piece p, int color){
    lastPieceMoved[color][0]=p;
    lastPieceMoved[color][1]=null;
  }
  
  /** Sets the last piece move to a piece
   * @requires piece is on board
   * @effects Sets lastPieceMoved[color][index] to be p
   **/
  public void setLastPieceMoved (Piece p, int color, int index){
    lastPieceMoved[color][index]=p;
  }
  
  /** Checks for equality. <br>
   * Overrides Object.equals
   * @returns true if given object is same as this
   **/
  public boolean equals (Object o){
    if (o instanceof Board){
      return equals ((Board) o);
    } else
      return false;
  }

  /** Checks for equality. <br>//HALF IMPLEMENTED//
   * Overrides Object.equals
   * @returns true if given board is same
   **/
  public boolean equals (Board other){
    return ( other.getChecked(Piece.WHITE)==this.checked[Piece.WHITE] &&
	     other.getChecked(Piece.BLACK)==this.checked[Piece.BLACK] &&
	     other.getLastPieceMoved(Piece.WHITE).equals(this.lastPieceMoved[Piece.WHITE]) &&
	     other.getLastPieceMoved(Piece.BLACK).equals(this.lastPieceMoved[Piece.BLACK]) 	     );
  }


  /**Registers a piece to the board at (col,row)
      
  * @requires piece instance of Piece && MINCOL<=col<=MAXCOL
  *  && MINROW<=row<=MAXROW && Board(col,row) is a usable square
  * @throws RuntimeException if !(MINCOL<=col<=MAXCOL
  *  && MINROW<=row<=MAXROW && Board(col,row) is a usable square)
  */
  public void registerPiece(int col, int row,Piece piece){ 
    if (topology[col][row]==0)
      throw new RuntimeException("Board.registerPiece: trying to write on unusable square!");

    //System.out.println ("register");

      
    //delete the old piece if there was a piece at that location

    if (gameBoard[col][row]!=null)    
      {   Piece oldpiece=gameBoard[col][row];
      int oldtype=oldpiece.getType();
      int oldcolor=oldpiece.getColor();  

      //System.out.println ("deleting old type");
	    
      if (oldtype!=Piece.EMPTY) 
	{
	  //System.out.println ("capture");

	  //decrease the number of alive pieces (but not the number of maximum pieces)
	  aliveCount[oldcolor][oldtype]--;
		
	  //delete it from piecearray
	  for (int i=0;i<pieceCount[oldcolor][oldtype];i++)
	      
	    if (pieceArray[oldcolor][oldtype][i]!=null)
	      if (pieceArray[oldcolor][oldtype][i].equals(oldpiece))
		{pieceArray[oldcolor][oldtype][i]=null;
		//System.out.println("deleting in: "+i+" piece:"+oldpiece);
		}
	}    

      }
    //delete it from the board, put the new piece on
    gameBoard[col][row]=piece;


    int newtype=piece.getType();
    int newcolor =piece.getColor();
    
    if (newtype!=Piece.EMPTY) 
      {

	//increase the number of pieces 
	aliveCount[newcolor][newtype]++;
	int oldpiececount=pieceCount[newcolor][newtype];                


               
	//we are adding a piece to the pieceArray into a null place
	if (aliveCount[newcolor][newtype]<=oldpiececount)
	  {
	    //go through the piecearray and add to the
	    //first null place
	    for (int i=0;i<oldpiececount;i++)
	      if (pieceArray[newcolor][newtype][i]==null)
		{ //System.out.println("Adding in:"+i+" piece:"+ piece);
		  pieceArray[newcolor][newtype][i]=piece;
		  break;
		}
	  }

              
	//we are appending the piece to the pieceArray
	if (aliveCount[newcolor][newtype]>oldpiececount)
	  {

	    //create a new array of size+1
	    Piece[] newArray =new Piece[oldpiececount+1];  
	    //put old guys in a new array

            if (pieceArray[newcolor][newtype]!=null)
	      System.arraycopy(pieceArray[newcolor][newtype],0,newArray,0,oldpiececount);

	    //for (int i=0;i<oldpiececount;i++)
	    //  newArray[i]=pieceArray[newcolor][newtype][i];
	  

	    //append the piece to the new array
	    newArray[oldpiececount]=piece;
	    // make the new pieceArray 
	    pieceArray[newcolor][newtype]=newArray;
	    // increase the pieceCount accordingly
	    pieceCount[newcolor][newtype]++;
	    // make sure there are no differences between aliveCount and pieceCount
	    aliveCount[newcolor][newtype]=pieceCount[newcolor][newtype];  	
	  }
      }
  }


    /**Initializes the zobrist matrix for this board
     *CALL THIS ONLY AFTER YOU PUT ALL THE PIECES INTO THE BOARD
     *@requires board to be full 
     */
    public void initZobrist()
    {
	this.zobrist=new Zobrist(this);
    }

    
  }
