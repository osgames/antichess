/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import java.util.*;

/**
 * This class contains the rules that apply to King.
 * It implements castling as a special move.
 **/

public class King extends Piece{
  /** Creates a new piece.
   * @param letter the character representation of the piece:
   * @param board the board the piece will be assigned to
   * @param row starting row of piece
   * @param col starting col of piece
   * @param color color
   * @requires letter = {P | p | K | k | N | n | R | r | Q | q | B | b}
   **/
  public King (char letter, Board b, int col, int row, int color){
    super (Piece.KING, letter, b, col, row, color);
    int[] myDCol = { -1, -1, -1,  0,  0,  1,  1,  1};
    int[] myDRow = { -1,  0,  1, -1,  1, -1,  0,  1};
    dcol = myDCol;
    drow = myDRow;
    slide = false;
    offsets = 8;
  }

  /**
   * Creates a copy to a new board
   */
  public Piece copyPiece (Board b){
    Piece p = new King (this.getChar(), b, this.getCol(), this.getRow(), this.getColor());
    p.setLastLocation (this.getLastCol(), this.getLastRow());
    p.setMoved (this.getMoved());
    return p;
  }

  /** Overrides getOtherMoves in Piece and retrieves castling moves. **/
  public List getOtherMoves(){
    // check for 4-player, if 4-player, no castling
    if (this.getBoard().getBoardType() == Board.FOURPLAYERS)
      return new ArrayList();
    else
      return getCastlingMoves();
  }
	
  /** Returns a list of castling moves the King can make <br>
   *  Overrides getSpecialMoves in Piece.
   *  @requires 2-player game
   *  @return list of moves representing the switches the King can make
   **/
  private List getCastlingMoves (){
    // This checks for castling, and combines the possible castling moves
    // with the switching moves.
		
    // We need to check the following:
    // (1) that the King and rook have never moved. 
    // (2) that the King is not under attack. You may not castle out of check. 
    // (3) that the King is not passing through or arriving upon a square
    // controlled by the opponent. 
    // (4) that all of the squares between the King and Rook are vacant. 
    Board b = this.getBoard();
    int startRow = this.getRow();
    int startCol = this.getCol();
    int type = this.getType();
    int color = this.getColor();

    int k;
    int rookRow, rookCol;

		
    List validMoves = new ArrayList();
    int[] rookRows = null; // Rooks' starting position
    int[] rookCols = {b.MINCOL, 0, b.MAXCOL};

    int opponentColor = this.getOpponentColor();

    // Check that king has not moved. If it has, no need to check.
    if (this.getMoved ()) return validMoves;

    // Check that king is not in check
    if (this.inCheck())
      return validMoves;
    // else
    //       System.out.println ("King is not in check");

    // Check color and determine Rooks' starting position
    // loc will contain the Rooks' starting position. myRow[1] is always null
    // and is a dummy variable.
    // myRow[0] contains left side Rook. (0 == dcol + 1 when dcol == -1)
    // myRow[2] contains right side Rook. (2 == dcol + 1 when dcol == 1)
    if (color == Piece.WHITE){
      // WHITE
      int[] myRow = {b.MINROW, 0, b.MINROW};
      rookRows = myRow;
    } else {
      int[] myRow = {b.MAXROW, 0, b.MAXROW};
      rookRows = myRow;
    }

    // dcol = -1: check left side
    // dcol = 1: check right side
    int curRow = startRow;
    int curCol = startCol;
    for (int dcol = -1; dcol < 2; dcol += 2){

      // Get the Rook location of a side
      rookRow = rookRows[dcol+1];
      rookCol = rookCols[dcol+1];
      int destRow = startRow;
      int destCol = startCol + dcol * 2;
      

      // See if a piece is there

      Piece rookPiece = b.getPieceAt (rookCol, rookRow);
      if (rookPiece == null)
	continue;

      // Check if it is a rook
      if (rookPiece.getType() != Piece.ROOK)
	continue;

      // Check if it is the same color
      if (rookPiece.getColor() != color)
	continue;

      // Check if the rook has moved
      if (rookPiece.getMoved ()) continue;


      // Check that the locations in between are empty and 
      // that location the king passes or lands on are not attacked by opponent


      // Assume castling is possible first
      boolean canCastle = true;

      // Offset the initial column by the direction column vector
      curCol = startCol + dcol;

      // k keeps track of whether the king will go through this location
      k = 0; // k < 2 if the king will pass/land on this location

      // Loop until we reach the destination square
			
      while (curCol != rookCol){
	if (curCol > b.MAXCOL ||
	    curCol < b.MINCOL)
	  break;

	// Get the location to check
	// Check if square is valid

	// Check if location is empty
	Piece p = b.getPieceAt(curCol, curRow);
	if (p == null){
	  //System.out.println ("castling: null square at " + curCol + " " + curRow);
	  canCastle=false;
	  break;
	}
	if (p.getType() != Piece.EMPTY){
	  //System.out.println ("castling: nonempty square at " + curCol + " " + curRow);
	  canCastle = false;
	  break;
	}

	// Check if a location that the king pass through or 
	// land on is attacked by opponent
	if (k < 2) {

	  // Iterate through all opponent pieces on the board
	  Piece pieces[][] = b.getPiecesOnBoard(opponentColor);

	  // Iterate through all pieces
	  for (int i = Piece.FIRSTPIECE; i <= Piece.LASTPIECE; i++){
	    int pieceCount = b.getPieceCount (opponentColor, i);
	    if (pieces[i] == null)
	      continue;
	    for (int j = 0; j < pieceCount; j++){

	      Piece opponentPiece = pieces[i][j];

	      // Check if it is a piece
	      if (opponentPiece == null)
		continue;

	      // This location is attacked by opponent. So
	      // we cannot castle, and need not check further
	      // in this direction.
	      if (opponentPiece.canAttackLocation (curCol, curRow)){
		canCastle = false;
		break;
	      }
	    }
	  }

	  // Since k=0,1, this portion is processed 2 times.
	  // destCol will contain the 2nd square away from
	  // the starting square of the king
	  //destCol = curCol;
	}

	// Advance to the next square
	curCol += dcol;
	//System.out.println ("cur col " + curCol);
	k++;
      }

      if (!canCastle) continue;

      // Add move
      Move m = new Move(startCol, startRow, destCol, destRow);
      m.setCastle (true);
      validMoves.add (m);

    }

    //System.out.println ("Castling moves: " + validMoves);
    return validMoves;
  }

  /* Checks if the king is in check.
   * This method assumes no knowledge of other pieces, and ask all opponent pieces if they are attacking this location.*/
  public boolean inCheckOld (){
    int opponentColor = this.getOpponentColor();
    Piece pieces[][] = getBoard().getPiecesOnBoard(opponentColor);
    int kingRow = this.getRow();
    int kingCol = this.getCol();
    Board b = this.getBoard();

    for (int i = Piece.FIRSTPIECE; i <= Piece.LASTPIECE; i++){
      int pieceCount = b.getPieceCount (opponentColor, i);
      for (int j = 0; j < pieceCount; j++){
	Piece opponentPiece = pieces[i][j];
	if (opponentPiece == null) continue;

	//  	System.out.println ("Checking king check with piece:");
	//  	System.out.println (opponentPiece);
	// 	System.out.println ("King is at " + kingCol + " " + kingRow);

	// If opponent can attack, then return true
	if (opponentPiece.canAttackLocation (kingCol, kingRow)){
	  return true;
	}
      }
    }
    return false;
  }

  /* Checks if the king is in check in four player game.
   * This method assumes no knowledge of other pieces,
   * and ask all opponent pieces if they are attacking this location.*/
  public boolean inCheckAll (){


    int kingRow = this.getRow();
    int kingCol = this.getCol();
    Board b = this.getBoard();

    int color = this.getColor();
    int opponentColor = this.getOpponentColor();
    for (;opponentColor != color;
	 opponentColor = Piece.getOpponentColor(opponentColor)){
      Piece pieces[][] = getBoard().getPiecesOnBoard(opponentColor);
      for (int i = Piece.FIRSTPIECE; i <= Piece.LASTPIECE; i++){
	int pieceCount = b.getPieceCount (opponentColor, i);
	for (int j = 0; j < pieceCount; j++){
	  Piece opponentPiece = pieces[i][j];
	  if (opponentPiece == null) continue;
	  
	  //  	System.out.println ("Checking king check with piece:");
	  //  	System.out.println (opponentPiece);
	  // 	System.out.println ("King is at " + kingCol + " " + kingRow);
	  
	  // If opponent can attack, then return true
	  if (opponentPiece.canAttackLocation (kingCol, kingRow)){
	    return true;
	  }
	}
      }
    }
    return false;
  }


  /** Overrides makeOtherMove in Piece. Implements castling **/
  public void makeOtherMove (Move move){

    // To make castling move, swap King and Empty, Rook and Empty
    // For undo, keep piece info for King and Rook
    // To undo, swap King and Empty, Rook and Empty, restore info
    
    Board b = this.getBoard();
    if (!move.getCastle())
      return;

    int[] rookRows = null; // Rooks' starting position
    int[] rookCols = {b.MINCOL, b.MAXCOL};
    int[] rookDestCols = {3, 5};
    if (this.getColor() == Piece.WHITE){
      // WHITE
      int[] myRow = {b.MINROW, b.MINROW};
      rookRows = myRow;
    } else {
      int[] myRow = {b.MAXROW, b.MAXROW};
      rookRows = myRow;
    }

    int startRow = move.getStartRow();
    int startCol = move.getStartCol();
    int destRow = move.getDestRow();
    int destCol = move.getDestCol();

    int side = startCol - destCol;
    if (side > 0) // King moves left
      side = 0;
    else
      side = 1; // King move right

    int rookRow = rookRows[side];
    int rookCol = rookCols[side];
    int rookDestCol = rookDestCols[side];
    Piece rookPiece = b.getPieceAt (rookCol, rookRow);
    
    // Keep track of backup information for undo move
    // 1. Store last pieces info
    move.setLastPieces (b);

    // 2. Store piece info for king and rook
    move.storePieceInfo (this, 0);
    move.storePieceInfo (rookPiece, 1);

    // 3. Swap rook
    rookPiece.swapPieces (rookDestCol, rookRow);
    // System.out.println ("Rook dest : " + rookDestCol + " " + rookRow);

    // 4. Swap king
    this.swapPieces (destCol, destRow);

    // make rook last piece as well
    b.setLastPieceMoved (this, rookPiece, this.getColor());
    
  }

  /** Overrides undoOtherMove in Piece. Implements undo castling **/
  public void undoOtherMove (Move move){

    // To make castling move, swap King and Empty, Rook and Empty
    // For undo, keep piece info for King and Rook
    // To undo, swap King and Empty, Rook and Empty, restore info
    
    Board b = this.getBoard();

    int[] rookRows = null; // Rooks' starting position
    int[] rookCols = {b.MINCOL, b.MAXCOL};
    int[] rookDestCols = {3, 5};
    if (this.getColor() == Piece.WHITE){
      // WHITE
      int[] myRow = {b.MINROW, b.MINROW};
      rookRows = myRow;
    } else {
      int[] myRow = {b.MAXROW, b.MAXROW};
      rookRows = myRow;
    }

    int startRow = move.getStartRow();
    int startCol = move.getStartCol();
    int destRow = move.getDestRow();
    int destCol = move.getDestCol();

    int side = startCol - destCol;
    if (side > 0) // King moves left
      side = 0;
    else
      side = 1; // King move right

    int rookRow = rookRows[side];
    int rookCol = rookCols[side];
    int rookDestCol = rookDestCols[side];
    Piece rookPiece = b.getPieceAt (rookDestCol, rookRow);

    // 4. Swap king
    this.swapPieces (startCol, startRow);

    // 3. Swap rook
    rookPiece.swapPieces (rookCol, rookRow);
    // System.out.println ("Rook dest : " + rookDestCol + " " + rookRow);

    // 2. Restore piece info for king and rook
    move.restorePieceInfo (this, 0);
    move.restorePieceInfo (rookPiece, 1);
    
    // 1. restore last piece info
    move.restoreLastPieces (b);

  }

  int cOffsets = 16;
  int[] cDCol = { -1, -1, -1,  0,  0,  1,  1,  1, // 8 standard directions
		  -2, -2, -1, -1,  1,  1,  2,  2}; //knight

  int[] cDRow = { -1,  0,  1, -1,  1, -1,  0,  1, // 8 standard directions
		  -1,  1, -2,  2, -2,  2, -1,  1}; //knight

  // number of opponent type that can attack from this direction
  // first index is color of king: white =0, black =1
  // second index is number of opponent type
  int[][] cPieceCount = {
    // WHITE KING
    {3, 3, 4, 3, 3, 3, 3, 4,
     1, 1, 1, 1, 1, 1, 1, 1},
    // BLACK KING
    {4, 3, 3, 3, 3, 4, 3, 3,
     1, 1, 1, 1, 1, 1, 1, 1}};

  // the piece types that can attack from a particular direction
  // first index is direction index
  // second index is piece type
  int[][] cPieceType = {
    // (col, row)
    // -1,-1
    {Piece.QUEEN, Piece.KING, Piece.BISHOP, Piece.PAWN},
    // -1, 0
    {Piece.QUEEN, Piece.KING, Piece.ROOK},
    // -1, 1
    {Piece.QUEEN, Piece.KING, Piece.BISHOP, Piece.PAWN},
    // 0, -1
    {Piece.QUEEN, Piece.KING, Piece.ROOK},
    // 0, 1
    {Piece.QUEEN, Piece.KING, Piece.ROOK},
    // 1, -1
    {Piece.QUEEN, Piece.KING, Piece.BISHOP, Piece.PAWN},
    // 1, 0
    {Piece.QUEEN, Piece.KING, Piece.ROOK},
    // 1, 1
    {Piece.QUEEN, Piece.KING, Piece.BISHOP, Piece.PAWN},
    // KNIGHT
    {Piece.KNIGHT}, {Piece.KNIGHT}, {Piece.KNIGHT}, {Piece.KNIGHT}, 
    {Piece.KNIGHT}, {Piece.KNIGHT}, {Piece.KNIGHT}, {Piece.KNIGHT}}; 

  // the piece slide info
  // first index is direction index
  // second index is piece slide
  int[][] cSlide = {
    // (col, row)
    // -1,-1
    {1, 0, 1, 0},
    // -1, 0
    {1, 0, 1},
    // -1, 1
    {1, 0, 1, 0},
    // 0, -1
    {1, 0, 1},
    // 0, 1
    {1, 0, 1},
    // 1, -1
    {1, 0, 1, 0},
    // 1, 0
    {1, 0, 1},
    // 1, 1
    {1, 0, 1, 0},
    // KNIGHT
    {0}, {0}, {0}, {0}, 
    {0}, {0}, {0}, {0}}; 


  /* Checks if the king is in check.
   * This method assums knowledge of how other pieces move, and
   * traces from its location to other positions.
   **/
  public boolean inCheck(){

    // if board is 4player, we cannot use this, so use old checking
    if (this.getBoard().getBoardType() == Board.FOURPLAYERS){
      return inCheckAll();
    }
    
    Board b = this.getBoard();
    int startRow = this.getRow();
    int startCol = this.getCol();
    int color = this.getColor();
    int opponentColor = this.getOpponentColor();

    int destRow;
    int destCol;

    int i;
    Piece p = null;
    Move m;


    // iterate through all possible directions
    for (i = 0; i < cOffsets; i++){

      // reset the destination row and column to the starting location of the piece
      destRow = startRow;
      destCol = startCol;

      int slide = 0;
      for (;;){

	// offset the destination row and column using the offset vector
	destRow += cDRow[i];
	destCol += cDCol[i];

	// check if we are out of the board
	// 	if (destRow > b.MAXROW ||
	// 	    destRow < b.MINROW ||
	// 	    destCol > b.MAXCOL ||
	// 	    destCol < b.MINCOL)
	// 	  break;


	if (b.getBoardType() == b.CYLINDER){
	  // Cylinder board: we need to wrap around columns

	  if (destCol < b.MINCOL)
	    destCol += b.NUMCOLS;
	  if (destCol > b.MAXCOL)
	    destCol -= b.NUMCOLS;

	  if (destRow > b.MAXROW ||
	      destRow < b.MINROW ||
	      (destCol == startCol && destRow == startRow))
	    break;
	} else {
	  if (destRow > b.MAXROW ||
	      destRow < b.MINROW ||
	      destCol > b.MAXCOL ||
	      destCol < b.MINCOL)
	    break;
	}
	p = b.getPieceAt(destCol, destRow);
	// check if the square is valid
	if (p == null)
	  continue;

	int opponentType  = p.getType();
	// Check if a piece is on the destination location
	if (opponentType != Piece.EMPTY){
	  // location has a piece
	  // Get the piece

	  // Check if the piece is the same color
	  if (p.getColor() == color){
	    // If same color, we are blocked.
	    // Done with this direction as we are blocked

	  } else {
	    // If different color, we check if the opponent piece is of the type
	    // which can attack our king
	    for (int k = 0; k < cPieceCount[color][i]; k++){
	      if (opponentType == cPieceType[i][k] && slide <= cSlide[i][k])
		return true;
	    }
	  }
	  // Since this location has a piece, we can either be checked by it if
	  // or we are shielded. So we need not check further in this direction.
	  break;

	} else {
	  // do nothing here as location is empty
	}
	slide = 1; // more than one square
	if (i == 8) break; // skip slide check for knight
      }
    }

    return false;
  }
}
