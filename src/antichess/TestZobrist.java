/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;
import junit.framework.*;
import java.io.*;
import java.util.*;

/** Tests Zobrist
 */
public class TestZobrist
  extends TestCase{
//   String testfilesFile = "src/test/tests";
//   String testfilesDir = "src/test/";
  String testfilesFile = "src/test/allTests";
  String testfilesDir = "src/test/";

  public TestZobrist(String name) {
    super(name);
  }


  // HELPER FUNCTIONS

  /** Converts a string of line into a List of sorted strings
   **/
  private List linesToSortedList (String s){
    StringTokenizer st = new StringTokenizer (s, "\n");
    List l = new ArrayList ();
    while (st.hasMoreElements()){
      String str  = (String) st.nextElement();
      if (str.length()>0)
	if(str.charAt(str.length()-1) == ';')
	  str = str.substring (0, str.length()-1);
      l.add (str);
    }
    Collections.sort (l);
    //System.out.println (l);
    return l;
  }
  
  // FILE FUNCTION

  /** This function only returns all text before the first ";"
   *  and sets comments to contain all text after the first ";"
   **/
  
  private String fileRead(String filename, boolean readAll) {
    if (filename == null)
      throw new RuntimeException("No file specified");


    String answer = new String();

    try {
      BufferedReader in = new BufferedReader(new FileReader(filename));
      // read each line until the end of the file and parse it into the script
      for (String line = in.readLine(); line != null; line = in.readLine()) {
	
	answer += line;

	if (line.length() > 0)
	  if (line.charAt (line.length()-1) == ';' && !readAll){
	    break;
	  }

	answer += "\n";


      }
      
    }
    catch (Exception e) {
      throw new RuntimeException("File not accessible\n" + filename + "\n" + e);
    }
    
    //System.out.println ("File read:\n" + answer);
    return answer;
  }
  
  protected void setUp(){
  }


  /** Tests zobrist
   **/
  public void testZobrist(){
    //if (1==1) return;


    Map hm = new HashMap();

    StringTokenizer st1 = new StringTokenizer (fileRead (testfilesFile, false), "\n");
    while (st1.hasMoreElements()){
      String filename = (String) st1.nextElement();
      System.out.println ("------- TESTING ZOBRIST ------");
      System.out.println ("------- Reading " + filename + " ------");
      
      String filecontents = fileRead (testfilesDir + filename, false);
      StringTokenizer st = new StringTokenizer (filecontents, ";");

      GameController gc  = new GameController (testfilesDir + filename);
      int nextTurn = gc.getNextTurn();
      Board b = gc.getBoard();
      Zobrist z = b.getZobrist();
      Long key = new Long(z.getKey (b, nextTurn));
      String bstring = gc.getBoardString();
//       if (!bstring.equals(filecontents)){
// 	System.out.println (filecontents);
// 	System.out.println (bstring);
// 	throw new RuntimeException ("boardstring different from filecontents");
//       }

      String previousString = (String) hm.get(key);
      if (previousString != null)
	if (!bstring.equals (previousString)){
	  System.out.println ("Duplicate zobrist key for two different boards:");
	  System.out.println (key);
	  System.out.println ("Current board: ======");
	  System.out.println (bstring);
	  System.out.println ("Previous board: ======");
	  System.out.println (previousString);
	  throw new RuntimeException ("Zobrist duplicated for different boards");
	}

      hm.put (key, bstring);
    
    }
  }

  // Tell JUnit what order to run the tests in
  public static Test suite()
  { 
    TestSuite suite = new TestSuite(); 
    suite.addTest(new TestZobrist("testZobrist"));
    return suite;
  }
}
