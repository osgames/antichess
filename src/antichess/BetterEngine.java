/*
 AntiChess, an antichess game

 Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
 (For copyright info please see COPYRIGHT file under the base directory)
 
 For bugs, contact info:
 barisy@mit.edu or hongping@mit.edu
*/
package antichess;


import java.util.*;
/**
 * <p>
 * BetterEngine represents the player engine
 *
 * <p>
 * BetterEngine is a class with static methods
 */
public final class BetterEngine implements Runnable{  


    //Checking for debugging information
    private static boolean debug = false;
    private static boolean debughash= false;
    private static boolean debughashcut=false;
 
  
    //Necessary fields for this search
    private Board board;  
    private Map boardTable;
    private int initialDepth;
    private int initialNextTurn;

    /**Constants which determine the search space
     */
    public static int QUIES_DEPTH=3;
    public static int WIN=123456789;
    public static int UNKNOWN=923456789;
    public static int MAXDEPTH=5;
    public static boolean QUIES = true;
    public static int SWITCH_STARTCOUNT=10;

    /**
     * This is to communicate the engine whether it should quit searching the searching process
     * WILL BE SET BY OUTSIDERS
     */

   public boolean searchInProgress;
    
    /**
     * If searchFinished is true, foundMove is guarenteed to have a valid move
     * WILL BE SET BY BETTER ENGINE
     */
   public boolean searchFinished;


    /**
     * If searchFinished is true, foundMove is a good move, check if it is valid
     * WILL BE SET BY BETTER ENGINE
     */

    public  Move foundMove;

    /**
     * If searchFinished is true, secondMove is a good move for opponent, check if it is valid
     * WILL BE SET BY BETTER ENGINE
     */


    public  Move secondMove;

    //CONSTRUCTORS

    /**This starts a new BetterEngine with the corresponding values
     */

    public BetterEngine(Board board, Map boardTable, int depth, int nextTurn)
    {
	this.board=board;
	this.boardTable=boardTable;
	this.initialDepth=depth;
	this.initialNextTurn=nextTurn;
    }

    //*********************************************1*************************************
    //**********************************************************************************
    //                     ALHPA-BETA WITH HASHTABLE 
    //**********************************************************************************
    //**********************************************************************************

   
  /**
   *  Standard AlphaBeta algorithm, lower terms
   * It is necessary to make a distinction between the entrance method which runs
   * on the root node and the recursive method which runs on children nodes.
   * This distinction allows us to return from the recursion if depth is given 
   * less than 0 or if it is an endgame situation (which shoudl be detected by GameController)
   * Furthermore, entrance method returns a move and modifies the board, meanwhile the 
   * recursive method only returns integet values.
   * @returns best value found so far
   * @requires board!=null && player=={Piece.WHITE || Piece.BLACK}
   */

  private int alphaBetaHashRec(int depth,int nextTurn,int alphaIN, int betaIN, int doneDepth){

    //if search is not progressing, please return  
    if (!searchInProgress)
	  return UNKNOWN;
      
    boolean printHashKey=false;  
 
    //get validMoves
    List validMoves = Piece.getValidMoves(board, nextTurn);

    //Messages
    if (debug)
	System.out.println("("+depth+") Number of moves: "+validMoves.size()+" alp: "+alphaIN+" be: "+betaIN);

    //check if it is the end of the game
     if (Piece.isTie(board,nextTurn,validMoves))
	    return 0;
     if (Piece.isWin(board,nextTurn,validMoves)) //only next moving player can win the game
	  return(WIN-1);



     //hashkey for this board
     HashKey hashKey=new HashKey(board.zobristCode(nextTurn));
 
     //evaluate if depth has been reached and search is still in progress
    if (depth<=0)  
	{
	
	//put it into the hashTable and return
	  int val;
	  //= makeQuies(board, nextTurn,alphaIN,betaIN,0);

	if (QUIES)
	    {
		val = makeQuies(board, nextTurn,alphaIN,betaIN,0);
		if (val==UNKNOWN)
		    val = evaluate (board, nextTurn);
	    }
		else
	  val = evaluate (board, nextTurn);
	

	if (debug)
	    System.out.println("("+depth+"):"+val);


	HashEntry putInEntry= new HashEntry(hashKey.key,null,HashEntry.HASHEXACT,val,0);
	boardTable.put(hashKey,putInEntry); 
	//print the debug code on the screen
	if (debughash)
		  System.out.println("("+depth+"):"+hashKey.hashCode()+":"+putInEntry);

	return val;}


     //get the hashEntry which have the same hashKey as the board
     HashEntry retrivedEntry=(HashEntry)boardTable.get(hashKey);


    if (printHashKey)
	System.out.println("k:"+hashKey.key+"i:"+(int)hashKey.key);


     //check if we can return this value
     if (retrivedEntry!=null) 
     	 {   
	     //check if the depth makes us use this node effectively
	     if (retrivedEntry.getKey()==hashKey.key && depth<=retrivedEntry.getDepth())
		 {
		     if (retrivedEntry.getType()==HashEntry.HASHEXACT)
			 {
			     if (debughashcut)
			     //Print out that we making a cut from the table
			     System.out.println("Cutting from the table exact:"+retrivedEntry.getValue());
		  
			     return retrivedEntry.getValue();
			 }
		     if (retrivedEntry.getType()==HashEntry.HASHALPHA && retrivedEntry.getValue()<=alphaIN)
			 {
			     if (debughashcut)
		     //Print out that we making a cut from the table
		     System.out.println("Cutting from the table alpha:"+alphaIN);
		  
		     return alphaIN;}
		     if (retrivedEntry.getType()==HashEntry.HASHBETA && betaIN<=retrivedEntry.getValue())
			 {
			     if (debughashcut)
		     //Print out that we making a cut from the table
		     System.out.println("Cutting from the table beta:"+betaIN);
		  
		     return betaIN;}
		 }
	     else //else, if this move is in our list play it first
	      if (retrivedEntry.getBestMove()!=null)	 
		 {
		     int size=validMoves.size();
			 for(int i=0;i<size;i++)
			     if (retrivedEntry.getBestMove().equals((Move)validMoves.get(i)))
				 {  
				  //move the first to be the first to be handled   
				  Move importantMove=(Move)validMoves.remove(i);
		                  validMoves.add(0,importantMove);
				  break;
				 }
		 }
	 }


     //start looking at the move list


     if (board.getTotalAliveCount(nextTurn)>SWITCH_STARTCOUNT)   ///consider switch moves only and only if there are less
     	 validMoves=Piece.getStandardValidMoves(board,nextTurn); //than SWITCH_STARTCOUNT left on the board
     if (validMoves.size()==0)
	 validMoves=Piece.getSwitchMoves(board,nextTurn);



     int best=(-WIN); //best move found so far, has the lowest number possible
     int value=0;                   //value of the child node, could be equal to anything in the beginning
     int size=validMoves.size();    //size of the validMoves  
     int alpha=alphaIN;             //alpha value for this level 
     int beta=betaIN;               //beta value for this level
     Move bestMove=null;            //has to be null, if it still is null after the 'for' loop
                                    //we can detect that our algorithm is not working correctly

     int hashType=HashEntry.HASHALPHA; //this node is treated as an alpha node to begin
                                       //with

     boolean pvFound=false;        //primary variation

     // having updated move list we can begin alpha-beta
     for(int i=0;i<size;i++)
      if (best<beta)	 

	 {
	  
     	     if (debug)
		 System.out.print("("+depth+") "+(Move)validMoves.get(i)+" ");

	     
	     if (best>alpha) { hashType=HashEntry.HASHEXACT; //this is an exact node
		               alpha=best;  //our alpha is the newly found value
			       pvFound=true; //pv node found
			       if (doneDepth==1)
				   secondMove=bestMove;
	                     }

      Piece.makeGeneratedMove(board,(Move)validMoves.get(i));	     
	     //PRIMARY VARIATION OPTIMIZATION

	     if (pvFound) {
		 value= (-1)*this.alphaBetaHashRec(depth-1,
							   Piece.getOpponentColor(nextTurn),-alpha-1,-alpha,2);
		 if ((value > alpha) && (value < beta)) // Check for failure.
		     value= (-1)*this.alphaBetaHashRec(depth-1,
							   Piece.getOpponentColor(nextTurn),-beta,-alpha,2);
	     } else
	     value= (-1)*this.alphaBetaHashRec(depth-1,
					    Piece.getOpponentColor(nextTurn),-beta,-alpha,2);
      Piece.undoMove(board,(Move)validMoves.get(i));
           
             if (value==UNKNOWN) return UNKNOWN;

	     if (value>best)   
		 {
		 best=value;
		 bestMove=(Move)validMoves.get(i);           //if this is a better move, take this one
		 
	         }
	     



	 }
      else //beta has been found
	  {
	      hashType=HashEntry.HASHBETA;
	      best=beta;
	      bestMove=(Move)validMoves.get(i);
	      break;
	  }



     //record to the hashTable before we return
     HashEntry putInEntry=new HashEntry(hashKey.key,bestMove,hashType,best,depth);
     boardTable.put(hashKey,putInEntry);
     //print the debug code on the screen
      if (debughash)
		  System.out.println("("+depth+"):"+hashKey.hashCode()+":"+putInEntry);

      if (searchInProgress)
	  return best;

      return UNKNOWN;
  }


  /**
   *  Standard Alpha-Beta algorithm, entrance method
   *  <br><b> board</b> Board to make a search on 
   *  <br><b> depth</b> Depth of seach
   *  <br><b> nextTurn</b>next player to move
   *  <br><b> alpha</b>lower term of alpha-beta
   *  <br><b> beta</b> higher term of alpha-beta
   *  @requires !Piece.isGameOver() && (depth>0)
   *  @returns the best move
   *  @throws RuntimeException if <code>requires</code> is not satisfied
  */



  public Move alphaBetaHash(int depth,int nextTurn,int alphaIN, int betaIN,int doneDepth){

      boolean printHashKey=false;  

      if (!searchInProgress)
	  return null;
      
    //get validMoves
    List validMoves = Piece.getValidMoves(board, nextTurn);

    //Messages
    if (debug)
	System.out.println("Upper Level:("+depth+") Number of moves: "+validMoves.size()+" alp: "+alphaIN+" be: "+betaIN);


    //check if it is the end of the game
    if (Piece.isGameOver(board,nextTurn,validMoves))
      throw new RuntimeException("BetterEngine.alphaBeta():Game is Over!");


    //if we have reached the end children, we evaluate the board
    //and return a value for it
    if (depth<=0) 
      throw new RuntimeException("BetterEngine.alphaBeta():Depth is non-positive ");

     //get the hashEntry which have the same hashKey as the board
    HashKey hashKey=new HashKey(board.zobristCode(nextTurn));
    HashEntry retrivedEntry=(HashEntry)boardTable.get(hashKey);
    
    if (printHashKey)
	System.out.println("k:"+hashKey.key+"i:"+(int)hashKey.key);
    

     //check if we can return this value

     if (retrivedEntry!=null)
     	 {   //check if the depth makes us use this node effectively
	     if (retrivedEntry.getKey()==hashKey.key &&depth<=retrivedEntry.getDepth()&&retrivedEntry.getBestMove()!=null)
		 {
	       	     if (debughashcut)
		     //Print out that we making a cut from the table
		     System.out.println("Cutting from the table");
		     
		     //since this board exists in the hashTable, even if itis an alpha, beta or exact node
		     //just check if we can make the suggested move and make it else 
		     //proceed normally
		     
		     boolean canMake=false;	
                     if (validMoves.contains(retrivedEntry.getBestMove())) 
			 canMake=true;   
		     
		    //if this move is in our validMove list then it means that we canMake this move	 
		    if (canMake) 	  
			{
			     Move gotMove=retrivedEntry.getBestMove();
			     
			     if (debug)
				 System.out.println("(root node) alphaBeta choose: "+gotMove);
			     return gotMove;
			}

		 }
	     else //else, if this move is in our list play it first
	      if (retrivedEntry.getBestMove()!=null)	 
		 {
		     int size=validMoves.size();
			 for(int i=0;i<size;i++)
			     if (retrivedEntry.getBestMove().equals((Move)validMoves.get(i)))
				 {  
				  //move the first to be the first to be handled
				  Move importantMove=(Move)validMoves.remove(i);
		                  validMoves.add(0,importantMove);
				  break;
				 }
		 }
 }


     //start looking at the move list
 
     if (board.getTotalAliveCount(nextTurn)>SWITCH_STARTCOUNT)   ///consider switch moves only and only if there are less
     	 validMoves=Piece.getStandardValidMoves(board,nextTurn); //than SWITCH_STARTCOUNT left on the board
     if (validMoves.size()==0)
	 validMoves=Piece.getSwitchMoves(board,nextTurn);

     int best=(-WIN); //best move found so far, has the lowest number possible
     int value=0;                   //value of the child node, could be equal to anything in the beginning
     int size=validMoves.size();    //size of the validMoves
     int alpha=alphaIN;             //alpha value for this level 
     int beta=betaIN;               //beta value for this level
     Move bestMove=null;            //has to be null, if it still is null after the 'for' loop
                                    //we can detect that our algorithm is not working correctly

     int hashType=HashEntry.HASHALPHA; //this node is treated as an alpha node to begin
                                       //with


     boolean pvFound=false;        //primary variation
     


	     for(int i=0;i<size;i++)
		 if (best<beta) 	 
		     {

			 if (debug)
			     System.out.print("("+depth+"):"+(Move)validMoves.get(i)+" ");
			 
			 
			 if (best>alpha) { 
			       hashType=HashEntry.HASHEXACT; //this is an exact node
		               alpha=best;  //our alpha is the newly found value
			       pvFound=true;
			       
			                 }
	     
			 if (debug)
			     System.out.println("("+depth+")"+"Called with alp:"+(alpha)+" be: "+(beta));

      		    Piece.makeGeneratedMove(board,(Move)validMoves.get(i)); //make the next move and modify the board
			 //PRIMARY VARIATION OPTIMIZATION

			 if (pvFound) {
			     value= (-1)*this.alphaBetaHashRec(depth-1,
								       Piece.getOpponentColor(nextTurn),-alpha-1,-alpha,1);
			     if ((value > alpha) && (value < beta)) // Check for failure.
				 value= (-1)*this.alphaBetaHashRec(depth-1,
									   Piece.getOpponentColor(nextTurn),-beta,-alpha,1);
			 } else
			 value= (-1)*this.alphaBetaHashRec(depth-1,
								Piece.getOpponentColor(nextTurn),-beta,-alpha,1);
                     Piece.undoMove(board,(Move)validMoves.get(i));  //undo the move, and revert back to inital board

			 if (value==UNKNOWN) return null; 		 
		   


			 if (value>best)   
			     {
				 best=value;
				 bestMove=(Move)validMoves.get(i);}          //if this is a better move, take this one
			
		     }
	     
		 else //beta has been found
		     {   bestMove=(Move)validMoves.get(i);
			 best=beta;    
			 hashType=HashEntry.HASHBETA;
			 break;
		     }


     
     //put this to the hashTable first
     HashEntry putInEntry=new HashEntry(hashKey.key,bestMove,hashType,best,depth);
     boardTable.put(hashKey,putInEntry);
     //print t he debug code on the screen
	      if (debughash)
		  System.out.println("("+depth+"):"+hashKey.hashCode()+":"+putInEntry);

      if (debug)
	  System.out.println("(root node) alphaBeta choose: "+bestMove);

      if (searchInProgress)
	  return bestMove;

      return null;
    
  }



    //***************************************************************************************
    //***************************************************************************************
    //*******************************   ITERATIVE DEEPING    **********************************
    //***************************************************************************************
    //***************************************************************************************
    //***************************************************************************************

    public void run()
    {   
	searchInProgress=true;
	searchFinished=false;

	List validMoves=Piece.getValidMoves(board,initialNextTurn);

	//dont spend time thinking about this guy
	if (validMoves.size()==1)
	    {
		foundMove=(Move)validMoves.get(0);
		
		searchFinished=true;
		searchInProgress=false;
		return;
	    }



	//*****Look into the hashTable first
	//**********************************
	//**********************************
    


	//get the hashEntry which have the same hashKey as the board
	HashKey hashKey=new HashKey(board.zobristCode(initialNextTurn));
	HashEntry retrivedEntry=(HashEntry)boardTable.get(hashKey);
    
	//if (printHashKey)
	//    System.out.println("k:"+hashKey.key+"i:"+(int)hashKey.key);
    

	//check if we can return this value
	if (retrivedEntry!=null && searchInProgress)
	    {   //check if the depth makes us use this node effectively
		if (retrivedEntry.getKey()==hashKey.key &&initialDepth<=retrivedEntry.getDepth()&&retrivedEntry.getBestMove()!=null)
		    {
			//if (debughashcut)
			    //Print out that we making a cut from the table
			//  System.out.println("Cutting from the table");
		     
			//since this board exists in the hashTable, even if itis an alpha, beta or exact node
			//just check if we can make the suggested move and make it else 
			//proceed normally
		    	 if (validMoves.contains(retrivedEntry.getBestMove())) 
			     {  secondMove=null;
			        foundMove=retrivedEntry.getBestMove();   
				searchFinished=true;
				return;
			     }

		    }}
	//**********************************
	//**********************************



        foundMove=null;
	Move bestFromDepth=null;

	for (int d=1;d<=initialDepth && searchInProgress;d++)
	    {
		System.out.println("BetterEngine.java: new depth:"+d);
		bestFromDepth=alphaBetaHash(d, initialNextTurn,-WIN,WIN , 0);
		if (bestFromDepth!=null)
		    foundMove=bestFromDepth;
	    }

     //if bestMove is null, there is an error in our algorithm
     if (foundMove==null)
	 {

	     foundMove=(Move)validMoves.get(0);
	     System.out.println("bestMove found so far is NULL: BIG ERROR IN PROGRAM");
	     // System.exit(0);
	 }

     searchFinished=true;

}









    //***************************************************************************************
    //***************************************************************************************
    //*******************************   QUIESCENT SEARCH    **********************************
    //***************************************************************************************
    //***************************************************************************************
    //***************************************************************************************

    /**Returns the quiescent seach value
     */

    private int makeQuies(Board board, int nextTurn, int alphaIN, int betaIN,int depth)
    {   

      if (!searchInProgress)
	  return UNKNOWN;

	//check for checkmates first

	List validMoves=Piece.getValidMoves(board, nextTurn);   //generate all the moves
	int size=validMoves.size();


	//check if it is the end of the game
	if (Piece.isTie(board,nextTurn,validMoves))
	    return 0;
	if (Piece.isWin(board,nextTurn,validMoves)) //only next moving player can win the game
	  return(WIN-1);

	//now that it is not a checkmate we can do quiescent search

	int alpha=alphaIN;
        int beta =betaIN;
 
	int val = evaluate(board,nextTurn);

	if (val >= beta)
	    return beta;

	if (val > alpha)
	    alpha = val;


	if (depth >= QUIES_DEPTH)
	    return(val);

	for (int i=0;i<size;i++)
	    if (((Move)validMoves.get(i)).getCapture() && searchInProgress)
		{ 
		    Piece.makeGeneratedMove(board,(Move)validMoves.get(i)); //make the next move and modify the board

		    val = -makeQuies(board, Piece.getOpponentColor(nextTurn), -beta, -alpha,depth+1);

		    Piece.undoMove(board,(Move)validMoves.get(i));  //undo the move, and revert back to inital board

		    if (val >= beta)
			return beta;
		    
		    if (val > alpha)
			alpha = val;
		}
	return alpha;
    }


    /**Our evaluate function
     */

    private int evaluate(Board board,int nextTurn)
    {
	//if (board.getBoardType()==Board.REGULAR)
	 return board.getDifference(nextTurn); 
	    //return Evaluator.evaluateBoard(board,nextTurn,0);
	 //else
	 //return CEvaluator.evaluateBoard(board,nextTurn,0);

    }


}
