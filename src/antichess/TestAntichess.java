/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import junit.framework.*;

public class TestAntichess extends TestSuite
{
  public static Test suite() { return new TestAntichess(); }
  public TestAntichess() { this("Test Antichess"); }
  public TestAntichess(String s)
  {
    super(s);
    //addTestSuite(TestBoard.class); 
    //addTestSuite(TestMove.class);
    //addTestSuite(TestTimer.class);
    //addTestSuite(TestGameController.class);
    //addTestSuite(TestPiece.class);
    //addTestSuite(TestEvaluator.class);
    //addTestSuite (TestNewEngine.class);
    //addTestSuite (TestCylinder.class);
    //addTestSuite (TestZobrist.class);
    addTestSuite(TestBetterEngine.class);
    
    
  }

  public static void main(String args[]) {
    System.out.println ("Please use the command: java junit.textui.TestRunner antichess.TestAntichess\nOR\nrunant runtest");
    System.out.println ("This command must be run from your main directory (which contains src/ and bin/ and build.xml etc)");
  }
}
