/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

/**
 *<p>GameMessage keeps the information about the changes in the flow
 * of an antichess game.
 *
 *<p>When a GameController is running a game, it notifies all its observers
 *with a GameMessage. GameMessages are immutable.
 **/

public class GameMessage{
  

  //FIELDS
  //PUBLIC
  /**constant value for a general message
   */
  public final static int MESSAGE=0;
  /**constant value for a move
   */
  public final static int MOVE=1;

  //PRIVATE
  private int type;

  //fields for a move
  private Move move;
  private int color;

  //fields for a message

  private String messageBody;

  //CONSTRUCTORS
  /**Constructs a message of a GameMessage.MOVE type
   *Holds information about the actual move and the color making move
   */
  public GameMessage(Move move, int color){
    this.type=MOVE;
    this.move=move;
    this.color=color;
    this.messageBody=null;
  }
  /**Constructs a message of a GameMessage.MESSAGE type
   *Holds information about message text
   */
  public GameMessage(String messageBody){
    this.type=MESSAGE;
    this.move=null;
    this.color=0;
    this.messageBody=messageBody;
  }
  
  //OBSERVERS
  
  /**Returns type
   *@returns type
   */
      public int getType(){
      return type;}

  /**Returns move
   *@returns move
   */
      public Move getMove(){
      return move;}

  /**Returns color
   *@returns color
   */
      public int getColor(){
      return color;}
  

  /**Returns message
   *@returns message
   */
      public String getMessage(){
      return messageBody;}

}
