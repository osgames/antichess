/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

public class HashEntry{

  //FIELDS
  //public fields
    
    public static final int HASHEXACT=0;
    public static final int HASHBETA=1;
    public static final int HASHALPHA=2;
 
  //int[] hashStr;
  private Move bestMove;
  private int val;
  private int type;
  private int depth;
  private long key;  

  //CONSTRUCTORS
  public HashEntry(long key,Move bestMove, int type, int val, int depth)
  {
    // this.hashStr=hashStr;
    if (bestMove!=null)
	this.bestMove=new Move(bestMove.toString());
    else this.bestMove=null;

    this.key=key;
    this.val=val;
    this.type=type;
    this.depth=depth;
  }
  
    
  //OBSERVERS
 //@returns key
    public long getKey()
    {return key;}
    

  //@returns bestMove
  public Move getBestMove()
  {return bestMove;}
  //@returns the type of the hash entry
  public int getType()
  {return type;}
  //@returns val
  public int getValue()
  {return val;}
  //@returns depth of calculation that resulted in bestMOve
  public int getDepth()
  {return depth;}
    //@returns a string rep of this hashEntry
  public String toString()
    { String result;
      return (result="i:"+(int)key+"k:"+key+":M:"+bestMove+":v:"+val+":d:"+depth+":t:"+type);}
    
}
