/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import java.util.*;
import java.io.*;

/**
 * Interactive loop for textui
 */

public class TextUI2 implements Observer{

  GameController gc;
  String filename;
  String[] colorStr = new String[5];
  // The current board string, used to see if any changes has been made to board
  String boardString = "";
  HumanPlayer humanPlayer;
  
  private Thread onGoingGame;

  /**********************************************************/
  /**********************************************************/
  // CONSTRUCTORS
  /**********************************************************/
  /**********************************************************/

  public TextUI2 (){
    colorStr [Piece.WHITE] = "WHITE";
    colorStr [Piece.BLACK] = "BLACK";
    colorStr [Piece.RED] = "RED";
    colorStr [Piece.BLUE] = "BLUE";
    startUp();
  }
  
  public void startUp(){
    onGoingGame = null;
    gc = null;
    getValues();
    runGame();
    //System.out.println ("loop done");
    if (gc.getPlayerType(gc.getNextTurn()) == gc.HUMAN){
      // Get the first move if necessary
      //delay to allow gc to start
      for(long i=0;i<50000000;i++);
      for(long i=0;i<50000000;i++);
      moveMade();
    }
  }

  /** Asks user to read from file or get other information
   * about the game
   */
  public void getValues(){
    gc = null;
    
    try{
      BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
      String yn = "";
      boolean fourplayers = false;
      while (!yn.equals("y") && !yn.equals("n")){
	System.out.println ("Do you want to play 4 player game? (y/n)");
	yn = input.readLine ();
      }
      if (yn.equals ("y"))
	fourplayers = true;
      else
	yn = "";

      // Ask if the user wants to load a gamefile
      while (!yn.equals("y") && !yn.equals("n")){
	System.out.println ("Do you want to open a gamefile? (y/n)");
	yn = input.readLine ();
	//System.out.println ("You have entered: " + yn);
	
	if (yn.equals( "y")){
	  System.out.println ("Enter the filename");
	  filename = input.readLine ();
	  
	} else if (yn.equals("n")){
	  // Not loading from file, so load from standard file
	  System.out.println ("Loading standard game file...");
	  filename = gc.standardGameFile;
	  
	} else {
	  System.out.println ("Invalid input.");
	}
      }

      if (fourplayers){
	// Fourplayers
	System.out.println ("Creating 4 player game");
	gc = new GameController (4);
	gc.setTimedGame(false); // disable timing, enable infinite time for textui
	System.out.println ("4 player game created");
      } else {
	// Two Players
	// Initialize a new gameController with the given file name
	gc = new GameController (filename);
	gc.setTimedGame(false); // disable timing, enable infinite time for textui
	System.out.println ("Game loaded from " + filename);
      }
      
      // Set board type
      if (fourplayers)
	yn = "n";
      else
	yn = "";
      while (!yn.equals("y") && !yn.equals("n")){
	System.out.println ("The current board type is: " +
			    (gc.getBoardType() == Board.CYLINDER?
			     "Cylinder. ": "Regular. "));
	System.out.println ("Do you want to change the board type? (y/n)");
	yn = input.readLine ();
	//System.out.println ("You have entered: " + yn);
      }
      if (yn.equals ("y")){
	if (gc.getBoardType() == Board.CYLINDER){
	  gc.setBoardType (Board.REGULAR);
	  
	}
	else {
	  gc.setBoardType (Board.CYLINDER);
	}
	  
	System.out.println ("The current board type is now: " +
			    (gc.getBoardType() == Board.CYLINDER?
			     "Cylinder. ": "Regular. "));
      }

      // Loops through white then black, and get the player type
      // and level (for machine player only)
      
      int color, playerLevel = 0, playerType = -1;
      String playerTypeStr, playerColorStr;
      
      for(color = Piece.FIRSTCOLOR; color <= Piece.LASTCOLOR; color ++){

	// Set the color string for output purposes
	//playerColorStr = (color == Piece.WHITE ? "White" : "Black");
	playerColorStr = Piece.playerColorString[color];
      

	// Get the player type and level
	playerTypeStr= "";
	while (!playerTypeStr.equals("h") && !playerTypeStr.equals("m")){
	  System.out.println ("Please enter " + playerColorStr + " player type (h/m):");

	  playerTypeStr = input.readLine ();

	  
	  if (playerTypeStr.equals("m")){
	    // Machine Player
	    playerType = gc.MACHINE;
	    String lvlStr = "";

	    String expected[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"};
	    // Get level for machine player
	    //while (!lvlStr.equals("0") &&!lvlStr.equals("1") &&!lvlStr.equals("2")){
	    boolean ok=false;
	    for (;;){
	      if (fourplayers){
		lvlStr = "11";
		break;
	      }
	      System.out.println (playerColorStr + " player level (0 = level 4; 1-10 = depths; 11 = dumb):");
	      //System.out.println (playerColorStr + " player level (0 to 11):");
	      lvlStr= input.readLine();
	      ok=false;
	      for (int i=0;i<expected.length;i++){
		if (lvlStr.equals(expected[i])){
		  ok=true;
		  break;
		}
	      }
	      if (ok)
		break;
	    }
	    
	    playerLevel = Integer.parseInt(lvlStr);
	    break;
	  }
	  else if (playerTypeStr.equals("h")){
	    // Human player. Sets level = 0
	    playerType = gc.HUMAN;
	    playerLevel = 0;
	    break;
	  } else {

	    // Else invalid input, loop
	    System.out.println("Invalid input.");
	  }
	}

	// Done getting information. set the player in gamecontroller
	gc.setPlayer (color, playerType, playerLevel);
      }
	
    } catch (Exception e){
      throw new RuntimeException ("Error getting values:\n " + e);
    }

    // We will always have a human player even if it is not used
    humanPlayer = gc.getHumanPlayer();
    if (humanPlayer == null)
      throw new RuntimeException ("humanplayer should not be null");
    
  }

  /** Given s, returns a string with s repeated a number of times
   */
  public String repeat (String s, int times){
    String x = "";
    int i;
    for (i=0;i<times;i++)
      x+=s;
    return x;
  }
  
  /** Displays the current board **/
  public void displayBoard(){
    Board b = gc.getBoard();
    int squareWidth = 4; // space between two borders
    int colNum, rowNum;

    colNum = b.MAXCOL - b.MINCOL + 1;
    rowNum = b.MAXROW - b.MINROW + 1;
    int r,c,i;

    // Top line
    System.out.println ("  " + repeat ("+" + repeat("-", squareWidth), colNum) + "+");
    for (r = b.MAXROW; r >= b.MINROW; r--){
      
      // blank line
      System.out.println ("  " + repeat ("|" + repeat(" ", squareWidth), colNum) + "|");

      // row number
      if (r+1<10)
	System.out.print (" "); // blank space
      System.out.print (r+1);
      for (c= b.MINCOL; c<=b.MAXCOL; c++){
	Piece p = b.getPieceAt (c, r);
	String ch = "  ";
	if (p==null){
	  ch = "XX";
	} else if (p.getType() == p.EMPTY){
	  ch = "  ";
	} else {
	  ch = p.getChar() + "";// + (p.WHITE == p.getColor() ? " " : "*");
	  if (p.getColor () == p.WHITE) {
	    ch += "w";
	  } else if (p.getColor () == p.BLACK){
	    ch += "b";
	  } else if (p.getColor () == p.RED){
	    ch += "r";
	  } else if (p.getColor () == p.BLUE){
	    ch += "*";
	  }
	  
	}
	System.out.print ("|" + repeat(" ", (squareWidth-1)/2) +
			  ch + repeat(" ", (squareWidth-1)/2));
	
      }
      System.out.println ("|");
      
      // blank line
      System.out.println ("  " + repeat ("|" + repeat(" ", squareWidth), colNum) + "|");
      // lower border
      System.out.println ("  " + repeat ("+" + repeat("-", squareWidth), colNum) + "+");
    }

    // Column letters
    String cstr = "abcdefghijklmnopqrstuvwzyz";
    System.out.print (" ");
    for (c= b.MINCOL; c<=b.MAXCOL; c++){
      String ch = " " + cstr.charAt(c);
      System.out.print (" " + repeat(" ", (squareWidth-1)/2) +
			ch + repeat(" ", (squareWidth-1)/2));
      
    }
    System.out.println("");

  }

  /** Display timeleft **/
  private void displayTime(){
    for (int i=Piece.FIRSTCOLOR; i<=Piece.LASTCOLOR; i++){
      System.out.println (Piece.playerColorString [i] + " time left: " + gc.getTimeLeft(i) + " ms");
    }
    System.out.println ("Please note that the timers are just displayed for save/load game purposes, and TextUI games have infinite time.");
    
  }
  
  /** Displays the time left and board
   **/
  private void displayGame(){
    displayTime();
    displayBoard();
    //System.out.print (gc.getBoard());
  }

  /** Starts and runs the game
   * @returns true if to exit program, false to start new game
   **/
  private void runGame (){
    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    if (gc==null)
      throw new RuntimeException("GameController is null just before the game starts!");
    if (onGoingGame!=null)
      throw new RuntimeException("GameController has a game running before a new game starts");
    
    //start observing what might be happening in the game
    gc.addObserver(this);

    if (onGoingGame==null)
      (onGoingGame=new Thread(gc)).start();
    else
      throw new RuntimeException("GameController has a game running before a new game starts");

  }

  /** This method is called whenever the gamecontroller object has an update
   * in the flow of the game. This is a required method by java.util.Observer
   * interface.
   * @requires o instance of GameController && o == gc && o!=null && arg instance of GameMessage
   * @effects calls functions accordingly
   */
  public void update(Observable o,Object arg){
    boolean debug = false;
    if (debug) System.out.println ("Update");
    
    //check if a game is going on at all
    if (gc==null) return;
    //check if the call came from our game
    if (o==null) return;
    if (!o.equals(gc)) return;
    if (!(arg instanceof GameMessage)) return;
    
    //act now depending on what has happened to the game.
    
    GameMessage gm = (GameMessage) arg;
    if (gm.getType() == gm.MOVE)
      moveMade();
    else
      breakGame(gm);

  }

  /** Ends a game
   * @param gm GameMessage containing the information about the end game condition
   **/
  private void breakGame(GameMessage gm){
    String msg = gm.getMessage();
    System.out.println (msg);
    try{
      BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
      String yn = "";
      while (!yn.equals("y") && !yn.equals("n")){
	System.out.println ("Do you want to quit? (y/n)");
	yn = input.readLine ();
      }

      if (yn.equals ("y")){
	System.exit (0);
      } else {
	startUp();
      }
    } catch (Exception e){
      throw new RuntimeException ("Error in breakGame: " + e);
    }
  }
  
  /** Given a move made, this method displays the new board,
   *  and will check if the human player can make a move in the next turn.
   *  If the human player can make a move, it will call getHumanMove
   * @param arg the move made. null if starting a new game and first player to move
   */
  private void moveMade(){
    boolean debug = false;
    if (debug) System.out.println ("Move made");
    displayGame();

    // If human player's list of valid moves is not empty,
    // It means that human player can make a move
    //if (humanPlayer.getMoveList()!=null){
    if (humanPlayer.getMyTurn()){
      getHumanMove();
    }
  }

  /** Gets a user input move from standard input
   */
  private void getHumanMove(){
    boolean debug = false;
    
    int nextTurn = gc.getNextTurn();
    System.out.println (colorStr[nextTurn] + "'s Turn");
    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    String s;
    Move m = null;
    
    // Get User Input
    
    // get input from system.in
    try {
      for (;;){
	
	System.out.println ("Enter a move (a1-a2) or one of the following commands:\n" +
			    "(quit/save/load/new/printboard/printmoves): ");
	s = input.readLine();
	if (s.equals ("quit")) {
	  System.exit(0);
	} else if (s.equals ("save")){
	  saveGame();
	  continue;
	} else if (s.equals ("load")){
	  startUp();
	  return;
	} else if (s.equals ("new")){
	  startUp();
	  return;
	} else if (s.equals ("printboard")){
	  printBoard();
	  continue;
	} else if (s.equals ("printmoves")){
	  printMoves();
	  continue;
	}
	
	// Process move
	m = new Move(s);
	if (humanPlayer.getMoveList().contains (m)){
	  break;
	}
	else {
	  displayGame();
	  System.out.println ("Invalid Move: " + s);
	}
      }
    } catch (Exception e){
      throw new RuntimeException ("Error in runGame: " + e);
    }
    humanPlayer.setMove (m);

    if (debug) System.out.println ("Move entered: " + m);
    if (debug) System.out.println ("GetMyTurn: " + humanPlayer.getMyTurn());
  }
  

  /**********************************************************/
  /**********************************************************/
  // OBSERVERS
  /**********************************************************/
  /**********************************************************/

  public void saveGame(){
    
    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    String s;
    try{
      System.out.println ("Enter filename:");
      s = input.readLine();
      gc.saveGameToFile(s);
      System.out.println ("Game saved to " + s);
    } catch (Exception e){
      System.out.println ("Error saving game.\n" + e);
    }
  }

  public void printMoves(){
    int turn = gc.getNextTurn();
    Board b = gc.getBoard();
    List validMoves = Piece.getValidMoves(b, turn);
    List tmp = new ArrayList();
    for (int i=0; i<validMoves.size(); i++){
      tmp.add (((Move)validMoves.get(i)).toString());
    }

    Collections.sort (tmp);
    System.out.println ("Possible moves: ");
    for (int i=0; i<tmp.size(); i++){
      System.out.print ((String) tmp.get(i) + " ");
      if ((i+1)%10==0) System.out.println("");
    }
    System.out.println ("");
  }

  public void printBoard (){
    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    String s;
    try{
      s = "";
      if (Piece.LASTCOLOR != Piece.BLACK)
	s = "y";
      while (!s.equals ("y") && !s.equals ("n")) {
	System.out.println ("Do you want to see the 8x8 chessboard?\n['y' for 8x8; 'n' for gamefileformat]:");
	s = input.readLine();		
      }
      if (s.equals ("y")){
	displayGame();
      } else {
	System.out.println ("Board string in GameFileFormat:\n"+gc.getBoardString());
      }

    } catch (Exception e){
      System.out.println ("Error printing board.\n" + e);
    }
    
  }

  /**********************************************************/
  /**********************************************************/
  // OTHER HELPER METHODS
  /**********************************************************/
  /**********************************************************/

    public static void main(String args[]) {
      TextUI2 text = new TextUI2();
      
      // Infinite loop. Game has to be terminated by user by
      // entering quit
      for (;;);
      
    }
  
}
