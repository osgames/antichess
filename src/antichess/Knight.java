/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import java.util.*;

/**
 * This class contains the rules that apply to Knight.
 **/

public class Knight extends Piece{
  /** Creates a new piece.
   * @param letter the character representation of the piece:
   * @param board the board the piece will be assigned to
   * @param row starting row of piece
   * @param col starting col of piece
   * @param color color
   * @requires letter = {P | p | K | k | N | n | R | r | Q | q | B | b}
   **/
  public Knight (char letter, Board b, int col, int row, int color){
    super (Piece.KNIGHT, letter, b, col, row, color);
    int[] myDCol = { -2, -2, -1, -1,  1,  1,  2,  2};
    int[] myDRow = { -1,  1, -2,  2, -2,  2, -1,  1};
    dcol = myDCol;
    drow = myDRow;
    slide = false;
    offsets = 8;
  }
  /**
   * Creates a copy to a new board
   */
  public Piece copyPiece (Board b){
    Piece p = new Knight (this.getChar(), b, this.getCol(), this.getRow(), this.getColor());
    p.setLastLocation (this.getLastCol(), this.getLastRow());
    p.setMoved (this.getMoved());
    return p;
  }
}
