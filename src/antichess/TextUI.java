/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import java.util.*;
import java.io.*;

/**
 * Interactive loop for textui
 */

public class TextUI /*implements Observer*/{

  GameController gc;
  String filename;
  String[] colorStr = new String[4];
  // The current board string, used to see if any changes has been made to board
  String boardString = "";
  HumanPlayer humanPlayer;
  private Thread onGoingGame;

  /**********************************************************/
  /**********************************************************/
  // CONSTRUCTORS
  /**********************************************************/
  /**********************************************************/

  public TextUI (){
    colorStr [Piece.WHITE] = "WHITE";
    colorStr [Piece.BLACK] = "BLACK";
    startUp();
  }
  
  public void startUp(){
    interactiveLoop();
  }

  /** Performs a loop in which user can enter commands
   */
  public void interactiveLoop(){
    boolean debug = false;
    
    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    String inputStr;
    List commandList = null;
    inputStr = "";
    for (;;){
      checkGameOver();
      try {
	inputStr = input.readLine();
	commandList = parseCommand(inputStr);
	if (debug) System.out.println (commandList);
	
	if (commandList.size()==0){
	  displayInputError();
	  continue;
	}
	
	String command = (String) commandList.get(0);
	int size = commandList.size();
	if (command.equals ("StartNewGame") && size == 4){
	  startNewGame(commandList);
	} else if (command.equals ("SaveGame") && size == 2){
	  saveGame (commandList);
	} else if (command.equals ("LoadGame") && size == 2){
	  loadGame (commandList);
	} else if (command.equals ("GetNextMove") && size == 1){
	  getNextMove();
	} else if (command.equals ("MakeNextMove") && size == 1){
	  makeNextMove ();
	} else if (command.equals ("MakeMove") && size == 2){
	  makeMove (commandList);
	} else if (command.equals ("PrintBoard") && size == 1){
	  printBoard ();
	} else if (command.equals ("IsLegalMove") && size == 2){
	  isLegalMove (commandList);
	} else if (command.equals ("PrintAllMoves") && size == 1){
	  printAllMoves ();
	} else if (command.equals ("QuitGame") && size == 1){
	  quitGame();
	} else {
	  displayInputError();
	  continue;
	}
      } catch (Exception e){
	throw new RuntimeException ("Error in interactive loop: " + e);
      }
	
    }
  }

  /** Starts a new game. StartNewGame [player] [player] [board-type]:
      Each player is denoted as "human" or "computer."  The board-type is
      either C (for cylindrical) or (R) for regular or non-cylindrical.  The
      first player is white and the second player is black.  For example,
      StartNewGame computer human C should start a new game
      with the computer playing as white and the human playing as black on a
      cylinder board.  The system should output New cylinder game
      started or New regular game started on its own line.
  */
  public void startNewGame(List commandList){
    boolean debug = false;

    // Get white player
    if (debug) System.out.println (commandList);
    String whitePlayerString = (String) commandList.get(1);
    if (!whitePlayerString.equals ("human") &&
	!whitePlayerString.equals ("computer")){
      displayInputError();
      return;
    }
    int whitePlayer = (whitePlayerString.equals ("human") ? gc.HUMAN : gc.MACHINE);

    // Get Black player
    String blackPlayerString = (String) commandList.get(2);
    if (!blackPlayerString.equals ("human") &&
	!blackPlayerString.equals ("computer")){
      displayInputError();
      return;
    }
    int blackPlayer = (blackPlayerString.equals ("human") ? gc.HUMAN : gc.MACHINE);

    // Check board type
    String boardTypeString = (String) commandList.get(3);
    if (!boardTypeString.equals ("C") &&
	!boardTypeString.equals ("R")){
      displayInputError();
      return;
    }

    if (boardTypeString.equals ("C")){
      System.out.println("New cylinder game started");
    } else {
      System.out.println("New regular game started");
    }

    // Create a new game controller
    // Assume level is always 0
    
    gc = new GameController();
    gc.setPlayer (Piece.WHITE, whitePlayer, 0);
    gc.setPlayer (Piece.BLACK, blackPlayer, 0);
      
	
  }

  /** Saves a game. SaveGame [filename]: The system should
      save the game to the given filename and report Game
      saved on its own line. The time left for each player
      is unspecified.
  **/
  public void saveGame (List commandList){
    // If a game is not in progress, exit
    if (gc == null) {
      //System.out.println ("Input error: no game in progress");
      displayInputError();
      return;
    }

    String filename = (String) commandList.get(1);
    gc.saveGameToFile (filename);
    System.out.println ("Game saved");
  }

  /** Loads a new game. 
      LoadGame [filename]: The system should
      load the game from the given filename. Once the files is
      loaded, print Game loaded on its own line. You
      should report Corrupt file if the file does not
      have a correct format.  We will not require you to determine if the board is legal.  If no game is currently in progress from
      a previously executed StartNewGame or LoadGame command,
      then assume a human-human game. There is no sense of time left
      for each player in the TextUI, so the time left data in the
      file format is ignored.
  **/   
  public void loadGame(List commandList){
    String filename = (String) commandList.get(1);

    // If a game is already in progress, we use the players defined
    // We make a backup of gc here
    GameController prevgc = gc;

    try{
      gc = new GameController (filename);
    } catch (Exception e){
      System.out.println ("Corrupt file");
      return;
    }
		
    // If a game is already in progress, we use the players defined
    // otherwise, we assume two human players
    if (prevgc == null){
      gc.setPlayer (Piece.WHITE, gc.HUMAN, 0);
      gc.setPlayer (Piece.BLACK, gc.HUMAN, 0);
    } else{
      gc.setPlayer (Piece.WHITE, prevgc.getPlayerType(Piece.WHITE), prevgc.getPlayerLevel(Piece.WHITE));
      gc.setPlayer (Piece.BLACK, prevgc.getPlayerType(Piece.BLACK), prevgc.getPlayerLevel(Piece.BLACK));
    }

    
    System.out.println ("Game loaded");
  }

  /** GetNextMove:   If
      this command is called during a human player's turn, the
      command prints Human turn on its own line.
      If this command is called during a machine player's turn, print
      on its own line the next move it believes to be the best.
      The printed move should be in the format described in the
      MachinePlayer specification. 
  **/
  public void getNextMove(){
    // If a game is not in progress, exit
    if (gc == null) {
      //System.out.println ("Input error: no game in progress");
      displayInputError();
      
      return;
    }

    if (gc.getPlayerType(gc.getNextTurn()) == gc.HUMAN){
      System.out.println ("Human turn");
    } else {
      System.out.println (Piece.getValidMoves (gc.getBoard(),
					       gc.getNextTurn()).get(0));
    }
  }

  /** MakeNextMove: If it is a computer player's
      turn, the system performs the move that
      GetNextMove would return.  If it is a human player's turn,
      the system should print
      Please specify human move on its own line.
  **/     
  public void makeNextMove(){
    // If a game is not in progress, exit
    if (gc == null) {
      //System.out.println ("Input error: no game in progress");
      displayInputError();
      return;
    }
		
    if (gc.getPlayerType(gc.getNextTurn()) == gc.HUMAN){
      System.out.println ("Please specify human move");
    } else {
      Move m = (Move) (Piece.getValidMoves (gc.getBoard(), gc.getNextTurn()).get(0));
      Piece.makeMove (gc.getBoard(), m);
      gc.toggleTurn();
    }
  }

  /** MakeMove [move]: Perform the move
      specified by the String move, in the format
      specified in the MachinePlayer specification. If the move is
      not legal, the system should print, on its own line,
      Illegal move and not perform the move.  If the
      move is legal, the system should perform the move and print the
      move back, on its own line.
  **/
  public void makeMove(List commandList){
    // If a game is not in progress, exit
    if (gc == null) {
      //System.out.println ("Input error: no game in progress");
      displayInputError();
      return;
    }

    // Make Move should not be called during computer's turn
    if (gc.getPlayerType(gc.getNextTurn()) == gc.MACHINE){
      //System.out.println ("Please use MakeNextMove or GetNextMove on computer's turn");
      return;
    }
      
    int turn = gc.getNextTurn();
    Board b = gc.getBoard();
    Move m = new Move ((String) commandList.get(1));
    List validMoves = Piece.getValidMoves(b, turn);
    if (!Piece.isValidMove (b, m, turn)) {
      System.out.println ("Illegal move");
      return;
    } else {
      System.out.println (m);
    }

    Piece.makeMove (b, m, validMoves);
    gc.toggleTurn();
  }

  /** IsLegalMove [move]: System should print,
      on its own line, either "legal" or "illegal" to specify if the
      move is a legal next move.
  **/
  public void isLegalMove(List commandList){
    // If a game is not in progress, exit
    if (gc == null) {
      //System.out.println ("Input error: no game in progress");
      displayInputError();
      return;
    }

    int turn = gc.getNextTurn();
    Board b = gc.getBoard();
    Move m = new Move ((String) commandList.get(1));
    if (!Piece.isValidMove (b, m, turn)) {
      System.out.println ("Illegal");
      return;
    } else {
      System.out.println ("Legal");
    }
  }

  /** PrintAllMoves: System should, in
      alphanumeric order, print all legal moves for the next player.
      Each move should each appear on its own line.
  **/
  public void printAllMoves(){
    // If a game is not in progress, exit
    if (gc == null) {
      //System.out.println ("Input error: no game in progress");
      displayInputError();
      return;
    }

    int turn = gc.getNextTurn();
    Board b = gc.getBoard();
    List validMoves = Piece.getValidMoves(b, turn);
    List tmp = new ArrayList();
    for (int i=0; i<validMoves.size(); i++){
      tmp.add (((Move)validMoves.get(i)).toString());
    }

    Collections.sort (tmp);
    for (int i=0; i<tmp.size(); i++){
      System.out.println ((String) tmp.get(i));
    }
  }

  /** QuitGame: Prints (on
      its own line) Exiting game and terminates the game.
  **/
  public void quitGame(){
    // If a game is not in progress, exit
    if (gc == null) {
      displayInputError();
      return;
    }
    
    System.out.println ("Exiting game");
    System.exit(0);
  }
		 
  /**
   * Checks if the game is over
   */
  private boolean checkGameOver(){
    // check if a game is in progress
    if (gc == null)
      return false;

    Board board = gc.getBoard();
    int nextTurn = gc.getNextTurn();
    List validMoves = Piece.getValidMoves (board, nextTurn);
    if (Piece.isGameOver (board, nextTurn, validMoves)){
      if (Piece.isTie (board, nextTurn, validMoves)){
	System.out.println ("Draw game.");
      } else { // Assume that it is a win
	System.out.println ((nextTurn == Piece.WHITE ? "White" : "Black")+ " Player has won"); 
      }

      // Gameover, so set gc to nul
      gc = null;
      return true;
    }
    return false;
  }

  /** Displays an error message **/
  public void displayInputError(){
    System.out.println ("Input error");
  }
  /** Converts a command string into a list of strings
   */
  public List parseCommand(String s){
    StringTokenizer st = new StringTokenizer (s, " ");
    List l = new ArrayList();
    while (st.hasMoreTokens()) {
      l.add(st.nextToken());
    }
    return l;

  }

  
  /** Displays the board in the game format
      PrintBoard: System should print the
      current "state" of the game to the screen using the same format
      as if it were being saved to a file. Include a newline at the
      end of the output also.  The time left per player is unspecified.
  **/
  private void printBoard(){
    System.out.println (gc.getBoardString());
  }

  /** Starts and runs the game
   * @returns true if to exit program, false to start new game
   **/
  private void runGame (){
    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    if (gc==null)
      throw new RuntimeException("GameController is null just before the game starts!");
    if (onGoingGame!=null)
      throw new RuntimeException("GameController has a game running before a new game starts");
    
    //start observing what might be happening in the game
    //gc.addObserver(this);

    if (onGoingGame==null)
      (onGoingGame=new Thread(gc)).start();
    else
      throw new RuntimeException("GameController has a game running before a new game starts");

  }
  

  public static void main(String args[]) {
    TextUI textUI = new TextUI();
    textUI.interactiveLoop();
  }
  
}
