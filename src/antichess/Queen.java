/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import java.util.*;

/**
 * This class contains the rules that apply to Queen.
 **/

public class Queen extends Piece{
  /** Creates a new piece.
   * @param letter the character representation of the piece:
   * @param board the board the piece will be assigned to
   * @param row starting row of piece
   * @param col starting col of piece
   * @param color color
   * @requires letter = {P | p | K | k | N | n | R | r | Q | q | B | b}
   **/
  public Queen (char letter, Board b, int col, int row, int color){
    super (Piece.QUEEN, letter, b, col, row, color);
    int[] myDCol = { -1, -1, -1,  0,  0,  1,  1,  1};
    int[] myDRow = { -1,  0,  1, -1,  1, -1,  0,  1};
    dcol = myDCol;
    drow = myDRow;
    slide = true;
    offsets = 8;
  }

  /**
   * Creates a copy to a new board
   */
  public Piece copyPiece (Board b){
    Piece p = new Queen (this.getChar(), b, this.getCol(), this.getRow(), this.getColor());
    p.setLastLocation (this.getLastCol(), this.getLastRow());
    p.setMoved (this.getMoved());
    return p;
  }

  public boolean canAttackLocation (int targetCol, int targetRow){
    // If cylinder, we cannot really calculate diagonal
    if (this.getBoard().getBoardType() == Board.CYLINDER)
      return super.canAttackLocation (targetCol, targetRow);
    
    int rowDif = Math.abs(this.getRow() - targetRow);
    int colDif = Math.abs(this.getCol() - targetCol);

    // Must be same diagonal, same row or same column
    if (rowDif != colDif && 
	rowDif != 0 && colDif != 0)
      return false;
    return super.canAttackLocation (targetCol, targetRow);
  }
  
  /** Undos a queen promotion move **/
  public void undoOtherMove (Move move){
    int startRow = move.getStartRow();
    int startCol = move.getStartCol();
    Board b = this.getBoard();
    int color = this.getColor();

    // 3. Swap back
    this.swapPieces (startCol, startRow);
    
    // Zobrist delete queen
    Zobrist z = b.getZobrist();
    z.deletePiece (b, Piece.QUEEN, color, startCol, startRow);
    
    // Zobrist add pawn
    z.addPiece (b, Piece.PAWN, color, startCol, startRow);

    // 2. Register pawn back
    b.registerPiece (startCol, startRow, move.modifiedPieces[0]);
    
    // 1. restore last piece info
    move.restoreLastPieces (b);
  }

  /* * Overrides Piece.undoCaptureMove.
   * Undos promotion capture move
   **/
  public void undoCaptureMove (Move move){
    if (move.getPromote()){
      int startCol = move.getStartCol();
      int startRow = move.getStartRow();
      int destRow = move.getDestRow();
      int destCol = move.getDestCol();
      int color = this.getColor();
      
      // Register back both pawn and captured

      Board b = this.getBoard();

      // 3. registerd captured back 
      Piece p2 = move.modifiedPieces[1];
      b.registerPiece (destCol, destRow, p2);


      // 2. registerd pawn back 
      b.registerPiece (startCol, startRow, move.modifiedPieces[0]);

      
      // 1. restore last piece info
      move.restoreLastPieces (b);
      
      // Zobrist add pawn
      Zobrist z = b.getZobrist();
      z.addPiece (b, Piece.PAWN, color, startCol, startRow);
      
      // Zobrist delete queen
      z.deletePiece (b, Piece.QUEEN, color, destCol, destRow);

      // Zobrist add captured
      z.addPiece (b, p2.getType(), p2.getColor(), destCol, destRow);
    } else {
      // Normal capture
      super.undoCaptureMove (move);
    }
  }
  
}
