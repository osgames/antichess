/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import java.util.*;

/**
 * This class contains the rules that apply to Rook.
 **/

public class Rook extends Piece{
  /** Creates a new piece.
   * @param letter the character representation of the piece:
   * @param board the board the piece will be assigned to
   * @param row starting row of piece
   * @param col starting col of piece
   * @param color color
   * @requires letter = {P | p | K | k | N | n | R | r | Q | q | B | b}
   **/
  public Rook(char letter, Board b, int col, int row, int color){
    super (Piece.ROOK, letter, b, col, row, color);
    int[] myDCol = { -1,  1,  0,  0};
    int[] myDRow = {  0,  0, -1,  1};
    dcol = myDCol;
    drow = myDRow;
    slide = true;
    offsets = 4;
  }
  /**
   * Creates a copy to a new board
   */
  public Piece copyPiece (Board b){
    Piece p = new Rook (this.getChar(), b, this.getCol(), this.getRow(), this.getColor());
    p.setLastLocation (this.getLastCol(), this.getLastRow());
    p.setMoved (this.getMoved());
    return p;
  }

  /** overrides Piece.canAttackLocation **/
  public boolean canAttackLocation (int targetCol, int targetRow){
    int rowDif = Math.abs(this.getRow() - targetRow);
    int colDif = Math.abs(this.getCol() - targetCol);

    // Must be either same row or same column
    if (rowDif != 0 && colDif != 0)
      return false;
    return super.canAttackLocation (targetCol, targetRow);
  }
}
