/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

public class HashKey{

  //FIELDS
  //public fields
    public long key;

  public HashKey(long key)
  {
      this.key=key;
  }
  

  //OBSERVERS
  //@returns bestMove
  public int hashCode()
  {return (int)key;}

  public boolean equals(Object o)
    {if (!(o instanceof HashKey))
	return false;
    return (((HashKey)o).key==this.key);}
  
}
