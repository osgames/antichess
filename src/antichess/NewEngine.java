/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import java.util.*;
/**
 * <p>
 * NewEngine represents the player engine
 *
 * <p>
 * NewEngine is a class with static methods
 */
public class NewEngine{


  private static boolean debug=false;
  private static boolean debughash=false;  
  private static boolean debughashcut=false;  
  /**
   * Standard NegaMax algorithm, recursive method (for children nodes)
   * It is necessary to make a distinction between the entrance method which runs
   * on the root node and the recursive method which runs on children nodes.
   * This distinction allows us to return from the recursion if depth is given 
   * less than 0 or if it is an endgame situation (which shoudl be detected by GameController)
   * Furthermore, entrance method returns a move and modifies the board, meanwhile the 
   * recursive method only returns integet values.
   * @returns best value found so far
   * @requires board!=null && player=={Piece.WHITE || Piece.BLACK}
   */

  private static int negaMaxRec(Board board,int depth,int nextTurn){

    //get validMoves
    List validMoves = Piece.getValidMoves(board, nextTurn);


    //Messages 
    if (debug)
	System.out.println("("+depth+") # of moves: "+validMoves.size());


    //check if it is the end of the game
     if (Piece.isTie(board,nextTurn,validMoves))
	    return 0;
     if (Piece.isWin(board,nextTurn,validMoves)) //only next moving player can win the game
	  return (Integer.MAX_VALUE-1);


    //if we have reached the end children, we evaluate the board
    //and return a value for it
    if (depth<=0) 
	return board.getDifference(nextTurn);


     //start looking at the move list
     int best=(-Integer.MAX_VALUE); //best move found so far, has the lowest number possible
     int value=0;                   //value of the child node, could be equal to anything in the beginning
     int size=validMoves.size();    //size of the validMoves

     for(int i=0;i<size;i++)

	 { Piece.makeGeneratedMove(board,(Move)validMoves.get(i)); //make the next move and modify the board

	
	   value= (-1)*NewEngine.negaMaxRec(board,depth-1,
					    Piece.getOpponentColor(nextTurn));




	   if (value>best) best=value;                             //if this is a better move, take this one
	   Piece.undoMove(board,(Move)validMoves.get(i));          //undo the move, and revert back to inital board
	 }

     return best; //return the best value
    
  }
  /**
   *  Standard NegaMax algorithm, entrance method (for the root node)
   *  <br><b> board</b> Board to make a search on
   *  <br><b> depth</b> Depth of seach
   *  <br><b> nextTurn</b> next player to move
   *  @requires !Piece.isGameOver() && (depth>0)
   *  @modifies board using the best move
   *  @returns the best move
   *  @throws RuntimeException if <code>requires</code> is not satisfied
  */
    
    public static Move negaMax(Board board,int depth,int nextTurn) {

	
    	

    //get validMoves
    List validMoves = Piece.getValidMoves(board, nextTurn);

    //check if it is the end of the game, if it is, it is an error
    if (Piece.isGameOver(board,nextTurn,validMoves))
      throw new RuntimeException("NewEngine.negaMax():Game is Over!");


    //if we have reached the end children in the entrance, it is an error
    if (depth<=0)
      throw new RuntimeException("NewEngine.negaMax():Depth is non-positive ");


     //start looking at the move list
     int best=(-Integer.MAX_VALUE);  //best move found so far, has the lowest number possible
     int value=0;                    //value of the child node, could be equal to anything in the beginning
     int size=validMoves.size();     //size of the validMoves
     Move bestMove=null;



     //if there is only one move to make, do not think 
     //you can think later, just make the move
     if (size==1)
	 bestMove=(Move)validMoves.get(0);
     else
	 {

	     for(int i=0;i<size;i++)
       
		 { Piece.makeGeneratedMove(board,(Move)validMoves.get(i)); //make the next move and modify the board

	 
		     value= (-1)*NewEngine.negaMaxRec(board,depth-1,
						      Piece.getOpponentColor(nextTurn));
		 
		 if (value>best) 
		     {
			 best=value;
			 bestMove=(Move)validMoves.get(i);}         //if this is a better move, take this one


		 Piece.undoMove(board,(Move)validMoves.get(i)); //undo the move, and revert back to inital board
		 }

	 }

     //if bestMove is null, there is an error in our algorithm
     if (bestMove==null)
	 {
	      System.out.println("bestMove found so far is NULL: BIG ERROR IN PROGRAM");
	      System.exit(0);
	 }

     //make the best move found so far
     Piece.makeGeneratedMove(board,bestMove);
     if (debug)
      System.out.println("(root node)negaMax choose:"+bestMove);

     
     return bestMove;
    }

  /**Returns a random move from the valid moves of the player
   *@requires board instanceof Board && player={Piece.WHITE || Piece.BLACK}
   *@modifies board using the best move
   *@returns the best move
   */
  
  public static Move randomMove(Board board, int player)
  {
   
    //get validMoves
    List validMoves = Piece.getValidMoves (board, player);
 
    //choose a random move 
    int moveIndex=(int) (validMoves.size()*Math.random());
    if (debug)
      System.out.println("randomMove choose index: "+(moveIndex+1)+" in "+validMoves.size()+" moves");
    
    //take that move and apply it to the board
    Move m=(Move)validMoves.get(moveIndex);
    //    Piece.makeGeneratedMove(board,m);
    if (debug)
      System.out.println("(root node) randomMove choose:"+m);

    
    return m;
  } 

   
  /**
   *  Standard AlphaBeta algorithm, lower terms
   * It is necessary to make a distinction between the entrance method which runs
   * on the root node and the recursive method which runs on children nodes.
   * This distinction allows us to return from the recursion if depth is given 
   * less than 0 or if it is an endgame situation (which shoudl be detected by GameController)
   * Furthermore, entrance method returns a move and modifies the board, meanwhile the 
   * recursive method only returns integet values.
   * @returns best value found so far
   * @requires board!=null && player=={Piece.WHITE || Piece.BLACK}
   */

  private static int alphaBetaRec(Board board,int depth,int nextTurn,int alphaIN, int betaIN){


    //get validMoves
    List validMoves = Piece.getValidMoves(board, nextTurn);

    //Messages
    if (debug)
	System.out.println("("+depth+") Number of moves: "+validMoves.size()+" alp: "+alphaIN+" be: "+betaIN);

    //check if it is the end of the game
     if (Piece.isTie(board,nextTurn,validMoves))
	    return 0;
     if (Piece.isWin(board,nextTurn,validMoves)) //only next moving player can win the game
	  return(Integer.MAX_VALUE-1);

     //evaluate if depth has been reached
    if (depth<=0) 
	{
	if (debug)
	    System.out.println("("+depth+"):"+(-board.getDifference(nextTurn)));
	return board.getDifference(nextTurn);}

     //start looking at the move list
     int best=(-Integer.MAX_VALUE); //best move found so far, has the lowest number possible
     int value=0;                   //value of the child node, could be equal to anything in the beginning
     int size=validMoves.size();    //size of the validMoves
     int alpha=alphaIN;             //alpha value for this level 
     int beta=betaIN;               //beta value for this level


     for(int i=0;i<size;i++)
      if (best<beta)	 

	 {
	     Piece.makeGeneratedMove(board,(Move)validMoves.get(i));
     	     if (debug)
		 System.out.print("("+depth+") "+(Move)validMoves.get(i)+" ");
	     
	     if (best>alpha) alpha=best;

	     value= (-1)*NewEngine.alphaBetaRec(board,depth-1,
					    Piece.getOpponentColor(nextTurn),-beta,-alpha);

	     if (value>best) best=value;
	     Piece.undoMove(board,(Move)validMoves.get(i));
	 }

     return best;
    
  }


  /**
   *  Standard Alpha-Beta algorithm, entrance method
   *  <br><b> board</b> Board to make a search on 
   *  <br><b> depth</b> Depth of seach
   *  <br><b> nextTurn</b>next player to move
   *  <br><b> alpha</b>lower term of alpha-beta
   *  <br><b> beta</b> higher term of alpha-beta
   *  @requires !Piece.isGameOver() && (depth>0)
   *  @modifies board using the best move
   *  @returns the best move
   *  @throws RuntimeException if <code>requires</code> is not satisfied
  */



  public static Move alphaBeta(Board board,int depth,int nextTurn,int alphaIN, int betaIN){

      System.out.println("Started A-B  with depth:"+depth);  
    //get validMoves
    List validMoves = Piece.getValidMoves(board, nextTurn);

    //Messages
    if (debug)
	System.out.println("Upper Level:("+depth+") Number of moves: "+validMoves.size()+" alp: "+alphaIN+" be: "+betaIN);


    //check if it is the end of the game
    if (Piece.isGameOver(board,nextTurn,validMoves))
      throw new RuntimeException("NewEngine.alphaBeta():Game is Over!");


    //if we have reached the end children, we evaluate the board
    //and return a value for it
    if (depth<=0) 
      throw new RuntimeException("NewEngine.alphaBeta():Depth is non-positive ");


     //start looking at the move list
     int best=(-Integer.MAX_VALUE); //best move found so far, has the lowest number possible
     int value=0;                   //value of the child node, could be equal to anything in the beginning
     int size=validMoves.size();    //size of the validMoves
     int alpha=alphaIN;             //alpha value for this level 
     int beta=betaIN;               //beta value for this level
     Move bestMove=null;            //has to be null, if it still is null after the 'for' loop
                                    //we can detect that our algorithm is not working correctly


     //if there is only one move to make, do not think 
     //you can think later, just make the move, only valid 
     //at the first level
     if (size==1)
	 bestMove=(Move)validMoves.get(0);
     else
	 {

	     
	     for(int i=0;i<size;i++)
		 if (best<beta) 	 
		     {
			 Piece.makeGeneratedMove(board,(Move)validMoves.get(i)); //make the next move and modify the board
			 if (debug)
			     System.out.print("("+depth+"):"+(Move)validMoves.get(i)+" ");
			 
			 if (best>alpha) alpha=best; //adjust alpha accordingly

	     
			 if (debug)
			     System.out.println("("+depth+")"+"Called with alp:"+(alpha)+" be: "+(beta));

			      	
			 value= (-1)*NewEngine.alphaBetaRec(board,depth-1,
							    Piece.getOpponentColor(nextTurn),-beta,-alpha);
			 
			 if (value>best)   
			     {
				 best=value;
				 bestMove=(Move)validMoves.get(i);}          //if this is a better move, take this one
			 Piece.undoMove(board,(Move)validMoves.get(i));  //undo the move, and revert back to inital board
		     }
     
	 }
     //if bestMove is null, there is an error in our algorithm
     if (bestMove==null)
	 {
	      System.out.println("bestMove found so far is NULL: BIG ERROR IN PROGRAM");
	      System.exit(0);
	 }

     //apply the bestMove found so far to the board
      Piece.makeGeneratedMove(board,bestMove);
      if (debug)
	  System.out.println("(root node) alphaBeta choose: "+bestMove);

     
     return bestMove;

    
  }

    //**********************************************************************************
    //**********************************************************************************
    //                     ALHPA-BETA WITH HASHTABLE 
    //**********************************************************************************
    //**********************************************************************************

   
  /**
   *  Standard AlphaBeta algorithm, lower terms
   * It is necessary to make a distinction between the entrance method which runs
   * on the root node and the recursive method which runs on children nodes.
   * This distinction allows us to return from the recursion if depth is given 
   * less than 0 or if it is an endgame situation (which shoudl be detected by GameController)
   * Furthermore, entrance method returns a move and modifies the board, meanwhile the 
   * recursive method only returns integet values.
   * @returns best value found so far
   * @requires board!=null && player=={Piece.WHITE || Piece.BLACK}
   */

  private static int alphaBetaHashRec(Board board,Map boardTable, int depth,int nextTurn,int alphaIN, int betaIN){

      boolean printHashKey=false;
      
    //get validMoves
    List validMoves = Piece.getValidMoves(board, nextTurn);


    //Messages
    if (debug)
	System.out.println("("+depth+") Number of moves: "+validMoves.size()+" alp: "+alphaIN+" be: "+betaIN);

    //check if it is the end of the game
     if (Piece.isTie(board,nextTurn,validMoves))
	    return 0;
     if (Piece.isWin(board,nextTurn,validMoves)) //only next moving player can win the game
	  return(Integer.MAX_VALUE-1);

     //evaluate if depth has been reached
    if (depth<=0) 
	{
	if (debug)
	    System.out.println("("+depth+"):"+(-board.getDifference(nextTurn)));

	//put it into the hashTable and return
	int val=board.getDifference(nextTurn);
	HashKey hashKey=new HashKey(board.zobristCode(nextTurn));
	HashEntry putInEntry= new HashEntry(hashKey.key,null,HashEntry.HASHEXACT,val,0);
	boardTable.put(hashKey,putInEntry); 
	//print the debug code on the screen
	if (debughash)
		  System.out.println("("+depth+"):"+hashKey.hashCode()+":"+putInEntry);




	return val;}


     //get the hashEntry which have the same hashKey as the board
     HashKey hashKey=new HashKey(board.zobristCode(nextTurn));
     HashEntry retrivedEntry=(HashEntry)boardTable.get(hashKey);


    if (printHashKey)
	System.out.println("k:"+hashKey.key+"i:"+(int)hashKey.key);

     //check if we can return this value

     if (retrivedEntry!=null)
     	 {   //check if the depth makes us use this node effectively
	     if (retrivedEntry.getKey()==hashKey.key && depth<=retrivedEntry.getDepth())
		 {
		     if (retrivedEntry.getType()==HashEntry.HASHEXACT)
			 {
			     if (debughashcut)
			     //Print out that we making a cut from the table
			     System.out.println("Cutting from the table exact:"+retrivedEntry.getValue());
		  
			     return retrivedEntry.getValue();
			 }
		     if (retrivedEntry.getType()==HashEntry.HASHALPHA && retrivedEntry.getValue()<=alphaIN)
			 {
			     if (debughashcut)
		     //Print out that we making a cut from the table
		     System.out.println("Cutting from the table alpha:"+alphaIN);
		  
		     return alphaIN;}
		     if (retrivedEntry.getType()==HashEntry.HASHBETA && betaIN<=retrivedEntry.getValue())
			 {
			     if (debughashcut)
		     //Print out that we making a cut from the table
		     System.out.println("Cutting from the table beta:"+betaIN);
		  
		     return betaIN;}
		 }
	     else //else, if this move is in our list play it first
	      if (retrivedEntry.getBestMove()!=null)	 
		 {
		     int size=validMoves.size();
			 for(int i=0;i<size;i++)
			     if (retrivedEntry.getBestMove().equals((Move)validMoves.get(i)))
				 {  
				  //move the first to be the first to be handled   
				  Move importantMove=(Move)validMoves.remove(i);
		                  validMoves.add(0,importantMove);
				  break;
				 }
		 }
	 }


     //start looking at the move list
     int best=(-Integer.MAX_VALUE); //best move found so far, has the lowest number possible
     int value=0;                   //value of the child node, could be equal to anything in the beginning
     int size=validMoves.size();    //size of the validMoves  
     int alpha=alphaIN;             //alpha value for this level 
     int beta=betaIN;               //beta value for this level
     Move bestMove=null;            //has to be null, if it still is null after the 'for' loop
                                    //we can detect that our algorithm is not working correctly

     int hashType=HashEntry.HASHALPHA; //this node is treated as an alpha node to begin
                                       //with


     // having updated move list we can begin alpha-beta
     for(int i=0;i<size;i++)
      if (best<beta)	 

	 {
	     Piece.makeGeneratedMove(board,(Move)validMoves.get(i));
     	     if (debug)
		 System.out.print("("+depth+") "+(Move)validMoves.get(i)+" ");

	     
	     if (best>alpha) { hashType=HashEntry.HASHEXACT; //this is an exact node
		               alpha=best;  //our alpha is the newly found value
	                     }


	     value= (-1)*NewEngine.alphaBetaHashRec(board,boardTable, depth-1,
					    Piece.getOpponentColor(nextTurn),-beta,-alpha);

	     if (value>best)   
		 {
		 best=value;
		 bestMove=(Move)validMoves.get(i);           //if this is a better move, take this one
	         }

	     

	     Piece.undoMove(board,(Move)validMoves.get(i));

	 }
      else //beta has been found
	  {
	      hashType=HashEntry.HASHBETA;
	      best=beta;
	      break;
	  }



     //record to the hashTable before we return
     HashEntry putInEntry=new HashEntry(hashKey.key,bestMove,hashType,best,depth);
     boardTable.put(hashKey,putInEntry);
     //print the debug code on the screen
      if (debughash)
		  System.out.println("("+depth+"):"+hashKey.hashCode()+":"+putInEntry);


     return best;

    
  }


  /**
   *  Standard Alpha-Beta algorithm, entrance method
   *  <br><b> board</b> Board to make a search on 
   *  <br><b> depth</b> Depth of seach
   *  <br><b> nextTurn</b>next player to move
   *  <br><b> alpha</b>lower term of alpha-beta
   *  <br><b> beta</b> higher term of alpha-beta
   *  @requires !Piece.isGameOver() && (depth>0)
   *  @modifies board using the best move
   *  @returns the best move
   *  @throws RuntimeException if <code>requires</code> is not satisfied
  */



  public static Move alphaBetaHash(Board board,Map boardTable, int depth,int nextTurn,int alphaIN, int betaIN){

      boolean printHashKey=false;

      System.out.println("Started Hash with depth:"+depth);  

    //get validMoves
    List validMoves = Piece.getValidMoves(board, nextTurn);

    //Messages
    if (debug)
	System.out.println("Upper Level:("+depth+") Number of moves: "+validMoves.size()+" alp: "+alphaIN+" be: "+betaIN);


    //check if it is the end of the game
    if (Piece.isGameOver(board,nextTurn,validMoves))
      throw new RuntimeException("NewEngine.alphaBeta():Game is Over!");


    //if we have reached the end children, we evaluate the board
    //and return a value for it
    if (depth<=0) 
      throw new RuntimeException("NewEngine.alphaBeta():Depth is non-positive ");

     //get the hashEntry which have the same hashKey as the board
    HashKey hashKey=new HashKey(board.zobristCode(nextTurn));
    HashEntry retrivedEntry=(HashEntry)boardTable.get(hashKey);
    
    if (printHashKey)
	System.out.println("k:"+hashKey.key+"i:"+(int)hashKey.key);
    

     //check if we can return this value

     if (retrivedEntry!=null)
     	 {   //check if the depth makes us use this node effectively
	     if (retrivedEntry.getKey()==hashKey.key &&depth<=retrivedEntry.getDepth()&&retrivedEntry.getBestMove()!=null)
		 {
			     if (debughashcut)
		     //Print out that we making a cut from the table
		     System.out.println("Cutting from the table");
		     


		     //since this board exists in the hashTable, even if itis an alpha, beta or exact node
		     //just check if we can make the suggested move and make it else 
		     //proceed normally
		     int size=validMoves.size();
		     boolean canMake=false;	   
			 for(int i=0;i<size;i++)
			     if (retrivedEntry.getBestMove().equals((Move)validMoves.get(i)))
				 {  
				  canMake=true;   
				  break;
				 }

		    //if this move is in our validMove list then it means that we canMake this move	 
		    if (canMake) 	  
			{
			     Move bestMove=retrivedEntry.getBestMove();
			     //apply the bestMove found so far to the board
			     Piece.makeGeneratedMove(board,bestMove);
			     if (debug)
				 System.out.println("(root node) alphaBeta choose: "+bestMove);
			     return bestMove;
			}

		 }
	     else //else, if this move is in our list play it first
	      if (retrivedEntry.getBestMove()!=null)	 
		 {
		     int size=validMoves.size();
			 for(int i=0;i<size;i++)
			     if (retrivedEntry.getBestMove().equals((Move)validMoves.get(i)))
				 {  
				  //move the first to be the first to be handled
				  Move importantMove=(Move)validMoves.remove(i);
		                  validMoves.add(0,importantMove);
				  break;
				 }
		 }
 }


     //start looking at the move list
     int best=(-Integer.MAX_VALUE); //best move found so far, has the lowest number possible
     int value=0;                   //value of the child node, could be equal to anything in the beginning
     int size=validMoves.size();    //size of the validMoves
     int alpha=alphaIN;             //alpha value for this level 
     int beta=betaIN;               //beta value for this level
     Move bestMove=null;            //has to be null, if it still is null after the 'for' loop
                                    //we can detect that our algorithm is not working correctly

     int hashType=HashEntry.HASHALPHA; //this node is treated as an alpha node to begin
                                       //with




     //if there is only one move to make, do not think 
     //you can think later, just make the move
     if (size==1)
	 bestMove=(Move)validMoves.get(0);
     else
	 {


	     for(int i=0;i<size;i++)
		 if (best<beta) 	 
		     {
			 Piece.makeGeneratedMove(board,(Move)validMoves.get(i)); //make the next move and modify the board
			 if (debug)
			     System.out.print("("+depth+"):"+(Move)validMoves.get(i)+" ");


			 if (best>alpha) { hashType=HashEntry.HASHEXACT; //this is an exact node
		               alpha=best;  //our alpha is the newly found value
			                 }
	     
			 if (debug)
			     System.out.println("("+depth+")"+"Called with alp:"+(alpha)+" be: "+(beta));


			 value= (-1)*NewEngine.alphaBetaHashRec(board,boardTable, depth-1,
								Piece.getOpponentColor(nextTurn),-beta,-alpha);

			 if (value>best)   
			     {
				 best=value;
				 bestMove=(Move)validMoves.get(i);}          //if this is a better move, take this one
			 Piece.undoMove(board,(Move)validMoves.get(i));  //undo the move, and revert back to inital board
		     }
	     
		 else //beta has been found
		     {
			 hashType=HashEntry.HASHBETA;
			 break;
		     }

	 }

     //if bestMove is null, there is an error in our algorithm
     if (bestMove==null)
	 {
	      System.out.println("bestMove found so far is NULL: BIG ERROR IN PROGRAM");
	      System.exit(0);
	 }
     
     //put this to the hashTable first
     HashEntry putInEntry=new HashEntry(hashKey.key,bestMove,hashType,best,depth);
     boardTable.put(hashKey,putInEntry);
     //print the debug code on the screen
	      if (debughash)
		  System.out.println("("+depth+"):"+hashKey.hashCode()+":"+putInEntry);


     //apply the bestMove found so far to the board
      Piece.makeGeneratedMove(board,bestMove);
      if (debug)
	  System.out.println("(root node) alphaBeta choose: "+bestMove);

     
     return bestMove;

    
  }



}
