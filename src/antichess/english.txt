#GENERAL
Name "AntiChess "
Version "1.0 "
OK "OK"
Cancel "Cancel"
Initial Time "5:00"
Human "Human "
Computer "Computer "
Vs "vs "
Color "Color "
White "White"
Black "Black"
Red "Red"
Purple "Purple"


#MAIN MENU
Game "Game "
Help "Help "

#GAME MENU
NewGame "New Game "
  #NEW GAME MENU
  2Players "2 Player Game "
  4Players "4 Player Game "
      TwoPlayerTitle "2 Player Game Properties"
      FourPlayerTitle "4 Player Game Properties"
      
      Player1 "Player1"
      Player2 "Player2"
      Player3 "Player3"
      Player4 "Player4"
      Name "Name "
      Side "Side "
      Style "Style "
      Type "Type "
      Time "Time "
      GameName "Game Name "
      GameStyle "Game Style  "
      BoardStyle "Board Style "
      Cylinderical "Cylinderical Board"




OpenGame "Open Game "
SaveGame "Save Game "
Settings "Settings "
  #SETTINGS MENU
  ShowLegalMoves "Show legal moves"
  ShowTime "Show time when printing moves"
  ShowPieces "Show pieces that can play"	 
Quit "Quit "

#HELP MENU
Contents "Contents "
About "About "

