/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;
import java.util.Random;
/**
 *
 *<p> Zobrist class handles the Zobrist algorithm for creating a zobrist key for a given board.
 **/

public class Zobrist{

    private Random randomGenerator;
    private long[][][] zobristMatrix;
    private Board board;
    private long key;
    private long turn[];

    private boolean debug=false;

    //CONSTRUCTORS

    /**Creates a Zobrist matrix for a specific board. Initializes key=0.
     *@requires board is full with pieces  
     *
     */

    public Zobrist(Board board){

	//create the randomGenerator
	randomGenerator=new Random();

	//get how many squares we have in the board
	int squareCount=(board.MAXROW+1-board.MINROW)*(board.MAXCOL+1-board.MINCOL);

	//create the zobrist matrix and fill it up
	zobristMatrix=new long[Piece.LASTPIECE+1-Piece.FIRSTPIECE][Piece.LASTCOLOR+1-Piece.FIRSTCOLOR][squareCount];

	for(int i=Piece.FIRSTPIECE;i<=Piece.LASTPIECE;i++)
	    for(int j=Piece.FIRSTCOLOR;j<=Piece.LASTCOLOR;j++)
		for(int k=0;k<squareCount;k++)
		    zobristMatrix[i][j][k]=randomGenerator.nextLong();

	//initalize black constant
	turn=new long[4];
	turn[0]=0;
	turn[1]=randomGenerator.nextLong();
	turn[2]=randomGenerator.nextLong();
	turn[3]=randomGenerator.nextLong();

	//initialize the board link
	this.board=board;

	//initialize the key
	Piece piece;
	int place;
	key=0;
        for (int i=board.MINCOL;i<=board.MAXCOL;i++)
	    for (int j=board.MINROW;j<=board.MAXROW;j++)
		if ((piece=board.getPieceAt(i,j))!=null)
		    if (piece.getType()!=Piece.EMPTY)
			{	
			    place=j*(board.MAXROW+1-board.MINROW)+(i-board.MINCOL);
			    key^=zobristMatrix[piece.getType()-Piece.FIRSTPIECE][piece.getColor()-Piece.FIRSTCOLOR][place];
			}
    }


    /**Returns a zobrist key for the board
     *@requires board is the board that initially zobristMatrix is created for
     * nextTurn is the next player to move
     *@returns a zobrist key for that board
     *@throws RuntimeException if this is a different board
     */
    public long getKey(Board board, int nextTurn)
    {
	if (debug)
	    if (board!=this.board)
		throw new RuntimeException("Zobrist.getKey():this is a different board");
	
	return (key^turn[nextTurn]);
         
    }

    /**Returns a zobrist key for the board
     *@requires board is the board that initially zobristMatrix is created for
     * nextTurn is the next player to move
     *@returns a zobrist key for that board
     *@throws RuntimeException if this is a different board
     */
    public long getKey(String boardStr, int nextTurn)
    {
	long newKey=0;
	int size=boardStr.length();
	char p;
	char pUp;
	int color=0;
	int pieceType=0;


	for(int i=0;i<size;i++)
	    {
		      // Get Piece Type
		     pUp = boardStr.toUpperCase().charAt(i);
		     p = boardStr.charAt(i);

		     // Get Color
		     if (p == pUp) // White are upper case
			 color = Piece.WHITE;
		     else 
			 color = Piece.BLACK;
		     
		     if (pUp == 'P'){
			 pieceType = Piece.PAWN;
		     } else if (pUp == 'K'){
			 pieceType = Piece.KING;
		     } else if (pUp == 'N'){
			 pieceType = Piece.KNIGHT;
		     } else if (pUp == 'R'){
			 pieceType = Piece.ROOK;
		     } else if (pUp == 'Q'){
			 pieceType = Piece.QUEEN;
		     } else if (pUp == 'B'){
			 pieceType = Piece.BISHOP;
		     } else
			 if (pUp!='-')
			 throw new RuntimeException ("Unrecognized piece type:" + pUp);
		     
		     if (pUp!='-')
			 newKey^=zobristMatrix[pieceType][color][i];
			 }
	

	
	return (newKey^turn[nextTurn]);
         
    }

	
    /**MUST BE CALLED WHEN BOARD IS CHANGED THROUGH A MOVE OR UNDOMOVE
     * Updates the key for the board update
     *@throws RuntimeException if this is a different board
     * 
     */
	
    public void deletePiece(Board board,int type, int color, int col, int row)
    {
	if (debug)
	    if (board!=this.board)
		throw new RuntimeException("Zobrist.getKey():this is a different board");
	
	
	key^=zobristMatrix[type-Piece.FIRSTPIECE][color-Piece.FIRSTCOLOR][row*(board.MAXROW+1-board.MINROW)+(col-board.MINCOL)];
    }


    /**MUST BE CALLED WHEN BOARD IS CHANGED THROUGH A MOVE OR UNDOMOVE
     * Updates the key for the board update
     *@throws RuntimeException if this is a different board
     * 
     */
	

    public void addPiece(Board board,int type, int color, int col, int row)
    {
	if (debug)
	    if (board!=this.board)
		throw new RuntimeException("Zobrist.getKey():this is a different board");

	key^=zobristMatrix[type-Piece.FIRSTPIECE][color-Piece.FIRSTCOLOR][row*(board.MAXROW+1-board.MINROW)+(col-board.MINCOL)];
    }

}
	    
