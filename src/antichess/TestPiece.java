/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;
import junit.framework.*;
import java.io.*;
import java.util.*;

/** Tests Piece
 */
public class TestPiece
  extends TestCase{
//   String testfilesFile = "src/test/tests";
//   String testfilesDir = "src/test/";
  String testfilesFile = "src/test/tests";
  String testfilesDir = "src/test/";
  String testEndGameFile = "src/test/testend";
  String testEndGameDir = "src/test/";
  public TestPiece(String name) {
    super(name);
  }


  // HELPER FUNCTIONS

  /** Converts a string of line into a List of sorted strings
   **/
  private List linesToSortedList (String s){
    StringTokenizer st = new StringTokenizer (s, "\n");
    List l = new ArrayList ();
    while (st.hasMoreElements()){
      String str  = (String) st.nextElement();
      if (str.length()>0)
	if(str.charAt(str.length()-1) == ';')
	  str = str.substring (0, str.length()-1);
      l.add (str);
    }
    Collections.sort (l);
    //System.out.println (l);
    return l;
  }
  
  // FILE FUNCTION

  /** This function only returns all text before the first ";"
   *  and sets comments to contain all text after the first ";"
   **/
  
  private String fileRead(String filename, boolean readAll) {
    if (filename == null)
      throw new RuntimeException("No file specified");


    String answer = new String();

    try {
      BufferedReader in = new BufferedReader(new FileReader(filename));
      // read each line until the end of the file and parse it into the script
      for (String line = in.readLine(); line != null; line = in.readLine()) {
	
	answer += line;

	if (line.length() > 0)
	  if (line.charAt (line.length()-1) == ';' && !readAll){
	    break;
	  }

	answer += "\n";


      }
      
    }
    catch (Exception e) {
      throw new RuntimeException("File not accessible\n" + filename + "\n" + e);
    }
    
    //System.out.println ("File read:\n" + answer);
    return answer;
  }
  
  protected void setUp(){
  }


  /**
   * Tests making a move and updating of the board
   **/
  public void testMakeMove(){
    

    StringTokenizer st1 = new StringTokenizer (fileRead (testfilesFile, false), "\n");
    while (st1.hasMoreElements()){
      String filename = (String) st1.nextElement();
      System.out.println ("------- TESTING MAKEMOVE ------");
      System.out.println ("------- Reading " + filename + " ------");
      
      String filecontents = fileRead (testfilesDir + filename, true);
      StringTokenizer st = new StringTokenizer (filecontents, ";");
      String boardinfo = (String) st.nextElement(); 
      if (!st.hasMoreElements())
	throw new RuntimeException ("Nothing found after first ;");
      
      String movestring = (String) st.nextElement();
      movestring = movestring.substring(1); // get rid ofpreceeding \n
      if (!st.hasMoreElements())
	throw new RuntimeException ("Nothing found after second ;");
      
      String expectedStr = (String) st.nextElement();
      boolean expected = (expectedStr.toLowerCase().equals("\ntrue")? true: false);
      System.out.println ("Testing " + filename);
      System.out.println ("Move: " + movestring);
      System.out.println ("Expected: " + expected);
      

      if (expected){
	GameController gc = new GameController (testfilesDir + filename); 
	Board b = gc.getBoard();
	Move move = new Move (movestring);
	//Piece p = b.getPieceAt (move.getStartCol(), move.getStartRow());
	int nextTurn = gc.getNextTurn();
	//if (p.getColor()!=nextTurn){
	  //throw new RuntimeException ("nextturn should be " + p.getColor());
	//}
	System.out.println ("Switched: " + b.getSpecial(Piece.WHITE)
			    + " " + b.getSpecial(Piece.BLACK));

	// Make move
	//System.out.println ("Before making move:\n" + gc.getBoard());
	
	//p.makeMove (move);
	//System.out.println ("Move modified piece count: " + move.getModifiedPieceCount());
	Piece.makeMove (b, move);
	
	//System.out.println ("After making move:\n" + gc.getBoard());
	
	//System.out.println (b == gc.getBoard());
	
	gc.setNextTurn(Piece.getOpponentColor (nextTurn));
	if (!linesToSortedList(fileRead(testfilesDir + filename + "2", false)).equals
		      (linesToSortedList (gc.getBoardString()))){
	  System.out.println (fileRead(testfilesDir + filename + "2", false));
	  System.out.println (gc.getBoardString());
	  System.out.println ("Expected:");
	  System.out.println (linesToSortedList(fileRead(testfilesDir + filename + "2", false)));
	  System.out.println ("Actual:");
	  System.out.println (linesToSortedList(gc.getBoardString()));
	  
	}
	  
	
	assertEquals ("Testing make move " + move + ":\n",
		      linesToSortedList(fileRead(testfilesDir + filename + "2", false)),
		      linesToSortedList (gc.getBoardString()));
      }
    
    }
  }
  
  /**
   * Tests undoing a move after making a move and updating of the board
   **/
  public void testUndoMove(){
    //if (1==1) return;
    StringTokenizer st1 = new StringTokenizer (fileRead (testfilesFile, false), "\n");
    while (st1.hasMoreElements()){
      String filename = (String) st1.nextElement();
      System.out.println ("------- TESTING UNDOMOVE ------");
      System.out.println ("------- Reading " + filename + " ------");
      
      String filecontents = fileRead (testfilesDir + filename, true);
      StringTokenizer st = new StringTokenizer (filecontents, ";");
      String boardinfo = (String) st.nextElement(); 
      if (!st.hasMoreElements())
	throw new RuntimeException ("Nothing found after first ;");
      
      String movestring = (String) st.nextElement();
      movestring = movestring.substring(1); // get rid ofpreceeding \n
      if (!st.hasMoreElements())
	throw new RuntimeException ("Nothing found after second ;");
      
      String expectedStr = (String) st.nextElement();
      boolean expected = (expectedStr.toLowerCase().equals("\ntrue")? true: false);
      System.out.println ("Testing " + filename);
      System.out.println ("Move: " + movestring);
      System.out.println ("Expected: " + expected);
      

      if (expected){
	GameController gc = new GameController (testfilesDir + filename); 
	Board b = gc.getBoard();
	Move move = new Move (movestring);
	//Piece p = b.getPieceAt (move.getStartCol(), move.getStartRow());
	int nextTurn = gc.getNextTurn();
	//if (p.getColor()!=nextTurn){
	  //throw new RuntimeException ("nextturn should be " + p.getColor());
	//}
	System.out.println ("Switched: " + b.getSpecial(Piece.WHITE)
			    + " " + b.getSpecial(Piece.BLACK));

	// Make move
	//System.out.println ("Before making move:\n" + gc.getBoard());
	
	//p.makeMove (move);
	Piece.makeMove (b, move);
	
	//System.out.println ("After making move:\n" + gc.getBoard());
	
	//System.out.println (b == gc.getBoard());
	
	gc.setNextTurn(Piece.getOpponentColor (nextTurn));
	if (!linesToSortedList(fileRead(testfilesDir + filename + "2", false)).equals
		      (linesToSortedList (gc.getBoardString()))){
	  System.out.println (fileRead(testfilesDir + filename + "2", false));
	  System.out.println (gc.getBoardString());
	  System.out.println ("Expected:");
	  System.out.println (linesToSortedList(fileRead(testfilesDir + filename + "2", false)));
	  System.out.println ("Actual:");
	  System.out.println (linesToSortedList(gc.getBoardString()));
	  
	}
	  
	
	assertEquals ("Testing make move " + move + ":\n",
		      linesToSortedList(fileRead(testfilesDir + filename + "2", false)),
		      linesToSortedList (gc.getBoardString()));

	// Make undo Move
	Piece.undoMove (b, move);
	
	gc.setNextTurn(nextTurn);
	
	if (!linesToSortedList(fileRead(testfilesDir + filename , false)).equals
		      (linesToSortedList (gc.getBoardString()))){
	  System.out.println (fileRead(testfilesDir + filename, false));
	  System.out.println (gc.getBoardString());
	  System.out.println ("Expected:");
	  System.out.println (linesToSortedList(fileRead(testfilesDir + filename, false)));
	  System.out.println ((fileRead(testfilesDir + filename, false)));

	  System.out.println ("Actual:");
	  System.out.println (linesToSortedList(gc.getBoardString()));
	  System.out.println ((gc.getBoardString()));
	  
	}
	assertEquals ("Testing undo move " + move + ":\n",
		      linesToSortedList(fileRead(testfilesDir + filename, false)),
		      linesToSortedList (gc.getBoardString()));
	
      }
    
    }
  }

  /**
   * Tests if a move is valid. The move is specified after the first semicolon
   * in the file, and ends with a second semicolon. The expected answer is
   * true/false, and ends with a semicolon too;
   **/
  public void testIsValidMove(){
    //if (1==1) return;

    StringTokenizer st1 = new StringTokenizer (fileRead (testfilesFile, false), "\n");
    while (st1.hasMoreElements()){
      String filename = (String) st1.nextElement();
      System.out.println ("------- TESTING VALID MOVE ------");
      System.out.println ("------- Reading " + filename + " ------");
      
      String filecontents = fileRead (testfilesDir + filename, true);
      StringTokenizer st = new StringTokenizer (filecontents, ";");
      String boardinfo = (String) st.nextElement(); 
      if (!st.hasMoreElements())
	throw new RuntimeException ("Nothing found after first ;");
      
      String movestring = (String) st.nextElement();
      movestring = movestring.substring(1); // get rid ofpreceeding \n
      if (!st.hasMoreElements())
	throw new RuntimeException ("Nothing found after second ;");
      
      String expectedStr = (String) st.nextElement();
      boolean expected = (expectedStr.toLowerCase().equals("\ntrue")? true: false);
      System.out.println ("Testing " + filename);
      System.out.println ("Move: " + movestring);
      System.out.println ("Expected: " + expected);
      

      GameController gc  = new GameController (testfilesDir + filename);
      Board b = gc.getBoard();
      Move move = new Move (movestring);
      //Piece p = b.getPieceAt (move.getStartCol(), move.getStartRow());
      int nextTurn = gc.getNextTurn();
      //if (p.getColor()!=nextTurn){
	//throw new RuntimeException ("nextturn should be " + p.getColor());
      //}
      boolean result = Piece.isValidMove (b, move, nextTurn);

//       if (p.getBoard()==null)
// 	throw new RuntimeException ("Board should not be null");

      assertEquals ("Testing move " + move, expected, result);
    
    }
  }

  
  /**
   * Tests for end game conditions. The first semicolon is the end of boardinfo
   * in the file. The line after board info gives true/false ending with the second
   * semicolon indicating if the game is over or not.
   * The line after that is true/false indicating if the game is a draw or not, and
   * ends with a third semicolon;
   **/
  public void testEndGame(){
    // if (1==1 ) return;

    StringTokenizer st1 = new StringTokenizer (fileRead (testEndGameFile, false), "\n");
    while (st1.hasMoreElements()){
      String filename = (String) st1.nextElement();
      System.out.println ("------- TESTING END GAME ------");
      System.out.println ("------- Reading " + filename + " ------");
      
      String filecontents = fileRead (testEndGameDir + filename, true);
      StringTokenizer st = new StringTokenizer (filecontents, ";");
      String boardinfo = (String) st.nextElement(); 
      if (!st.hasMoreElements())
	throw new RuntimeException ("Nothing found after first ;");
      
      String expectedStr = (String) st.nextElement();
      boolean expectedEnd = (expectedStr.toLowerCase().equals("\ntrue")? true: false);
      
      expectedStr = (String) st.nextElement();
      boolean expectedDraw = (expectedStr.toLowerCase().equals("\ntrue")? true: false);
      
      System.out.println ("Testing " + filename);
      System.out.println ("Expected end game: " + expectedEnd);
      System.out.println ("Expected draw game: " + expectedDraw);
      

      GameController gc = new GameController (testEndGameDir + filename);
      Board b = gc.getBoard();

      int nextTurn = gc.getNextTurn();

      boolean resultEndGame = Piece.isGameOver (b, nextTurn);

      assertEquals ("Testing end game: ", expectedEnd, resultEndGame);

      if (resultEndGame){
	boolean resultDraw = Piece.isGameOver (b, nextTurn);
	
      }
    
    }
  }

  // Tell JUnit what order to run the tests in
  public static Test suite()
  { 
    TestSuite suite = new TestSuite(); 
    suite.addTest(new TestPiece("testEndGame"));
    suite.addTest(new TestPiece("testIsValidMove"));
    suite.addTest(new TestPiece("testMakeMove"));
    suite.addTest(new TestPiece("testUndoMove"));
    return suite;
  }
}
