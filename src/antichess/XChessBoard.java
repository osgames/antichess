/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

/**
 * XChessBoard creates an antichess board image on the board, places pieces on it,
 * and animates pieces for drag&drop.
 *
 * <p><u>Here is the outline of how XChessBoard handles an antichess board:</u><br>
 * Everytime setGameController is called, a new backgroud image is created
 * by first calling initBoard() and then, setBoardImage().
 *
 * <p>placePieces() creates labels with piece images for every non-empty square.
 * It places this accordingly on the board.
 *
 * <p>updateBoard() goes through all the squares of the board. It updates the positions of 
 * already created pieces. If there are not enough labels, it creates new labels. If
 * there are any excess labels it gets rid of them.
 *
 * <p>Drag&drop is implemented through JLabel.setLocation() method. This provides a flawless
 * animation compared to other methods.
 *
 *
 *
 */

public class XChessBoard extends javax.swing.JLayeredPane implements MouseListener,MouseMotionListener {
 
   //FIELDS
 
    private static final boolean DEBUG=false; //for printing on the screen

    private static GameController gc; //GameController for the game, assumed to be fixed, only supplied in the constructor
    private Board board;
    private int SIZEX,SIZEY;          //total size for the board
    private int letterMargin;        //letter margin added to the board size 
                                     //for adding position letters on the side
    
    private int squareSizeX, squareSizeY; //square size for the board
    private int pieceSizeX, pieceSizeY; // piece size 
    
    private char startLetter, startNumber; //for position, the first letter and number
  
    private java.awt.Color WHITECOLOR; //square colors
    private java.awt.Color BLACKCOLOR;
    
    private int boardMargin; //margin of the chessboard from the sides
    private int fourtwoMargin; //margin for 4 vs 2 player  
    private Image[] pieceImageList;
    private int piecePaddingX;//margin of the labelList from the square sides
    private int piecePaddingY;//margin of the labelList from the square sides


    private int startCol, startRow, endCol, endRow; //moving labelList coordinates

    private JLabel background; //the board: background image
    private java.util.ArrayList[] labelList; //list of pieces, pieces are arrays
    private JLabel movingLabel; //the dragged labe/piece

    
    private int arbX, arbY; //how much arbitrage we need in the chess icon itself
    private boolean mousePress; //true if mouse is pressed
  private JLabel [][] circArray; //stores the cirlces for valid pieces 
  private JLabel [][] circArray2; //stores the circles for valid moves


    private HumanPlayer human; //human player

    
    /** Creates new form XChessBoard 
     */
 public XChessBoard()  {

	initValues();
        initComponents();
        addMouseListener(this);
        addMouseMotionListener(this);
	
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() {//GEN-BEGIN:initComponents

        setLayout(new java.awt.BorderLayout());
        setPreferredSize(new java.awt.Dimension(SIZEX, SIZEY));
    }//GEN-END:initComponents
    
    
    /**Initializes global variables
     *@requires topology is a reference to a two dimensional integer array
     *@effects assigns topology the topology of the game

     */
    private void initValues(){

	//sets the size of the board on the screen
	SIZEX=SIZEY=600;
	letterMargin=20;
	//sets square colors for the chessboard
	WHITECOLOR=new java.awt.Color(204,204,244);
	BLACKCOLOR=java.awt.Color.BLACK;
	
	boardMargin = 3;
	piecePaddingX=8;
	piecePaddingY=3;
	

	squareSizeX=squareSizeY=1;
	
	arbX=arbY=0;
	startCol=startRow=endRow=endCol=0;
	movingLabel=null;

	//Load the images
	pieceImageList=new Image[4*Piece.PIECECOUNT];




    }

    /**Loads the images from the directory
     *@requires images exist in the codebase directory
     *@effects loads the images to the pieceImageList
     *@throws Exception if the piece images are not in 
     *the code base directory
     */


  private void getImages()
    {  
     pieceImageList[Piece.WHITE*Piece.PIECECOUNT+ Piece.PAWN]=(new ImageIcon(XChessBoard.class.getResource("Wpawn.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.BLACK*Piece.PIECECOUNT+ Piece.PAWN]=(new ImageIcon(XChessBoard.class.getResource("BLpawn.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.WHITE*Piece.PIECECOUNT+ Piece.ROOK]=(new ImageIcon(XChessBoard.class.getResource("Wrook.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.BLACK*Piece.PIECECOUNT+ Piece.ROOK]=(new ImageIcon(XChessBoard.class.getResource("BLrook.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.WHITE*Piece.PIECECOUNT+ Piece.BISHOP]=(new ImageIcon(XChessBoard.class.getResource("Wbishop.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.BLACK*Piece.PIECECOUNT+ Piece.BISHOP]=(new ImageIcon(XChessBoard.class.getResource("BLbishop.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.WHITE*Piece.PIECECOUNT+ Piece.KNIGHT]=(new ImageIcon(XChessBoard.class.getResource("Wknight.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.BLACK*Piece.PIECECOUNT+ Piece.KNIGHT]=(new ImageIcon(XChessBoard.class.getResource("BLknight.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.WHITE*Piece.PIECECOUNT+ Piece.KING]=(new ImageIcon(XChessBoard.class.getResource("Wking.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.BLACK*Piece.PIECECOUNT+ Piece.KING]=(new ImageIcon(XChessBoard.class.getResource("BLking.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.WHITE*Piece.PIECECOUNT+ Piece.QUEEN]=(new ImageIcon(XChessBoard.class.getResource("Wqueen.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.BLACK*Piece.PIECECOUNT+ Piece.QUEEN]=(new ImageIcon(XChessBoard.class.getResource("BLqueen.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.BLUE*Piece.PIECECOUNT+ Piece.PAWN]=(new ImageIcon(XChessBoard.class.getResource("Bpawn.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.RED*Piece.PIECECOUNT+ Piece.PAWN]=(new ImageIcon(XChessBoard.class.getResource("Rpawn.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.BLUE*Piece.PIECECOUNT+ Piece.ROOK]=(new ImageIcon(XChessBoard.class.getResource("Brook.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.RED*Piece.PIECECOUNT+ Piece.ROOK]=(new ImageIcon(XChessBoard.class.getResource("Rrook.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.BLUE*Piece.PIECECOUNT+ Piece.BISHOP]=(new ImageIcon(XChessBoard.class.getResource("Bbishop.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.RED*Piece.PIECECOUNT+ Piece.BISHOP]=(new ImageIcon(XChessBoard.class.getResource("Rbishop.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.BLUE*Piece.PIECECOUNT+ Piece.KNIGHT]=(new ImageIcon(XChessBoard.class.getResource("Bknight.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.RED*Piece.PIECECOUNT+ Piece.KNIGHT]=(new ImageIcon(XChessBoard.class.getResource("Rknight.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.BLUE*Piece.PIECECOUNT+ Piece.KING]=(new ImageIcon(XChessBoard.class.getResource("Bking.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.RED*Piece.PIECECOUNT+ Piece.KING]=(new ImageIcon(XChessBoard.class.getResource("Rking.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.BLUE*Piece.PIECECOUNT+ Piece.QUEEN]=(new ImageIcon(XChessBoard.class.getResource("Bqueen.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
     pieceImageList[Piece.RED*Piece.PIECECOUNT+ Piece.QUEEN]=(new ImageIcon(XChessBoard.class.getResource("Rqueen.gif"))).getImage().getScaledInstance(pieceSizeX,pieceSizeY,java.awt.Image.SCALE_DEFAULT);
	 }




   /**Loads the red  circle images from the directory that indicate valid pieces to be moved 
     *@requires images exist in the codebase directory
     *@effects loads the images to the circArray which is two dimensional
     *@throws Exception if the piece images are not in 
     *the code base directory
     */

  
  private void getCircles(){
     for (int i=board.MINCOL;i<=board.MAXCOL;i++)
      {
	//creates the second dimension for i'th entry
	circArray[i] = new JLabel[board.MAXROW+1];
	//for this second dimension, loads the images from the XChessBoard.class directory
	for (int j=board.MINROW;j<=board.MAXROW;j++)
         circArray[i][j] = new JLabel(new ImageIcon(XChessBoard.class.getResource("Rdot.gif")));
  	
      }
   }


   /**Loads the blue  circle images from the directory that indicate valid moves that can be made by piece being moved
     *@requires images exist in the codebase directory
     *@effects loads the images to the circArray which is two dimensional
     *@throws Exception if the piece images are not in 
     *the code base directory
     */

  
  
  private void getCircles2(){
     for (int i=board.MINCOL;i<=board.MAXCOL;i++)
      {
	//creates the second dimension for i'th entry
	circArray2[i] = new JLabel[board.MAXROW+1];
	//for this second dimension, loads the images from the XChessBoard.class directory
	for (int j=board.MINROW;j<=board.MAXROW;j++)
         circArray2[i][j] = new JLabel(new ImageIcon(XChessBoard.class.getResource("Gdot.gif")));
  	
      }
   }



 
    /**Method to place red circles on the board that indicate the pieces that can be moved
     *@effects adds the red circles on the board
     */ 

  private void placeCircles(){

  
   for (int i=board.MINCOL;i<=board.MAXCOL;i++)
      {
	for (int j=board.MINROW;j<=board.MAXROW;j++){
	 
	  JLabel circs = (JLabel)circArray[i][j];
	  circs.setBounds(i*squareSizeX+boardMargin, SIZEY-(j+1)*squareSizeY+boardMargin,
				   squareSizeX/5,squareSizeY/5);
	
	   add(circs,JLayeredPane.PALETTE_LAYER);
	  
	   circs.setVisible(false);
		       
  	}
      }
  }


//show green circles on the board for valid moves that can be made given the starting position of the piece being moved.
//only keeps track of the normal moves ..need to add switch moves and fix it for 4 player chess

  private void placeCircles2(){

  
   for (int i=board.MINCOL;i<=board.MAXCOL;i++)
      {
	for (int j=board.MINROW;j<=board.MAXROW;j++){
	 
	  JLabel circs = (JLabel)circArray2[i][j];
	  circs.setBounds(i*squareSizeX+12*boardMargin, SIZEY-(j+1)*squareSizeY+boardMargin,
				   squareSizeX/5,squareSizeY/5);
	
	   add(circs,JLayeredPane.PALETTE_LAYER);
	  
	   circs.setVisible(false);
	       
		       
  	}

      }
   
  }




  


  
   

  /** Invoked when the mouse button has been clicked (pressed
   * and released) on a component.
   */
  public void mouseClicked(MouseEvent e) {}
    
  /** Invoked when the mouse enters a component.
   */
  public void mouseEntered(MouseEvent e) {}
    
  /** Invoked when the mouse exits a component.
   */
  public void mouseExited(MouseEvent e) {}
    
  /** Invoked when the mouse button has been moved on a component (with no buttons down).
   */
  public void mouseMoved(MouseEvent e) {}

  /** Invoked when a mouse button is pressed on a component and then
   * dragged.  <code>MOUSE_DRAGGED</code> events will continue to be
   * delivered to the component where the drag originated until the
   * mouse button is released (regardless of whether the mouse position
   * is within the bounds of the component).
   * <p>
   * Due to platform-dependent Drag&Drop implementations,
   * <code>MOUSE_DRAGGED</code> events may not be delivered during a native
   * Drag&Drop operation.
   *
   */
  public void mouseDragged(MouseEvent e) {
      if (gc==null || movingLabel==null || !human.getMyTurn())
	  return;
      if (DEBUG)
	  System.out.print("d");
      movingLabel.setLocation(e.getX()+arbX+piecePaddingX,e.getY()-fourtwoMargin+arbY+piecePaddingY);
 }
    
  /** Invoked when a mouse button has been pressed on a component.
   */
  public void mousePressed(MouseEvent e) {
      if (gc==null || !human.getMyTurn())
	  return;
           
      int posX=e.getX();
      int posY=e.getY();


      startCol=(posX-boardMargin)/squareSizeX;
      arbX=(boardMargin-posX)%squareSizeX;
      startRow=(SIZEY-(posY-fourtwoMargin-boardMargin))/squareSizeY;
      arbY=(-SIZEY+(boardMargin-posY-fourtwoMargin))%squareSizeY;

      
      //System.out.println("X:"+posX+"y:"+posY+".."+"c:"+startCol+"r:"+startRow+"arby:"+arbY);

      

       if (startCol>board.MAXCOL) startCol=board.MAXCOL;
       if (startCol<board.MINCOL) startCol=board.MINCOL;

       if (startRow>board.MAXROW) startRow=board.MAXROW;
       if (startRow<board.MINROW) startRow=board.MINROW;

       Piece piece;
       
       if ((piece=board.getPieceAt(startCol,startRow))!=null)
	   
	   
 	   if(piece.getType()!=Piece.EMPTY)
 	       { 
		   for(int j=0;j<gc.getNumberofPlayers()*Piece.PIECECOUNT;j++)
		       {movingLabel=null;
		       if (labelList[j]!=null)
			   for (int i=0;i<labelList[j].size();i++)
			       if (((JLabel)labelList[j].get(i)).getBounds().contains(posX,posY))
				   {
				       movingLabel=(JLabel)labelList[j].get(i);
				       if (DEBUG)
					   System.out.println("Selected piece "+movingLabel.toString().substring(20,30));
				       moveToFront(movingLabel);
				       break;}
		       if (movingLabel!=null) break;
		       }

		    java.util.List moves = piece.getAllValidMoves(board,piece.getColor());
		    //makes the red circles visible
	
		 if (gc.getShowLegalPieces())
		   for (int i = 0; i<moves.size();i++)
		     {
		       Move m = (Move)moves.get(i);
		       int sCol = m.getStartCol();
		       int sRow = m.getStartRow();
		       JLabel circ = (JLabel)circArray[sCol][sRow];
		       circ.setLocation(sCol*squareSizeX+boardMargin, SIZEY-(sRow+1)*squareSizeY+boardMargin+fourtwoMargin);
		       circ.setVisible(true);
		       
		       
		     }

		   //makes the blue circles visible 
		 if (gc.getShowLegalMoves())
		   for (int i=0;i<moves.size();i++)
		     {
		       Move m = (Move)moves.get(i);
		       int eCol = m.getDestCol();
		       int eRow = m.getDestRow();
		       if ((m.getStartCol() == startCol) && (m.getStartRow() == startRow))
			 {
			   JLabel circ2 = (JLabel)circArray2[eCol][eRow];
			   circ2.setVisible(true);
			   

			 }
		       

		     }
		  


		   

		   mousePress=true;
	       }}
    
  /** Invoked when a mouse button has been released on a component.
   * @requires startCol and startRow non-zero starting row and col of the movingLabel
   * && movingLabel!=null && gc!=null && ( squareSizeX!=0 && squareSizeY!=0) 
   */
  public void mouseReleased(MouseEvent e) {
      if (gc==null) 
	  return;
     //hides the red circles once the piece is not being moved

       for (int i=board.MINCOL;i<=board.MAXCOL;i++)
      {
	for (int j=board.MINROW;j<=board.MAXROW;j++){
	 
	  JLabel circs = (JLabel)circArray[i][j];
	   circs.setVisible(false);
  	}
      }
       //hides the blue circles once the piece is not being moved

       for (int i=board.MINCOL;i<=board.MAXCOL;i++)
	 {
	   for (int j=board.MINROW;j<=board.MAXROW;j++){
	     
	     circArray2[i][j].setVisible(false);
	   }
	 }





      if (movingLabel==null || !human.getMyTurn())
	  return;

      if(mousePress)
	  mousePress = false;

       endCol=(e.getX()-boardMargin)/squareSizeX;
       endRow=((SIZEY-e.getY()+fourtwoMargin)+boardMargin)/squareSizeY;


       //if the piece is dropped out of the board then,
       //put it into the board
       if (endCol>board.MAXCOL) endCol=board.MAXCOL;
       if (endCol<board.MINCOL) endCol=board.MINCOL;

       if (endRow>board.MAXROW) endRow=board.MAXROW;
       if (endRow<board.MINROW) endRow=board.MINROW;
       
       //check if the moveMade is in the possible moves list
       //if it is, make the move and tell it to humanPlayer. 
       //Else, put the piece into the old place.

       Move moveMade=new Move(startCol,startRow,endCol,endRow);

       if (human.getMoveList().contains(moveMade))
	   { movingLabel.setLocation(endCol*squareSizeX+piecePaddingX+boardMargin,
				 SIZEY-(endRow+1)*squareSizeY+piecePaddingY+boardMargin+fourtwoMargin);       
	   human.setMove(moveMade);}
	   
       else
	   movingLabel.setLocation(startCol*squareSizeX+piecePaddingX+boardMargin,
				   SIZEY-(startRow+1)*squareSizeY+piecePaddingY+boardMargin+fourtwoMargin); 

       moveToBack(movingLabel);
       movingLabel=null;
      
    }

  /**Sets the GameController handler to the specified gamecontroller
   *@requires gc instance of GameController
   *@effects GameController=gc
   */

  public void setGameController(GameController gc){

      removeAll(); //remove all the previously registered components  
      repaint();

      this.gc=gc;
    
      if (gc!=null)
	  {
	
	  initBoard();
	  getImages();
	  getCircles();
	   getCircles2();
	  setBoardImage();
	  placePieces();
	  placeCircles();
	  placeCircles2();
	  }

  }

  /**Initalizes the board variables
   *@requires gc!=null
   *@effects board=gc.getBoard() && squareSizeX && squareSizeY && human
   */
    private void initBoard(){
	board=gc.getBoard();
        squareSizeX=SIZEX/(board.MAXCOL-board.MINCOL+1);
        squareSizeY=SIZEY/(board.MAXROW-board.MINROW+1);       
	human=gc.getHumanPlayer();

	labelList=new ArrayList[gc.getNumberofPlayers()*Piece.PIECECOUNT];

	pieceSizeX=squareSizeX-piecePaddingX*(gc.getNumberofPlayers()==2?4:2);
	pieceSizeY=squareSizeY-piecePaddingY*(gc.getNumberofPlayers()==2?4:2);

	if (gc.getNumberofPlayers()==2)
	    {	
		//position start number/letter
		startLetter='a';
		startNumber='8';
		//margin 
	    	fourtwoMargin=0;
	    }
	else { fourtwoMargin=(-10);
	       //position start number/letter
		startLetter='a';
		startNumber='e'; 
	}
		//for red circles
	circArray = new JLabel [board.MAXCOL+1][];
	//for blue circles
	circArray2 = new JLabel [board.MAXCOL+1][];
	
    }


 /**Sets the board image for background.
  *Paints squares in given BLACKCOLOR and WHITECOLOR.
  *Adds position letters/numbers at the bottom and on 
  *the right
  *@effects background image
  */
    private void setBoardImage(){

	Image boardImage=createImage(SIZEX+letterMargin,SIZEY+letterMargin);
	Graphics g=boardImage.getGraphics();

	

	//draw squares
	for (int i=board.MINCOL;i<=board.MAXCOL;i++)
	  { 
            for (int j=board.MINROW;j<=board.MAXROW;j++)
		if (board.getUsable(i,j))
		    {
			g.setColor(BLACKCOLOR);
			g.drawRect(i*squareSizeX, j*squareSizeY, squareSizeX, squareSizeY);

			if ((i+j)%2==0) {g.setColor(WHITECOLOR);}
			else g.setColor(BLACKCOLOR);
			
			g.fillRect(i*squareSizeX, j*squareSizeY, squareSizeX, squareSizeY);
		    }
	  }
	//draw position letters/numbers
	//this uses java.lang.Character.forDigit() and and digit()
	//in order to accodomote chaging number of rows and columns
	//this also uses getFontMetrics.getHeight() for bottom letters
	//to be far away enough from the actual board.
	char[] position=new char[1];
	g.setColor(BLACKCOLOR);
	int padding=g.getFontMetrics().getHeight();


	for (int i=board.MINCOL;i<=board.MAXCOL;i++)
	    {position[0]=java.lang.Character.forDigit(java.lang.Character.digit(startLetter,
					      java.lang.Character.MAX_RADIX)+i,java.lang.Character.MAX_RADIX);
	    g.drawChars(position,0,1,i*squareSizeX+squareSizeX/2,SIZEY+padding);}
 
        for (int j=board.MINROW;j<=board.MAXROW;j++)
	    {position[0]=java.lang.Character.forDigit(java.lang.Character.digit(startNumber,
					      java.lang.Character.MAX_RADIX)-j,java.lang.Character.MAX_RADIX);

	    g.drawChars(position,0,1,SIZEX+letterMargin/2,j*squareSizeY+squareSizeY/2);}



	//now set the image as background image
	background = new JLabel(new ImageIcon(boardImage));
	background.setBounds(boardMargin,boardMargin,SIZEX+letterMargin,SIZEY+letterMargin);

	add(background,JLayeredPane.DEFAULT_LAYER);

    }


    /**Method to place pieces on the board
     *@effects adds the pieces on the board and creates a labelList 
     * which keeps links to the labels on the board
     */ 
    private void placePieces()
    { 
	JLabel newLabel;
	Piece piece;

	for (int i=board.MINCOL;i<=board.MAXCOL;i++)
	  {
	      for (int j=board.MINROW;j<=board.MAXROW;j++)
		if ((piece=board.getPieceAt(i,j))!=null)
		  if(piece.getType()!=Piece.EMPTY)
		      {
			  int pIndex=piece.getType()+Piece.PIECECOUNT*piece.getColor();
			  

			  //check if there are any pieces at all
			  //if not create a new piece Arraylist
			  if (labelList[pIndex]==null)
			      labelList[pIndex]=new ArrayList();
		      
			  //create the new label and place it on the board
			  newLabel=new JLabel(new ImageIcon(pieceImageList[pIndex]));
			  newLabel.setBounds(i*squareSizeX+boardMargin+piecePaddingX, SIZEY-(j+1)*squareSizeY+boardMargin+fourtwoMargin+piecePaddingY,
				   squareSizeX-2*piecePaddingX,squareSizeY-2*piecePaddingY);

			  //add the label to the labelList and to the board
			  labelList[pIndex].add(newLabel);
			  add(newLabel,JLayeredPane.PALETTE_LAYER);
		      }
	      
	  }

    }


    /**Method to update pieces on the board
     * Goes through board, and if there is a piece at (i,j)
     * takes a label from labelList, puts the necessary image on it
     * places it on the right place.
     * This method discards labels if there are less pieces on the board.
     * @modifies the piece arrangement on the screen and labelList
     * @requires labelList.size()>=numberOfPiecesonBoard
    */ 
    public void updateBoard(){
	

	int[] alive =new int[gc.getNumberofPlayers()*Piece.PIECECOUNT];
	JLabel newLabel;
	Piece piece;

	for (int i=board.MINCOL;i<=board.MAXCOL;i++)
	  {
	      for (int j=board.MINROW;j<=board.MAXROW;j++)
		if ((piece=board.getPieceAt(i,j))!=null)
		  if(piece.getType()!=Piece.EMPTY)
		      {
			  int pIndex=piece.getType()+Piece.PIECECOUNT*piece.getColor();
			  //check if there are any pieces at all
			  //if not create a new piece Arraylist
			
			  if (labelList[pIndex]==null)
			      labelList[pIndex]=new ArrayList();
		      
			  
			  //check if we still have not-used labels
			  //if not create a new one
			  if (labelList[pIndex].size()==alive[pIndex])
			      {
				  newLabel=new JLabel(new ImageIcon(pieceImageList[pIndex]));
				   //add the label to the labelList and to the board	  
			          newLabel.setBounds(i*squareSizeX+boardMargin+piecePaddingX, SIZEY-(j+1)*squareSizeY+boardMargin+fourtwoMargin+piecePaddingY,
						     squareSizeX-2*piecePaddingX,squareSizeY-2*piecePaddingY);
				  labelList[pIndex].add(newLabel);
				  add(newLabel,JLayeredPane.PALETTE_LAYER);
				 
			      }

			      
			      
			  //get the next label from the labelList
			  newLabel=(JLabel)labelList[pIndex].get(alive[pIndex]++);
			  newLabel.setVisible(true);

			  //set its location accordingly
			  newLabel.setLocation(i*squareSizeX+boardMargin+piecePaddingX, SIZEY-(j+1)*squareSizeY+boardMargin+fourtwoMargin+piecePaddingY);
		       
		      }	  
	  }

	//discard unnecessary labels
	//remove them as well	
	for(int i=0;i<gc.getNumberofPlayers()*Piece.PIECECOUNT;i++)
	    if(labelList[i]!=null)
		{int aliveCopy=alive[i];
		for(;alive[i]<labelList[i].size();alive[i]++)
		    ((JLabel)labelList[i].get(alive[i])).setVisible(false);
		while(aliveCopy<labelList[i].size())
		    labelList[i].remove(aliveCopy);
		}


	


    }


}    

