/*
 AntiChess, an antichess game

 Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
 (For copyright info please see COPYRIGHT file under the base directory)
 
 For bugs, contact info:
 barisy@mit.edu or hongping@mit.edu
*/
package antichess;


import java.util.*;
import java.io.*;
/**
 * <p>
 * TextFields reads text fields of AntiChess from the chosen text file.
 * This way, multiple languages can be defined for AntiChess.
 *
 * <p>
 * TextFields defines some public constants for use in the Game.
 * TextFields is immutable.
 */



public class TextFields {

  /**********************************************************/
  /**********************************************************/
  // FIELDS 
  /**********************************************************/
  /**********************************************************/

  //DEFAULT

    private static final String defaultLanguage="english";

  //GENERAL

    public static String ProgramName; // Program Name
    public static String Version; // Program Version
    public static String OK; 
    public static String Cancel;
    public static String InitialTime;
    public static String Human;
    public static String Computer;
    public static String Vs;
    public static String Color;
    public static String White;
    public static String Black;
    public static String Red;
    public static String Purple;

    //Main Menu
   
    public static String Game;
    public static String Help;

 //GameMenu

    public static String NewGame;

        //NewGameMenu
        public static String TwoPlayers;
        public static String FourPlayers;
                //2 Player Game MENU
                      public static String TwoPlayerTitle;
                      public static String FourPlayerTitle;
			  public static String Player1;
			  public static String Player2;
			  public static String Player3;
			  public static String Player4;
			  public static String Name;
			  public static String Side;
			  public static String Style;
			  public static String Type;
			  public static String Time;
			  public static String GameName;
			  public static String GameStyle;
			  public static String BoardStyle;
			  public static String Cylinderical;

     


    public static String OpenGame;
    public static String SaveGame;
    public static String Settings;
        //SettingsMenu
       public static String ShowLegalMoves;
       public static String ShowTime;
       public static String ShowPieces;
	    
    public static String Quit; 

    //HelpMenu
    public static String Contents;
    public static String About;
 
  /**********************************************************/
  /**********************************************************/
  //CONSTRUCTOR
  /**********************************************************/
  /**********************************************************/
 
  /**<p>Creates a TextFields object, and loads the corresponding text
   * fields from the default language file.
   * <p>There is a template file for language files. For adding additional
   * languages, just open "language.txt" with a text editor and enter the
   * corresponding values. You should leave write the values in double quotes.
   * For an example, please look at the "english.txt"
   * 
   * @effects creates a TextFields object.
   * @throws FileNotFoundException if the defaultLanguage file cannot be found
   */

    public TextFields(){
	this(defaultLanguage);
    }



  /**<p>Creates a TextFields object, and loads the corresponding text
   * fields from the language file.
   * @effects creates a TextFields object.
   * @throws FileNotFoundException if the defaultLanguage file cannot be found (when defaultLanguage file
   * is used)
   */
   public TextFields(String languageName)
    {
	StringTokenizer newLine;
	String answer="";
	BufferedReader in=null; // file is read into this buffer

	//see if file exists, if not, read it from defaultLanguage
	try{
                 
                
               if (languageName.equals(defaultLanguage))
		    
		in=new BufferedReader(new InputStreamReader(TextFields.class.getResourceAsStream(defaultLanguage+".txt")));

	       else

		   in = new BufferedReader(new InputStreamReader(TextFields.class.getResourceAsStream(languageName+".txt")));   
	        for (String line = in.readLine(); line != null; line = in.readLine()) 
	        answer += line + "\n";


		}
		catch (Exception e)
		    {
			e.printStackTrace(); }
		 
	

	    


	newLine=new StringTokenizer(answer,"\"");
	//GENERAL

	
        newLine.nextToken();
	ProgramName=newLine.nextToken();

	newLine.nextToken();
	Version=newLine.nextToken();
	
        newLine.nextToken();
	OK=newLine.nextToken();

	newLine.nextToken();
	Cancel=newLine.nextToken();
	
        newLine.nextToken();
	InitialTime=newLine.nextToken();

	newLine.nextToken();
	Human=newLine.nextToken();

	newLine.nextToken();
	Computer=newLine.nextToken();

	newLine.nextToken();
        Vs=newLine.nextToken();	

	newLine.nextToken();
        Color=newLine.nextToken();	

	newLine.nextToken();
        White=newLine.nextToken();	

	newLine.nextToken();
        Black=newLine.nextToken();	

	newLine.nextToken();
        Red=newLine.nextToken();	

	newLine.nextToken();
        Purple=newLine.nextToken();	

	//MAINMENU
        

	newLine.nextToken();
	Game=newLine.nextToken();

	newLine.nextToken();
	Help=newLine.nextToken();


	//GAMEMENU

	newLine.nextToken();
	NewGame=newLine.nextToken();

	      //NEW GAME MENU
	      newLine.nextToken();
	      TwoPlayers=newLine.nextToken();
      

	      newLine.nextToken();
	      FourPlayers=newLine.nextToken();


	          // 2 Player game MENU
			  
	          newLine.nextToken();
	          TwoPlayerTitle=newLine.nextToken();
			  
	          newLine.nextToken();
	          FourPlayerTitle=newLine.nextToken();

		  newLine.nextToken();
		  Player1=newLine.nextToken();
		  
		  newLine.nextToken();
		  Player2=newLine.nextToken();

		  newLine.nextToken();
		  Player3=newLine.nextToken();
		  
		  newLine.nextToken();
		  Player4=newLine.nextToken();
		  
		  newLine.nextToken();
		  Name=newLine.nextToken();
		  
		  newLine.nextToken();
		  Side=newLine.nextToken();
		  
		  newLine.nextToken();
		  Style=newLine.nextToken();
		  
		  newLine.nextToken();
		  Type=newLine.nextToken();
		  
		  newLine.nextToken();
		  Time=newLine.nextToken();
		  
		  newLine.nextToken();
		  GameName=newLine.nextToken();
		  
		  newLine.nextToken();
		  GameStyle=newLine.nextToken();
		  
		  newLine.nextToken();
		  BoardStyle=newLine.nextToken();
		  
		  newLine.nextToken();
		  Cylinderical=newLine.nextToken();

	
	newLine.nextToken();
	OpenGame=newLine.nextToken();

	newLine.nextToken();
	SaveGame=newLine.nextToken();

	newLine.nextToken();
	Settings=newLine.nextToken();


	      //SETTINGS GAME MENU
	      newLine.nextToken();
	      ShowLegalMoves=newLine.nextToken();

	      newLine.nextToken();
	      ShowTime=newLine.nextToken();

	      newLine.nextToken();
	      ShowPieces=newLine.nextToken();



	newLine.nextToken();
        Quit=newLine.nextToken();

	//Help Menu

	newLine.nextToken();
	Contents=newLine.nextToken();

        newLine.nextToken();
        About=newLine.nextToken();

    }	

}
