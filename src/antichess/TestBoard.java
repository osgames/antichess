/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;
import junit.framework.*;
import java.io.*;
import java.util.*;

public class TestBoard extends TestCase{
      //4x4 square  
    private int[][] topology0={ {1,1,1,1},
				{1,1,1,1},
				{1,1,1,1},
				{1,1,1,1}};
      //4x4 square with the middle 4 squares not included 
    private int[][] topology1={ {1,1,1,1},
				{1,0,0,1},
				{1,0,0,1},
				{1,1,1,1}};   

      //8x8 square      
private int[][] topology2={ {1,1,1,1,1,1,1,1},
			    {1,1,1,1,1,1,1,1},
			    {1,1,1,1,1,1,1,1},
			    {1,1,1,1,1,1,1,1},
			    {1,1,1,1,1,1,1,1},
			    {1,1,1,1,1,1,1,1},
			    {1,1,1,1,1,1,1,1},
			    {1,1,1,1,1,1,1,1}};
      //8x8 square with (8,8) not included  
    private int[][] topology3={{1,1,1,1,1,1,1,1},
			       {1,1,1,1,1,1,1,1},
			       {1,1,1,1,1,1,1,1},
			       {1,1,1,1,1,1,1,1},
			       {1,1,1,1,1,1,1,1},
			       {1,1,1,1,1,1,1,1},
			       {1,1,1,1,1,1,1,1},
			       {1,1,1,1,1,1,1,0}};

   
    private Board board0     = null; 
    private Board board1     = null; 
    private Board board2     = null;
    private Board board3     = null;
    private Board board4     = null;

  public TestBoard(String name) {
    super(name);
  }


    /**Helper Function
     * Checks whether given piece is in array
     * @requires b!=null && array!=null && p!=null
     * @returns true if p is in array
     */
    private boolean hasInside(Board b,Piece[][] array, Piece p)
    {   int color=p.getColor();
        int type=p.getType();
	int numberofpieces=b.getPieceCount(color,type);
	for(int i=0;i<numberofpieces;i++)
	    if (array[type][i]!=null)
		if ((array[type][i]).equals(p)) return true;
	return false;
    }


  protected void setUp(){
      board0=new Board(topology0);
      board1=new Board(topology1);
      board2=new Board(topology2);
      board3=new Board(topology3);
      board4=new Board(topology2);
  }


    public void testChecked(){
	board0.setChecked(Piece.WHITE,true);
        //black should be false initially
	assertTrue("Board.getChecked: should be true for WHITE",board0.getChecked(Piece.WHITE));
	assertTrue("Board.getChecked: should be false for BLACK",!board0.getChecked(Piece.BLACK));
        //white is true
	board0.setChecked(Piece.BLACK,true);
        assertTrue("Board.getChecked: should be true for WHITE",board0.getChecked(Piece.BLACK));
	assertTrue("Board.getChecked: should be true for BLACK",board0.getChecked(Piece.BLACK));
	board0.setChecked(Piece.WHITE,false);
	board0.setChecked(Piece.BLACK,true);
        assertTrue("Board.getChecked: should be false for WHITE",!board0.getChecked(Piece.WHITE));
	assertTrue("Board.getChecked: should be true for BLACK",board0.getChecked(Piece.BLACK));
    }



    public void testSpecial(){
	board0.setSpecial(Piece.WHITE,true);
        //black should be false initially
	assertTrue("Board.getSpecial: should be true for WHITE",board0.getSpecial(Piece.WHITE));
	assertTrue("Board.getSpecial: should be false for BLACK",!board0.getSpecial(Piece.BLACK));
        //white is true
	board0.setSpecial(Piece.BLACK,true);
        assertTrue("Board.getSpecial: should be true for WHITE",board0.getSpecial(Piece.BLACK));
	assertTrue("Board.getSpecial: should be true for BLACK",board0.getSpecial(Piece.BLACK));
	board0.setSpecial(Piece.WHITE,false);
	board0.setSpecial(Piece.BLACK,true);
        assertTrue("Board.getSpecial: should be false for WHITE",!board0.getSpecial(Piece.WHITE));
	assertTrue("Board.getSpecial: should be true for BLACK",board0.getSpecial(Piece.BLACK));
    }

    //also checks getPieceAt, getPieceCount, getAliveCount, getPiecesOnBoard, 

    public void testRegisterPiece(){


	//assumes Piece constructors work fine


	//adds white pieces
	Piece pawn0=new Pawn('p',board2,0,1,Piece.WHITE);
	Piece pawn1=new Pawn('p',board2,1,1,Piece.WHITE);
	Piece pawn2=new Pawn('p',board2,2,1,Piece.WHITE);
	Piece pawn3=new Pawn('p',board2,3,1,Piece.WHITE);
	Piece pawn4=new Pawn('p',board2,4,1,Piece.WHITE);
	Piece pawn5=new Pawn('p',board2,5,1,Piece.WHITE);
	Piece pawn6=new Pawn('p',board2,6,1,Piece.WHITE);
	Piece pawn7=new Pawn('p',board2,7,1,Piece.WHITE);
	Piece rook0=new Rook('r',board2,0,0,Piece.WHITE);
	Piece knight0=new Knight('n',board2,1,0,Piece.WHITE);
	Piece bishop0=new Bishop('b',board2,2,0,Piece.WHITE);
	Piece king0=new King('k',board2,3,0,Piece.WHITE);
	Piece queen0=new Queen('q',board2,4,0,Piece.WHITE);
	Piece bishop1=new Bishop('b',board2,5,0,Piece.WHITE);
	Piece knight1=new Knight('n',board2,6,0,Piece.WHITE);
	Piece rook1=new Rook('r',board2,7,0,Piece.WHITE);

        //Piece constructor implicitly calls registerPiece on board
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(0,1),pawn0);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(1,1),pawn1);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(2,1),pawn2);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(3,1),pawn3);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(4,1),pawn4);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(5,1),pawn5);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(6,1),pawn6);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(7,1),pawn7);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(0,0),rook0);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(1,0),knight0);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(2,0),bishop0);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(3,0),king0);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(4,0),queen0);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(5,0),bishop1);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(6,0),knight1);
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(7,0),rook1);


	assertEquals("Board.registerPiece did not update pieceCount right!",8,board2.getPieceCount(Piece.WHITE,Piece.PAWN));
	assertEquals("Board.registerPiece did not update pieceCount right!",2,board2.getPieceCount(Piece.WHITE,Piece.ROOK));
	assertEquals("Board.registerPiece did not update pieceCount right!",2,board2.getPieceCount(Piece.WHITE,Piece.KNIGHT));
	assertEquals("Board.registerPiece did not update pieceCount right!",2,board2.getPieceCount(Piece.WHITE,Piece.BISHOP));
	assertEquals("Board.registerPiece did not update pieceCount right!",1,board2.getPieceCount(Piece.WHITE,Piece.QUEEN));
	assertEquals("Board.registerPiece did not update pieceCount right!",1,board2.getPieceCount(Piece.WHITE,Piece.KING));
	assertEquals("Board.registerPiece did not update pieceCount right!",0,board2.getPieceCount(Piece.BLACK,Piece.PAWN));
	assertEquals("Board.registerPiece did not update pieceCount right!",0,board2.getPieceCount(Piece.BLACK,Piece.ROOK));
	assertEquals("Board.registerPiece did not update pieceCount right!",0,board2.getPieceCount(Piece.BLACK,Piece.KNIGHT));
	assertEquals("Board.registerPiece did not update pieceCount right!",0,board2.getPieceCount(Piece.BLACK,Piece.BISHOP));
	assertEquals("Board.registerPiece did not update pieceCount right!",0,board2.getPieceCount(Piece.BLACK,Piece.QUEEN));
	assertEquals("Board.registerPiece did not update pieceCount right!",0,board2.getPieceCount(Piece.BLACK,Piece.KING));


	assertEquals("Board.registerPiece did not update aliveCount right!",8,board2.getAliveCount(Piece.WHITE,Piece.PAWN));
	assertEquals("Board.registerPiece did not update aliveCount right!",2,board2.getAliveCount(Piece.WHITE,Piece.ROOK));
	assertEquals("Board.registerPiece did not update aliveCount right!",2,board2.getAliveCount(Piece.WHITE,Piece.KNIGHT));
	assertEquals("Board.registerPiece did not update aliveCount right!",2,board2.getAliveCount(Piece.WHITE,Piece.BISHOP));
	assertEquals("Board.registerPiece did not update aliveCount right!",1,board2.getAliveCount(Piece.WHITE,Piece.QUEEN));
	assertEquals("Board.registerPiece did not update aliveCount right!",1,board2.getAliveCount(Piece.WHITE,Piece.KING));
	assertEquals("Board.registerPiece did not update aliveCount right!",0,board2.getAliveCount(Piece.BLACK,Piece.PAWN));
	assertEquals("Board.registerPiece did not update aliveCount right!",0,board2.getAliveCount(Piece.BLACK,Piece.ROOK));
	assertEquals("Board.registerPiece did not update aliveCount right!",0,board2.getAliveCount(Piece.BLACK,Piece.KNIGHT));
	assertEquals("Board.registerPiece did not update aliveCount right!",0,board2.getAliveCount(Piece.BLACK,Piece.BISHOP));
	assertEquals("Board.registerPiece did not update aliveCount right!",0,board2.getAliveCount(Piece.BLACK,Piece.QUEEN));
	assertEquals("Board.registerPiece did not update aliveCount right!",0,board2.getAliveCount(Piece.BLACK,Piece.KING));
	
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), pawn0));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), pawn1));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), pawn2));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), pawn3));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), pawn4));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), pawn5));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), pawn6));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), pawn7));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), rook0));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), rook1));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), knight0));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), knight1));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), bishop0));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), bishop1));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), queen0));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), king0));


        //put in other pieces and see how registerPiece handles the situation
	Piece pawn8=new Pawn('p',board2,0,2,Piece.WHITE);        
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(0,2),pawn8);        
	assertEquals("Board.registerPiece did not update pieceCount right!",9,board2.getPieceCount(Piece.WHITE,Piece.PAWN));
	assertEquals("Board.registerPiece did not update aliveCount right!",9,board2.getAliveCount(Piece.WHITE,Piece.PAWN));
	assertTrue("Board.registerPiece did not update pieceArray correctly!",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), pawn8));

	Piece pawn9=new Pawn('p',board2,0,2,Piece.WHITE); 
	pawn9.setLastLocation(9,0); //change this so that it is not .equals(pawn8)
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(0,2),pawn9);        
	assertEquals("Board.registerPiece did not update pieceCount right!",9,board2.getPieceCount(Piece.WHITE,Piece.PAWN));
	assertEquals("Board.registerPiece did not update aliveCount right!",9,board2.getAliveCount(Piece.WHITE,Piece.PAWN));
	assertTrue("Board.registerPiece did not update pieceArray correctly!pawn9 should be put in",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), pawn9));
	assertTrue("Board.registerPiece did not update pieceArray correctly!pawn8 should be not in",
		   !hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), pawn8));

	Piece rook2=new Rook('r',board2,0,2,Piece.WHITE); 
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(0,2),rook2);        
	assertEquals("Board.registerPiece did not update pieceCount right!",9,board2.getPieceCount(Piece.WHITE,Piece.PAWN));
	assertEquals("Board.registerPiece did not update aliveCount right!",8,board2.getAliveCount(Piece.WHITE,Piece.PAWN));
	assertEquals("Board.registerPiece did not update pieceCount right!",3,board2.getPieceCount(Piece.WHITE,Piece.ROOK));
	assertEquals("Board.registerPiece did not update aliveCount right!",3,board2.getAliveCount(Piece.WHITE,Piece.ROOK));
	assertTrue("Board.registerPiece did not update pieceArray correctly! pawn9 should have been deleted",
		   !hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), pawn9));
	assertTrue("Board.registerPiece did not update pieceArray correctly! rook2 should have been put in",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), rook2));

	//put pawn back on the rook
	pawn9=new Pawn('p',board2,0,2,Piece.WHITE); 
	pawn9.setLastLocation(9,0); //change this so that it is not .equals(pawn8)
	assertEquals("Board.registerPiece did not register right!",board2.getPieceAt(0,2),pawn9);        
	assertEquals("Board.registerPiece did not update pieceCount right!",9,board2.getPieceCount(Piece.WHITE,Piece.PAWN));
	assertEquals("Board.registerPiece did not update aliveCount right!",9,board2.getAliveCount(Piece.WHITE,Piece.PAWN));
	assertEquals("Board.registerPiece did not update pieceCount right!",3,board2.getPieceCount(Piece.WHITE,Piece.ROOK));
	assertEquals("Board.registerPiece did not update aliveCount right!",2,board2.getAliveCount(Piece.WHITE,Piece.ROOK));
	assertTrue("Board.registerPiece did not update pieceArray correctly!pawn9 should be put in",
		   hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), pawn9));
	assertTrue("Board.registerPiece did not update pieceArray correctly!rook2 should be not in",
		   !hasInside(board2,board2.getPiecesOnBoard(Piece.WHITE), rook2));


}
	

    public void testBoardCopy(){
	//adds white pieces
	Piece pawn0=new Pawn('p',board3,0,1,Piece.WHITE);
	Piece pawn1=new Pawn('p',board3,1,1,Piece.WHITE);
	Piece pawn2=new Pawn('p',board3,2,1,Piece.WHITE);
	Piece pawn3=new Pawn('p',board3,3,1,Piece.WHITE);
	Piece pawn4=new Pawn('p',board3,4,1,Piece.WHITE);
	Piece pawn5=new Pawn('p',board3,5,1,Piece.WHITE);
	Piece pawn6=new Pawn('p',board3,6,1,Piece.WHITE);
	Piece pawn7=new Pawn('p',board3,7,1,Piece.WHITE);
	Piece rook0=new Rook('r',board3,0,0,Piece.WHITE);
	Piece knight0=new Knight('n',board3,1,0,Piece.WHITE);
	Piece bishop0=new Bishop('b',board3,2,0,Piece.WHITE);
	Piece king0=new King('k',board3,3,0,Piece.WHITE);
	Piece queen0=new Queen('q',board3,4,0,Piece.WHITE);
	Piece bishop1=new Bishop('b',board3,5,0,Piece.WHITE);
	Piece knight1=new Knight('n',board3,6,0,Piece.WHITE);
	Piece rook1=new Rook('r',board3,7,0,Piece.WHITE);

	Board board4=new Board(board3);
	assertEquals("Board.constructor: not equal to clone",board3.toString(),board4.toString());
	
    }


    public void testGetUsable(){
	assertTrue("Board.getUsable: is not working",board1.getUsable(0,0));
	assertTrue("Board.getUsable: is not working",board1.getUsable(0,1));
	assertTrue("Board.getUsable: is not working",board1.getUsable(0,2));
	assertTrue("Board.getUsable: is not working",board1.getUsable(0,3));
	assertTrue("Board.getUsable: is not working",board1.getUsable(1,0));
	assertTrue("Board.getUsable: is not working",!board1.getUsable(1,1));
	assertTrue("Board.getUsable: is not working",!board1.getUsable(1,2));
	assertTrue("Board.getUsable: is not working",board1.getUsable(1,3));	
	assertTrue("Board.getUsable: is not working",board1.getUsable(2,0));
	assertTrue("Board.getUsable: is not working",!board1.getUsable(2,1));
	assertTrue("Board.getUsable: is not working",!board1.getUsable(2,2));
	assertTrue("Board.getUsable: is not working",board1.getUsable(2,3));
	assertTrue("Board.getUsable: is not working",board1.getUsable(3,0));
	assertTrue("Board.getUsable: is not working",board1.getUsable(3,1));
	assertTrue("Board.getUsable: is not working",board1.getUsable(3,2));
	assertTrue("Board.getUsable: is not working",board1.getUsable(3,3));	
    }


    public void testLastPieceMoved(){
	//adds white pieces
	Piece pawn0=new Pawn('p',board3,0,1,Piece.WHITE);
	Piece pawn1=new Pawn('p',board3,1,1,Piece.WHITE);
	Piece pawn2=new Pawn('p',board3,2,1,Piece.WHITE);
	Piece pawn3=new Pawn('p',board3,3,1,Piece.WHITE);
	Piece pawn4=new Pawn('p',board3,4,1,Piece.WHITE);
	Piece pawn5=new Pawn('p',board3,5,1,Piece.WHITE);
	Piece pawn6=new Pawn('p',board3,6,1,Piece.BLACK);
	Piece pawn7=new Pawn('p',board3,7,1,Piece.BLACK);
	Piece rook0=new Rook('r',board3,0,0,Piece.BLACK);
	Piece knight0=new Knight('n',board3,1,0,Piece.BLACK);
	Piece bishop0=new Bishop('b',board3,2,0,Piece.BLACK);
	Piece king0=new King('k',board3,3,0,Piece.BLACK);
	Piece queen0=new Queen('q',board3,4,0,Piece.BLACK);
	Piece bishop1=new Bishop('b',board3,5,0,Piece.BLACK);
	Piece knight1=new Knight('n',board3,6,0,Piece.BLACK);
	Piece rook1=new Rook('r',board3,7,0,Piece.BLACK);
	
	board3.setLastPieceMoved(pawn0);
	board3.setLastPieceMoved(rook1);
	
	assertTrue("Board.set/getLastPieceMoved is not working!",(board3.getLastPieceMoved(Piece.WHITE)).equals(pawn0));
	assertTrue("Board.set/getLastPieceMoved is not working!",(board3.getLastPieceMoved(Piece.BLACK)).equals(rook1));
	
	board3.setLastPieceMoved(pawn1);
	board3.setLastPieceMoved(knight1);
	
	assertTrue("Board.set/getLastPieceMoved is not working!",(board3.getLastPieceMoved(Piece.WHITE)).equals(pawn1));
	assertTrue("Board.set/getLastPieceMoved is not working!",(board3.getLastPieceMoved(Piece.BLACK)).equals(knight1));
    }

    public void testSetPieceAtandKing(){
	//adds white pieces
	Piece pawn0=new Pawn('p',board3,0,1,Piece.WHITE);
	Piece pawn1=new Pawn('p',board3,1,1,Piece.WHITE);
	Piece pawn2=new Pawn('p',board3,2,1,Piece.WHITE);
	Piece pawn3=new Pawn('p',board3,3,1,Piece.WHITE);
	Piece pawn4=new Pawn('p',board3,4,1,Piece.WHITE);
	Piece pawn5=new Pawn('p',board3,5,1,Piece.WHITE);
	Piece pawn6=new Pawn('p',board3,6,1,Piece.BLACK);
	Piece pawn7=new Pawn('p',board3,7,1,Piece.BLACK);
	Piece rook0=new Rook('r',board3,0,0,Piece.BLACK);
	Piece knight0=new Knight('n',board3,1,0,Piece.BLACK);
	Piece bishop0=new Bishop('b',board3,2,0,Piece.BLACK);
	Piece king0=new King('k',board3,3,0,Piece.BLACK);
	Piece queen0=new Queen('q',board3,4,0,Piece.BLACK);
	Piece bishop1=new Bishop('b',board3,5,0,Piece.BLACK);
	Piece knight1=new Knight('n',board3,6,0,Piece.BLACK);
	Piece rook1=new Rook('r',board3,7,0,Piece.BLACK);

	board3.setPieceAt(5,5,rook1);
	assertTrue("Board.setPieceAt()",(board3.getPieceAt(5,5)).equals(rook1));
 
	assertTrue("Board.getKing()",(board3.getKing(Piece.BLACK)).equals(king0));
	assertNull("Board.getKing()",board3.getKing(Piece.WHITE));
   }


    public void testGetDifference(){
	//adds white pieces
	Piece pawn0=new Pawn('p',board4,0,1,Piece.WHITE);
	Piece pawn1=new Pawn('p',board4,1,1,Piece.WHITE);
	Piece pawn2=new Pawn('p',board4,2,1,Piece.WHITE);

	//adds black pawns
	Piece pawn4=new Pawn('p',board4,4,1,Piece.BLACK);
	Piece pawn5=new Pawn('p',board4,5,1,Piece.BLACK);
	Piece pawn6=new Pawn('p',board4,6,1,Piece.BLACK);

	assertEquals("Board.getDiffence() is wrong for equal number of pawns",0,board4.getDifference(Piece.WHITE));
	assertEquals("Board.getDiffence() is wrong for equal number of pawns",0,board4.getDifference(Piece.BLACK));


	Piece pawn3=new Pawn('p',board4,3,1,Piece.WHITE);

	assertEquals("Board.getDiffence() is wrong for one extra WHITE pawn",-1*Piece.PIECEVALUE[Piece.PAWN],board4.getDifference(Piece.WHITE));
	assertEquals("Board.getDiffence() is wrong for one extra WHITE pawn",Piece.PIECEVALUE[Piece.PAWN],board4.getDifference(Piece.BLACK));
    }


public void testBoard(){
}


  // Tell JUnit what order to run the tests in
  public static Test suite()
  { 
    TestSuite suite = new TestSuite(); 
    suite.addTest(new TestBoard("testChecked"));
    suite.addTest(new TestBoard("testSpecial"));
    suite.addTest(new TestBoard("testRegisterPiece"));
    suite.addTest(new TestBoard("testBoardCopy"));
    suite.addTest(new TestBoard("testGetUsable")); 
    suite.addTest(new TestBoard("testLastPieceMoved"));
    suite.addTest(new TestBoard("testSetPieceAtandKing"));
    suite.addTest(new TestBoard("testGetDifference"));
  return suite;
  }
}
