/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/

package antichess;

import java.util.*;
import java.io.*;

/**
 *
 * This class controls the main flow of the game.
 * This class also performs reading in of a saved game file, and the parsing of strings
 * in the game file format.
 * GameController is mutable.
 * @specfield board : Board  /// the resulting board of parsing a string/file
 * @specfield nextTurn : White | Black // the color of the next player to make a move
 * @specfield timeLeft : int // amount of time left for each player, in ms
 **/


public class GameController extends java.util.Observable implements java.lang.Runnable{

  public static final String standardGameFile = "startboard.txt";

  /** This constant represents a human player **/
  public static final int HUMAN = 1;
  /** This constant represents a machine player **/
  public static final int MACHINE = 2;

  
  /** This constant assumes that are at most 2 players, and it is used for
      array declaration**/
  static final int maxPlayers = 5;

  private boolean gameInProgress = false;
  private Timer[] timer = new Timer[maxPlayers];
  private Board board;
  private int nextTurn;
  private int[] playerType = new int[maxPlayers];
  private MachinePlayer[] machinePlayer = new MachinePlayer[maxPlayers];
  private HumanPlayer humanPlayer = new HumanPlayer();
  private boolean[] isWhite = new boolean[maxPlayers];
  private int[] level = new int[maxPlayers];
  private String gameName;
  private String newMoveString, oldMoveString;  
  private int numberofPlayers;
  private List moveHistory;
  private boolean timedGame = true;
    private boolean showLegalPieces =false;
    private boolean showLegalMoves =false;
  


  /**********************************************************/
  /**********************************************************/
  // CONSTRUCTOR
  /**********************************************************/
  /**********************************************************/

  /**
   * Creates a new game from the standard file GameController.standardGameFile.
   *  Use setPlayer initialize the players and timers;
   *  Use readFromFile/readFromString to initialize the board
   *  and time and nextTurn information;
   *  Use setTime to change the time
   *  
   * @effects creates a new gameController, initialized with standard board
   **/
  public GameController (){
    this.numberofPlayers=2;
    moveHistory = new ArrayList();
    Piece.LASTCOLOR = Piece.BLACK;
    readFromFile (standardGameFile);
  }

  /**
   * Creates a game with the specified number of players
   * @requires numPlayers = 2 or 4
   * @modifies Piece.LASTCOLOR to 3 if numPlayers == 4
   **/
  public GameController (int numofPlayers){
    boolean debug = false;
    
    this.numberofPlayers=numofPlayers;
    moveHistory = new ArrayList();

    if (numofPlayers == 2){
      Piece.LASTCOLOR = Piece.BLACK;
      readFromFile(standardGameFile);
      
    } else if (numofPlayers == 4){
      Piece.LASTCOLOR = Piece.BLUE;

      // create board
      create4PlayerBoard();
      if (debug) System.out.println ("Done creating board");

      // create timer
      for (int color = Piece.FIRSTCOLOR; color <= Piece.LASTCOLOR; color ++){
	timer[color] = new Timer (300000);
      }

      // sets first player to white
      nextTurn = Piece.WHITE;
      
    } else {
      throw new RuntimeException ("WRONG NUMBER OF PLAYERS: + " + numofPlayers);
    }
  }

  /**
   * Creates a new game from the given filename.
   *  Use setPlayer initialize the players and timers;
   *  Use readFromFile/readFromString to initialize the board
   *  and time and nextTurn information;
   *  Use setTime to change the time.
   * @requires filename exists
   * @throws RuntimeException if there is an error opening the file
   **/
  public GameController (String fileName) throws RuntimeException{
    this.numberofPlayers=2;  
    Piece.LASTCOLOR = Piece.BLACK;
    moveHistory = new ArrayList();

    if (fileName == null)
      readFromFile (standardGameFile);
    else{

      // readFromFile/String will throw an exception which
      // should be handled by the calling code
      readFromFile (fileName);
    }

  }

  /**
   * A factory method that creates a new game controller from a given
   * string; this is meant to be used by the machine player.
   * @returns a new gameController initialized with boardString
   **/
  static public GameController createFromString (String boardString){

    GameController gc = new GameController();
    gc.readFromString(boardString);

    return gc;
  }

  /**
   * Creates a 4 player board
   */
  private void create4PlayerBoard(){
    boolean debug = false;

    if (debug) System.out.println ("Creating topology");
    // Create 4 player topology
    //     int topology[][] = {
    //       {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    //       {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    //       {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    //       {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0}};
    
    int topology[][] = {
      {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
      {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
      {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
      {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
      {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
      {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0}};

    board = new Board (topology);


    // Create pieces for each player
    Piece p;
    char c = 0; // All pieces of same type have same character, regardless of color
    int color;

    if (debug) System.out.println ("Creating white pieces");
    // WHITE (South)
    color = Piece.WHITE;
    // Pawns
    c = 'P';
    p = new Pawn(c, board, 3, 1, color);
    p = new Pawn(c, board, 4, 1, color);
    p = new Pawn(c, board, 5, 1, color);
    p = new Pawn(c, board, 6, 1, color);
    p = new Pawn(c, board, 7, 1, color);
    p = new Pawn(c, board, 8, 1, color);
    p = new Pawn(c, board, 9, 1, color);
    p = new Pawn(c, board, 10, 1, color);
    // Rooks
    c = 'R';
    p = new Rook(c, board, 3, 0, color);
    p = new Rook(c, board, 10, 0, color);
    // Knights
    c = 'N';
    p = new Knight(c, board, 4, 0, color);
    p = new Knight(c, board, 9, 0, color);
    // Bishops
    c = 'B';
    p = new Bishop(c, board, 5, 0, color);
    p = new Bishop(c, board, 8, 0, color);
    // Queen
    c = 'Q';
    p = new Queen(c, board, 6, 0, color);
    // King
    c = 'K';
    p = new King(c, board, 7, 0, color);
    
    if (debug) System.out.println ("Creating black pieces");
    // Black (North)
    color = Piece.BLACK;
    // Pawns
    c = 'P';
    p = new Pawn(c, board, 3, 12, color);
    p = new Pawn(c, board, 4, 12, color);
    p = new Pawn(c, board, 5, 12, color);
    p = new Pawn(c, board, 6, 12, color);
    p = new Pawn(c, board, 7, 12, color);
    p = new Pawn(c, board, 8, 12, color);
    p = new Pawn(c, board, 9, 12, color);
    p = new Pawn(c, board, 10, 12, color);
    // Rooks
    c = 'R';
    p = new Rook(c, board, 3, 13, color);
    p = new Rook(c, board, 10, 13, color);
    // Knights
    c = 'N';
    p = new Knight(c, board, 4, 13, color);
    p = new Knight(c, board, 9, 13, color);
    // Bishops
    c = 'B';
    p = new Bishop(c, board, 5, 13, color);
    p = new Bishop(c, board, 8, 13, color);
    // Queen
    c = 'Q';
    p = new Queen(c, board, 6, 13, color);
    // King
    c = 'K';
    p = new King(c, board, 7, 13, color);
    
    if (debug) System.out.println ("Creating red pieces");
    // Red (West)
    color = Piece.RED;
    // Pawns
    c = 'P';
    p = new Pawn(c, board, 1, 3, color);
    p = new Pawn(c, board, 1, 4, color);
    p = new Pawn(c, board, 1, 5, color);
    p = new Pawn(c, board, 1, 6, color);
    p = new Pawn(c, board, 1, 7, color);
    p = new Pawn(c, board, 1, 8, color);
    p = new Pawn(c, board, 1, 9, color);
    p = new Pawn(c, board, 1, 10, color);
    // Rooks
    c = 'R';
    p = new Rook(c, board, 0, 3, color);
    p = new Rook(c, board, 0, 10, color);
    // Knights
    c = 'N';
    p = new Knight(c, board, 0, 4, color);
    p = new Knight(c, board, 0, 9, color);
    // Bishops
    c = 'B';
    p = new Bishop(c, board, 0, 5, color);
    p = new Bishop(c, board, 0, 8, color);
    // Queen
    c = 'Q';
    p = new Queen(c, board, 0, 6, color);
    // King
    c = 'K';
    p = new King(c, board, 0, 7, color);
    
    if (debug) System.out.println ("Creating blue pieces");
    // blue (east)
    color = Piece.BLUE;
    // Pawns
    c = 'P';
    p = new Pawn(c, board, 12, 3, color);
    p = new Pawn(c, board, 12, 4, color);
    p = new Pawn(c, board, 12, 5, color);
    p = new Pawn(c, board, 12, 6, color);
    p = new Pawn(c, board, 12, 7, color);
    p = new Pawn(c, board, 12, 8, color);
    p = new Pawn(c, board, 12, 9, color);
    p = new Pawn(c, board, 12, 10, color);
    // Rooks
    c = 'R';
    p = new Rook(c, board, 13, 3, color);
    p = new Rook(c, board, 13, 10, color);
    // Knights
    c = 'N';
    p = new Knight(c, board, 13, 4, color);
    p = new Knight(c, board, 13, 9, color);
    // Bishops
    c = 'B';
    p = new Bishop(c, board, 13, 5, color);
    p = new Bishop(c, board, 13, 8, color);
    // Queen
    c = 'Q';
    p = new Queen(c, board, 13, 6, color);
    // King
    c = 'K';
    p = new King(c, board, 13, 7, color);

    


    // Fill up board with empty pieces
    Piece.addEmpty(board);

    // Set board type
    board.setBoardType (board.FOURPLAYERS);

    // init zobristh
    board.initZobrist();
  }
  


  /**********************************************************/
  /**********************************************************/
  // OBSERVERS
  /**********************************************************/
  /**********************************************************/

  
  /** Gets the level of the player
   * @requires color = {Piece.WHITE | Piece.BLACK}
   **/
  public int getPlayerLevel(int color){
    return level[color];
  }


  /** Gets the number of the player
   *   
   **/
  public int getNumberofPlayers(){
    return numberofPlayers;
  }


  /** Gets if the legally moved pieces should be moved
   *   
   **/
  public boolean getShowLegalPieces(){
    return showLegalPieces;
  }


  /** Gets if the legally moved pieces should be moved
   *   
   **/
  public boolean getShowLegalMoves(){
    return showLegalMoves;
  }


  /**
   * Gets the player type
   * @requires color = {Piece.WHITE | Piece.BLACK}
   * @returns { HUMAN | MACHINE}
   **/
  public int getPlayerType (int color){
    return playerType [color];
  }

  /**
   * Gets the machinePlayer object
   * @returns the MachinePlayer object, or null if color is a human player
   **/
  public MachinePlayer getMachinePlayer (int color){
    return machinePlayer[color];
  }

  /**
   * Gets the humanPlayer object.
   * @returns the humanPlayer object
   **/
  public HumanPlayer getHumanPlayer (){
    return humanPlayer;
  }

  /**
   * Gets the name of the game
   * @returns name of the game
   **/
  public String getName (){
    if (gameName==null)
      return "";
    return gameName;

  }

  /**
   * Gets the time left for a particular color
   * @param color the color of the player to get the timeleft for
   * @requires color = {Piece.WHITE | Piece.BLACK}
   * @returns amount of time left in ms
   **/
  public long getTimeLeft (int color){
    return timer[color].getTimeLeft();
  }

  /**
   * Gets the time left for a particular color
   * @param color the color of the player to get the timeleft for
   * @requires color = {Piece.WHITE | Piece.BLACK}
   * @returns amount of time left in [minutes]:[seconds] format
   * appends '0' if [seconds] is one digit
   **/
  public String getTimeLeftString(int color){

    long l;

    if (color<numberofPlayers) 
      l=timer[color].getTimeLeft();
    else l=0;

    long sec=(l%60000)/1000;
    String result=l/60000+":";
    if (sec<10)
      result+="0"+sec;
    else result+=sec;

    return result;
  }

  
  /**
   * Gets the Board representing the state of the game
   * @returns board representing state of game
   **/
  public Board getBoard (){
    return board;
  }

  /**
   * Gets the color of the nextplayer to make a move
   * @returns {Piece.WHITE | Piece.BLACK}
   **/
  public int getNextTurn (){
    return nextTurn;
  }
  
  /** Gets the type of board to use
   * @returns boardType = Board.{CYLINDER | REGULAR}
   **/
  public int getBoardType (){
    return board.getBoardType ();
  }

  /** Gets a list representing the history of movestrings made in game
   */
  public List getMoveHistory(){
    return moveHistory;
  }


  /**
   * Gets the last three moves made in the game. If number of moves
   * is less than 3, all previous moves are returned.
   */
  private List getLast3Moves (){
    boolean debug = false;
    if (moveHistory.size() <= 3){
      if (debug) System.out.println ("last 3 moves: " + moveHistory);
      
      return moveHistory;
    }
    int i;
    List l = new ArrayList();
    for (i=moveHistory.size()-3; i<moveHistory.size(); i++){
      l.add (moveHistory.get(i));
    }
    if (l.size()!=3)
      throw new RuntimeException ("getlast3moves failed: "  + l);
    if (debug) System.out.println ("last 3 moves: " + l);
    return l;
  }

  /**
   * Checks if the game is over
   */
  private boolean checkGameOver(List validMoves){
    boolean debug = false;
    if (debug) System.out.println ("Checking gameover for: " + Piece.playerColorString[nextTurn] + "\n" + validMoves);
    if (Piece.isGameOver (board, nextTurn, validMoves)){
      setChanged();
      if (Piece.isTie (board, nextTurn, validMoves)){
	notifyObservers(new GameMessage("Draw!"));
	
      } else { // Assume that it is a win
	//notifyObservers(new GameMessage((nextTurn == Piece.WHITE ? "White" : "Black") +" wins!"));
	notifyObservers(new GameMessage(Piece.playerColorString[nextTurn] +" wins!"));
	
	
      }
      return true;
    }
    return false;
  }
  /**********************************************************/
  /**********************************************************/
  //MUTATORS
  /**********************************************************/
  /**********************************************************/


  /** Sets if the legally moved pieces should be moved
   *   
   **/
  public void setShowLegalPieces(boolean val){
    showLegalPieces=val;
  }

  /** Sets if the legally moved pieces should be moved
   *   
   **/
  public void setShowLegalMoves(boolean val){
    showLegalMoves=val;
  }

  /** Enables/ disables timing for a game. This is used by TextUI to
   *  allow infinite time.
   **/
  public void setTimedGame (boolean x){
    timedGame = x;
  }
  
  /** Sets the type of board to use
   * @requires boardType = Board.{CYLINDER | REGULAR}
   **/
  public void setBoardType (int type){
    board.setBoardType (type);
  }
  /**
   *Sets gameInProgress variable
   *Should be used by UI's to indicate that game is Over!
   *@effects gameInProgress
   **/
  public void setGameInProgress (boolean value){
    this.gameInProgress=value;}

  /**
   * Makes a new player and timer with the given color, type and level.
   * @requires color = Piece.{WHITE | BLACK} && type == {HUMAN | MACHINE} &&
   * gameInProgress == false
   * @effects creates a new MachinePlayer if type == MACHINE
   **/
  public void setPlayer (int color, int type, int level){

    if (gameInProgress)
      return;
    
    this.playerType[color] = type;
    this.level[color] = level;
    if (type == MACHINE){

      // check number of players
      if (Piece.LASTCOLOR == Piece.BLACK)
	machinePlayer[color] = new MachinePlayer (color == Piece.WHITE, level, getBoardString(),false);
      else
	machinePlayer[color] = new MachinePlayer (color, level, this.getBoard());
	
    } else if (type == HUMAN){
      humanPlayer = new HumanPlayer();
    } else
      throw new RuntimeException ("GameController.setPlayer: invalid player type");
  }

  /** Sets the time left for a player
   * @requires color = {Piece.BLACK | Piece.WHITE}
   * @modifies this, this.timer[color]
   **/
  public void setTimeLeft (int color, int timeLeft){
    if (gameInProgress)
      return;
    timer[color].setTimeLeft (timeLeft);
  }

  /** Sets the time left for a player given a time string
   * such as [minutes]:[seconds], if the time string has multiple
   * ':', then the first one is used to seperate [minutes] and 
   * [seconds]
   * @requires color = {Piece.BLACK | Piece.WHITE}
   * @modifies this, this.timer[color]
   **/
  public void setTimeLeftString (int color, String timeLeft){
    if (gameInProgress)
      return;

    StringTokenizer s=new StringTokenizer(timeLeft,":",false);
    int min=0,sec=0;
    if (s.hasMoreTokens())
      min=Integer.parseInt((String)s.nextToken());
    if (s.hasMoreTokens())
      sec=Integer.parseInt((String)s.nextToken());

    timer[color].setTimeLeft (min*60000+sec*1000);
  }

  /** Sets the board to be the given board
   * @modifies this
   * @effects this.board will point to b
   **/
  private void setBoard (Board b){
    if (gameInProgress)
      return;
    board = b;
  }

  /**
   * Sets the name of the game
   * @effects name of the game
   **/
  public void setName (String name){
    if (gameInProgress)
      return ;
    this.gameName=name;

  }

  /**
   * Starts the timer for side color
   * @requires color = {Piece.BLACK | Piece.WHITE}
   * @modifies this, this.timer[color]
   **/
  public void startTimer (int color){
    timer[color].startTimer();
  }

  /**
   * Starts the timer for side color
   * @requires color = {Piece.BLACK | Piece.WHITE}
   * @modifies this, this.timer[color]
   **/
  public void stopTimer (int color){
    timer[color].stopTimer();
  }
  
  /**
   * Sets the color of the nextplayer to make a move
   * @modifies this.nextTurn
   * @requires next = {Piece.WHITE | Piece.BLACK}
   **/
  public void setNextTurn(int next){
    // if (gameInProgress)
    //return;
    nextTurn = next;
  }

  /**
   * Toggles the color of the nextplayer to make a move
   * @modifies this.nextTurn
   **/
  public void toggleTurn(){
    setNextTurn(Piece.getOpponentColor(getNextTurn()));
  }

  /** Starts the game and sets gameInProgress to true; NOT FULLY IMPLEMENTED;
   * see code for more details
   *
   **/
  public void run(){
    this.startGame();}


  /** Starts the game and sets gameInProgress to true; NOT FULLY IMPLEMENTED;
   * see code for more details
   *
   **/
  public void startGame (){
    boolean debug = false;

    gameInProgress = true;
    
    Move m;
    newMoveString= null;
    oldMoveString= null;

    List validMoves = null;
    if (debug) System.out.println ("gc getvalidmoves 1");
    validMoves = Piece.getAllValidMoves (board, nextTurn);

    // check if the game is already over
    if (checkGameOver(validMoves)){
      if (debug) System.out.println ("check game over valid MOves: " +  validMoves);
      return;
    }
    
    // We only need to do this for human player in textUI if a human
    // player starts a game
    // This should not have any effect on GUI 
    if (playerType[nextTurn] == HUMAN){
      humanPlayer.setMoveList (validMoves);
    } else {
      humanPlayer.setMoveList (null);
    }
    
    // Start timer
    if (timedGame)
      timer[nextTurn].startTimer();

    
    while(gameInProgress){
      if (debug) System.out.println ("Valid moves: " + validMoves);
      

      //GET RID OF THIS LATER
      //for(long i=0;i<50000000;i++); 
      //

      if (debug) System.out.println ("Getting moves from players");

      // Get the valid moves for the current player
      if (playerType[nextTurn] == MACHINE){

	if (debug) System.out.println ("Getting move from machine player");

	//starting a thread which listens to the machine player
	newMoveString=null; //make the move null first
	if (numberofPlayers==4){
	  // get the last 3 moves and give to the next player
	  newMoveString = machinePlayer[nextTurn].makeMove (getLast3Moves());
	} else {
	  (new Thread() { 
	      public void run(){
		newMoveString = machinePlayer[nextTurn].makeMove (oldMoveString, (int)timer[nextTurn].getTimeLeft(),
								  (int)timer[Piece.getOpponentColor(nextTurn)].getTimeLeft());}}).start();}

	
	while (newMoveString==null && timer[nextTurn].getTimeLeft()>0 && gameInProgress){
	}

      } else if (playerType[nextTurn] == HUMAN){

	// get a string input s from GUI
	Move humanMove = null;

	if (debug) System.out.println ("Getting move from human player");
	if (debug) System.out.println ("GetMyTurn: " + humanPlayer.getMyTurn());
	
	while (humanMove == null && timer[nextTurn].getTimeLeft()>0 && gameInProgress){
	  humanMove = humanPlayer.getMove();
	}
	if (debug) System.out.println ("Gotten move from human: " + humanMove);
	if (humanMove!=null)
	  newMoveString = humanMove.toString();
	
      }

      // stop timer
      if (timedGame)
	timer[nextTurn].stopTimer();

      //*****************************************
      // Check for time constraints
      // and notify users if they are violated
      if (timer[nextTurn].getTimeLeft()<=0)
	{//Time is up, game ended
	     
	  if (debug) System.out.println ("Time has ended:Notifying observers");
      
	  //notify observers that there has been a move by the last player
	  setChanged();
	  //notifyObservers(new GameMessage((Piece.WHITE==nextTurn ? "Black":"White")+" wins: Time run out!"));
	  notifyObservers(new GameMessage(Piece.playerColorString[Piece.getOpponentColor (nextTurn)]
					  +" wins: " +
					  Piece.playerColorString[nextTurn] + " run out of time!"));
	  //now return from GameController
	  return;
	}
      //******************************************



      //*******************************************
      //*******************************************
      //*******************************************
      //*******************************************


      if (gameInProgress)
      

	  {


      //make oldMoveString to point to the old move
      oldMoveString=newMoveString;
      
      // create move and update the board
      m = new Move(newMoveString);

      if (debug) System.out.println ("gc:\n"+board);

      if (debug) System.out.println ("validmoves: \n" + validMoves);
      System.out.println (Piece.playerColorString [nextTurn] + " Move: " + m + "(string: " + newMoveString + ")");

      // update the board
      Piece.makeMove(board, m);
      moveHistory.add(m.toString());

      // check that machine player's board is updated
     
       if (playerType[nextTurn] == MACHINE){
	if (!machinePlayer[nextTurn].getBoard().toString().equals(board.toString())){
	  System.out.println ("Machine board:\n" + machinePlayer[nextTurn].getBoard());
	  System.out.println ("GC board:\n" + board);
	  throw new RuntimeException ("Machine board for " + Piece.playerColorString[nextTurn] + " not updated correctly");
	}
								  
      }
      
      if (debug) System.out.println ("gc2:\n"+board);



      // previous turn
      int previousTurn = nextTurn;
      
      // switch player
      toggleTurn();
      
      // Update the list of validMoves before notifying
      // This is done first so that humanPlayer will always be able
      // to access the movelist. getting valid moves might take too long,
      // so it has to be done first before notifying

      if (debug) System.out.println ("gc getvalidmoves 1");
      validMoves = Piece.getAllValidMoves(board, nextTurn);      
      if (playerType[nextTurn] == HUMAN){
	humanPlayer.setMoveList (validMoves);
      } else {
	humanPlayer.setMoveList (null);
      }

      if (debug) System.out.println ("gc3:\n"+board);

      if (debug) System.out.println ("Notifying observers");
      
      if (checkGameOver(validMoves)) {
	if (debug) System.out.println ("check game over valid MOves: " +  validMoves);
	return;
      }
      
      // Start timer for next player
      if (timedGame)
	timer[nextTurn].startTimer();
    
      //notify observers that there has been a move by the last player
      setChanged();
      notifyObservers(new GameMessage(m, previousTurn));
	  }}

  }

  

  /**********************************************************/
  // String and File Parsing
  /**********************************************************/
  
  /** Get the string representation of the current game, as specified in the game file format
   * @returns string representation of game
   **/
  public String getBoardString (){

    String str = new String("");

    // LINE 0: Cylindrical Board
    if (board.getBoardType() == board.CYLINDER){
      str += "Cylinder Board\n";
    }

    // LINE 1: Next player

    String nextPlayerString = (nextTurn == Piece.WHITE ? "white" : "black") + "\n";
    str += nextPlayerString;

    // LINE 2: Time left
    String timeLeftString = timer[Piece.WHITE].getTimeLeft() + "\t";
    timeLeftString += timer[Piece.BLACK].getTimeLeft() + "\n";

    str +=  timeLeftString;

    // LINE 3: Switch Info
    String switchString = (board.getSpecial (Piece.WHITE) ? "1" : "0") + "\t";
    switchString += (board.getSpecial (Piece.BLACK) ? "1" : "0") + "";
    str += switchString;

    // LINE 4 ONWARDS: Piece Info
    String pieceStr = new String("");

    //Loop through all pieces
    String colLetters = "abcdefgh";
    List pieceStrList = new ArrayList();
    for (int color=Piece.FIRSTCOLOR;color<=Piece.LASTCOLOR;color++){
      //System.out.println ("Last piece: ");
      //System.out.println (board.getLastPieceMoved (color));

      Piece pieces[][] = board.getPiecesOnBoard(color);

      for (int i=Piece.FIRSTPIECE; i <= Piece.LASTPIECE; i++){
	int pieceCount = board.getPieceCount (color, i);
	for (int j=0; j<pieceCount; j++){
	  Piece piece = pieces[i][j];
	  pieceStr = new String("");
	  if (piece == null) continue;
	  // Line break
	  pieceStr += "\n";
	  // Get char
	  pieceStr += piece.getChar() + "\t";
	  // Get cur position
	  pieceStr += colLetters.charAt (piece.getCol()) + "" + (piece.getRow()+1) + "\t";
	  // Get last position
	  pieceStr += colLetters.charAt (piece.getLastCol()) + "" +  (piece.getLastRow()+1) + "\t";
	  // Check if moved
	  pieceStr += (piece.getMoved() ? "1" : "0") + "\t";
	  // Check Last Piece Move
	  //pieceStr += (piece.equals (board.getLastPieceMoved(piece.getColor())) ? "1" : "0");
	  pieceStr += (board.isLastPieceMoved(piece) ? "1":"0");
	  pieceStrList.add (pieceStr);
	}
      }
    }

    // Sort the strings
    Collections.sort (pieceStrList);
    for (int i=0; i<pieceStrList.size(); i++){
      pieceStr = (String) pieceStrList.get(i);
      str += pieceStr;
    }

    str +=  ";";// terminate with ";" and NO END LINE

    return str;

  }
  /**
   * Writes the game file to the specified filename, in game file format.
   * @param fileName full path of file to write to
   **/
  public void saveGameToFile (String fileName){
    try {
      PrintStream output = new PrintStream (new FileOutputStream(fileName));
      output.println (getBoardString());
    } catch (Exception e){
      throw new RuntimeException ("ERROR WRITING TO " + fileName + "\n" + e);
    }
  }
  /** Fills the GameFileManager with information from a file
   * @param fileName the full path of the file containing the game information
   * @requires file is in the gamefile format
   * @modifies this
   * @effects sets GameFileManager with information extracted
   **/
  private void readFromFile(String fileName){
    try {
      readFromString (fileRead (fileName));
    } catch (Exception e){
      throw new RuntimeException ("Error reading from file: " + fileName + ":\n" + e);
    }
  }


  /** Fills the GameFileManager with information from a game string
   * @param gameStr the string containing the game information
   * @requires gameStr is in the gamefile format
   * @modifies this
   * @effects sets GameFileManager with information extracted
   **/
  private void readFromString(String gameStr){
    StringTokenizer st = new StringTokenizer (gameStr, "\n");
    String str;
    boolean outputToScreen = false;
    int[][] topology = { {1,1,1,1,1,1,1,1},
			 {1,1,1,1,1,1,1,1},
			 {1,1,1,1,1,1,1,1},
			 {1,1,1,1,1,1,1,1},
			 {1,1,1,1,1,1,1,1},
			 {1,1,1,1,1,1,1,1},
			 {1,1,1,1,1,1,1,1},
			 {1,1,1,1,1,1,1,1}};
    // Overwrites board with a new board
    board = new Board(topology);

    // LINE 0 (optional)
    // Cylinder board
    str = (String) st.nextElement();
    if (str.equals("Cylinder Board")){
      board.setBoardType(board.CYLINDER);
      str = (String) st.nextElement();
    }
    
    // LINE 1
    // Get next player 

    //System.out.println (str + ".");
    if (str.equals ("white")){
      nextTurn = Piece.WHITE;
      if (outputToScreen) System.out.println ("white");
    }
    else if (str.equals ("black")){
      nextTurn = Piece.BLACK;
      if (outputToScreen) System.out.println ("black");
    }
    else
      throw new RuntimeException ("Error getting next player color");


    // LINE 2
    // Get Time Left for each player
    try {
      str = (String) st.nextElement();
      StringTokenizer timeSt = new StringTokenizer (str, "\t");
      String whiteTimeStr = (String) timeSt.nextElement();
      if (!timeSt.hasMoreElements())
	throw new RuntimeException ("Error in input file format for time. missing tab");
      String blackTimeStr = (String) timeSt.nextElement();
      timer[Piece.WHITE] = new Timer(Integer.valueOf (whiteTimeStr).intValue());
      timer[Piece.BLACK] = new Timer (Integer.valueOf (blackTimeStr).intValue());
      if (outputToScreen) System.out.println ("white time: " +
					      timer[Piece.WHITE].getTimeLeft());
      if (outputToScreen) System.out.println ("black time: " +
					      timer[Piece.BLACK].getTimeLeft());
    } catch (Exception e){
      throw new RuntimeException ("Error getting timeleft\n" + e);
    }


    // LINE 3
    // Get switch info
    str = (String) st.nextElement();
    if (str.charAt(0) == '1'){
      board.setSpecial (Piece.WHITE, true);
      if (outputToScreen) System.out.println ("white used special: " +
					      board.getSpecial(Piece.WHITE));
    }
    if (str.charAt(2) == '1'){
      board.setSpecial (Piece.BLACK, true);
      if (outputToScreen) System.out.println ("black used special: " +
					      board.getSpecial(Piece.BLACK));
    }


    // LINE 4 UNTIL LINE ENDING with ";"
    // Get piece info
    int row, col;
    while (st.hasMoreElements()){
      str = (String) st.nextElement();

      // Get current pos
      // current col a-h
      col = str.charAt(2) - 'a';// + 1;
      // current row '1'-'8'
      row = str.charAt(3) - '1'; //- '0';

     
      // Get Piece Type
      char pUp = str.toUpperCase().charAt(0);
      char p = str.charAt(0);
      Piece piece;

      int color;
      // Get Color
      if (p == pUp) // White are upper case
	color = Piece.WHITE;
      else 
	color = Piece.BLACK;

      if (outputToScreen)
	System.out.println ("creating..");
      if (pUp == 'P'){
	piece = new Pawn(p, board, col, row, color);
      } else if (pUp == 'K'){
	piece = new King(p, board, col, row, color);
      } else if (pUp == 'N'){
	piece = new Knight(p, board, col, row, color);
      } else if (pUp == 'R'){
	piece = new Rook(p, board, col, row, color);
      } else if (pUp == 'Q'){
	piece = new Queen(p, board, col, row, color);
      } else if (pUp == 'B'){
	piece = new Bishop(p, board, col, row, color);
      } else
	throw new RuntimeException ("Unrecognized piece type:" + pUp);

      // Get last pos
      // last col a-h
      int lastCol = str.charAt(5) - 'a';// + 1;
      // last row '1'-'8'
      int lastRow = str.charAt(6) - '1';//'0';
      piece.setLastLocation (lastCol, lastRow);



      // Get has-moved
      char moved = str.charAt(8);
      if (moved == '1')
	piece.setMoved (true);

      // Get last-piece-moved
      moved = str.charAt(10);
      if (moved == '1'){
	//**!!!***************************
	//SET LAST PIECE MOVE IN BOARD

	if (board.getLastPieceMoved (piece.getColor()) == null){
	  board.setLastPieceMoved (piece);
	}
	else{
	  board.setLastPieceMoved (piece,1);
	}
      }
      
      if (outputToScreen){
	System.out.println (piece);
	System.out.println ("islastpiece: " + moved);
      }
			
      // End of piece info
      if (str.charAt(str.length()-1) == ';'){
	break;
      }
    }


    // fill up board with empty pieces
    Piece.addEmpty(board);

    // init Zobrist
    board.initZobrist();
    
  }

  /** Reads a file's contents into a string
   *takes String  
   **/
  private String fileRead(String filename) {
    if (filename == null)
      throw new RuntimeException("No file specified");

    String answer = new String();
    try {

      BufferedReader in;

      if (filename.equals(standardGameFile))
	in = new BufferedReader(new InputStreamReader(GameController.class.getResourceAsStream(filename)));
      else 
	in= new BufferedReader(new FileReader(filename));

      // read each line until the end of the file and parse it into the script
      for (String line = in.readLine(); line != null; line = in.readLine()) {
	answer += line + "\n";

      }
    }
    catch (Exception e) {

      e.printStackTrace();

      //throw new RuntimeException("File not accessible\n" + filename);

    }
    
    //System.out.println ("File read:\n" + answer);
    return answer;
  }
  

}



