/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;
import java.util.*;
/**
 *
 *<p> HumanPlayer is the human player for AChess Program.
 *It handles the communication between a human and a GameController.
 *
 *<p> When it is a human player's turn, GameController should first call
 * setMoveList() with possible moves. Then, it should wait for getMove()
 * to return a non-null move. When there is a moveMade, UI should first
 * call getMyTurn() and if it returns true, it should call getMoveList()
 * to get a list of possible moves. If the moveMade is among this, UI
 * should call setMove() with moveMade.
  *
 *<p>It is necessarily true that when <code>myTurn</code> is true,
 * <code>moveList</code> is non-null, however it can be empty. It is
 * also necessarily true that when a getMove() returns non-null, subsequent
 * calls to getMyTurn() will return false.
 *
 * @specfield moveList list of moves
 * @specfield move a selected move from the moveList
 * @specfield myTurn true if it is HumanPlayers turn
 **/

public class HumanPlayer{

  //FIELDS
  private List moveList;
  private Move move;
  private boolean myTurn;

  //CONSTRUCTOR
  public HumanPlayer(){
    moveList=null;
    move=null;
    myTurn=false;}

  //OBSERVERS
  
  /**Returns whether it is human players turn.
   *Should be called by UI.
   *@returns myTurn
   */
      public boolean getMyTurn(){
      return myTurn;}
  

  /**Returns move, if move is not null (ie a move is selected by UI)
   *this method sets the myTurn false.
   *Should be called by GameController.
   *@returns move
   *@effects if (move!=null) {myTurn=false; move=null}
  */
  synchronized public Move getMove(){
	// Make a copy of move first because
	// setMyTurn will set it to null
	Move m = move;
	
	if (move!=null)
	  setMyTurn(false);
	return m;}

  /**Returns moveList
   *Should be called by UI.
   *@returns moveList
   */
 synchronized public List getMoveList(){
      return moveList;}


  //PRODUCERS

  /**Sets myTurn=turn and move=null. We need to
   * have move set to null, so that when a move
   * is made, GameController will know it by comparing
   * getMove() to null.
   *@effects myTurn=turn && move=null 
   */
      private void setMyTurn(boolean turn){
	move=null;
	myTurn=turn;
      }
  

  /**Sets move, if it is myTurn. Should be
   * called by UI.
   *@requires myTurn==true, move isElementof moveList
   *@effects move
   */
  synchronized public void setMove(Move move){
	if (myTurn)
	this.move=move;}

  /**Sets moveList. Should be called by GameController.
   *@effects moveList && (if moveList!=null then myTurn=true)
   * && (if moveList==null then myTurn=false)
   */
  synchronized public void setMoveList(List moveList){
      this.moveList=moveList;
      if (this.moveList==null)
	setMyTurn(false);
      else setMyTurn(true);
      }

}
