/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;
import java.util.*;
import java.io.*;
import java.lang.*;

/**
 *
 *<p> MachinePlayer is the machine player for AChess Program.
 *
 *
 **/

public class MachinePlayer{

  //FIELDS
  private long TIME_LIMIT = 5000;
  private long startTime=0;
  private long halfTime=0;
  private Board board;
  private int color;
  private int opponentColor;
  private int level;
  private GameController gc;
  private boolean firstMoveMade;  
  private java.util.Map boardTable;
  private Move assumedOpponentMove = null;
  private String assumedOpponentMoveString = null;
  private BetterEngine ponderEngine = null;
  private BetterEngine engine = null;
  private volatile Thread ponderThread = null;
  private boolean toPonder = false;


  /** **REQUIRED BY 6.170 Antichess Specification** 
   *@effects Creates a new MachinePlayer with the normal starting 
   *board (as described in Figure 5). If isWhite is 
   *true, the player plays as white, else as black. Level 
   *is an optional, arbitrary integer which can be used to 
   *affect the algorithm the machine player uses. The 
   *tournament referee will always request a level 0 
   *MachinePlayer.
   */
  public MachinePlayer(boolean isWhite, int level, boolean cylinder){

    // ** NOT FULLY IMPLEMENTED
    gc = new GameController();
    board = new Board(gc.getBoard());
    this.color = (isWhite ? Piece.WHITE : Piece.BLACK);
    this.opponentColor = Piece.getOpponentColor(color);
    //this.startTime = gc.getTimeLeft (color);
    //this.halfTime = startTime / 2;
    this.level = level;
    this.firstMoveMade=false;
    this.boardTable=new java.util.Hashtable();
    if (cylinder)
	this.board.setBoardType(Board.CYLINDER);
    else this.board.setBoardType(Board.REGULAR);
    this.engine = new BetterEngine(board, boardTable,
				   ((level == 0) ? 22 : level),
				   color);
    loadOpenBook();
  }
  
  /**  **REQUIRED BY 6.170 Antichess Specification** 
   * @requires bd is a string representing a valid board, as 
   *  described in Figure 5.
   * @effects
   * Creates a new MachinePlayer in the same way described 
   * by the constructor MachinePlayer(boolean, int), except 
   * that the initial board configuration is set to be the one 
   * described by bd.
   */
  public MachinePlayer(boolean isWhite, int level, String bd,boolean cylinder){
    // ** NOT FULLY IMPLEMENTED

    //Read and find out about the board
    gc=GameController.createFromString (bd);
    board=new Board(gc.getBoard());
    if (cylinder)
	this.board.setBoardType(Board.CYLINDER);
    else this.board.setBoardType(Board.REGULAR);
    this.startTime = gc.getTimeLeft (color);
    this.halfTime = startTime / 2;
    this.level = level;
    this.color = (isWhite ? Piece.WHITE : Piece.BLACK);
    this.opponentColor = Piece.getOpponentColor(color);    
    this.firstMoveMade=false;
    this.boardTable=new java.util.Hashtable(); 
    this.engine = new BetterEngine(board, boardTable,
				   ((level == 0) ? 22 : level),
				   color);
    loadOpenBook();
  }

  /** Constructs a machineplayer from a given board. For 4 players**/
  public MachinePlayer(int color, int level, Board bd){

    board=new Board(bd);
    this.level = level;
    this.color = color;
    this.opponentColor = Piece.getOpponentColor(color);    
    this.firstMoveMade=false;
    this.boardTable=new java.util.Hashtable(); 
    //    loadOpenBook();
  }
  
  /** **REQUIRED BY 6.170 Antichess Specification**
   * @effects Returns a string name for the machine player (this is 
   * needed for the tournament.)
   */
  public  String getName(){
    return("Ionesco is an absurdist writer,");
  }
  /**  **REQUIRED BY 6.170 Antichess Specification** 
   * @requires opponentMove is either a string representing a valid 
   *   move by the opponent on the board stored in this (see 
   *   Figure 4), or the empty string. timeLeft and 
   *   opponentTimeLeft are both  > 0
   *@effects returns a valid next move for this player in a string 
   *  of the appropriate format, given the opponent's move, 
   *  the time left for this player, and the time left for the 
   *  opponent. Both times are in milliseconds. If 
   *  opponentMove is the empty string, then the board for this 
   *  player should be considered up to date (as would be the 
   *  case if this player is asked to make the first move of the 
   *  game). 
   *
   *NOTE: This procedure may run greater than timeLeft, but 
   *this would mean losing the game.
   *@modifies this
   */
  public String makeMove(String opponentMove, int timeLeft, int opponentTimeLeft){
    boolean debug = false;
    List validMoves;

    if (opponentMove!=null) {
      Piece.makeMove(board,new Move(opponentMove));
      //if (debug) System.out.println ("Machine player made opponent move: " + opponentMove +"\n" + board);      
    }

    if (this.startTime==0)
	{
	this.startTime = timeLeft;
	this.halfTime = startTime / 2;
        }

    Move m=null;
    System.out.println("timeleft"+timeLeft);
    if (timeLeft > (1.9*halfTime))
	{ System.out.println("1.ceyrek");
	TIME_LIMIT = startTime / 60;
      }
    else
   if (timeLeft<(1.9*halfTime) && (timeLeft>(0.5*halfTime)))
      {System.out.println("2.ceyrek");
	  TIME_LIMIT = startTime / 20;}
  else  
 if ((timeLeft > (halfTime/6)) && (timeLeft<(0.5*halfTime)))
      {System.out.println("3ceyrek");
	TIME_LIMIT = timeLeft / 25;
      }
    else
      { System.out.println("4.ceyrek");
	  TIME_LIMIT = timeLeft / 100;
      }

//     /**** CHECK TO SEE RESULT OF PREVIOUS PONDERING ****/
//     // Check to see if we have been pondering
//     if (ponderEngine != null){
      
//       // Check our previous guess for the assumed move was correct
//       if (assumedOpponentMoveString!=null &&
// 	  opponentMove.equals(assumedOpponentMoveString)){
// 	if (debug) System.out.println ("Previous assumed move was correct");


// 	// If the pondering is already completed, get the result

// 	if (ponderEngine.searchFinished){
// 	  if (debug) System.out.println ("ponderEngine already completed");
// 	} else {
// 	  if (debug) System.out.println ("ponderEngine not complete, waiting for it to complete");
// 	  // complete the pondering
// 	  long threadStartTime = System.currentTimeMillis();

// 	  for (;!ponderEngine.searchFinished && System.currentTimeMillis() - threadStartTime <= TIME_LIMIT;){
// 	    try {
// 	      Thread.sleep (200L);
// 	    } catch (Exception e){}
// 	    if (System.currentTimeMillis() - threadStartTime >= TIME_LIMIT){
// 	      System.out.println ("search out time passed: " + (System.currentTimeMillis() - threadStartTime));
// 	      ponderEngine.searchInProgress = false;

// 	      // Wait for thread to finish
// 	      while (!ponderEngine.searchFinished){
// 		try {
// 		  Thread.sleep (200L);
// 		} catch (Exception e){}
// 	      }
	      
// 	      break;
// 	    }
// 	  }
// 	}

// 	// retrieve the result of pondering
// 	ponderEngine.searchInProgress = false;
// 	m = ponderEngine.foundMove;
	
//       } else {
// 	// The assumed move is wrong, so stop the pondering right away
// 	// stop pondering
// 	ponderEngine.searchInProgress = false;
//       }
//     }
    
    /**** DO OUR OWN SEARCH HERE IF NECESSARY ****/
    // check to see if we need to do our own search
    if (m  == null){
    //if (true){

      // Do our own search here
    

      if (level==11){
	//Choose random player
      	m=NewEngine.randomMove(board,color);
	if (debug) System.out.println ("NewEngine.randomMove made moves: " + m);
	return m.toString(); // we do not ponder when playing randomly
      }else {

	// Start a new thread and terminate it if we run out of time

	System.out.println ("starting thread with TIME_LIMIT: " + TIME_LIMIT);
	engine.searchInProgress =true;
	engine.searchFinished = false;
	Thread thread = new Thread (engine);
	thread.start();
	//System.out.println ("engine.searchFinished: " + engine.searchFinished);
	long threadStartTime = System.currentTimeMillis();
	
	//engine.run();

	while (!engine.searchFinished){
	  //System.out.println ("time passed: " + (System.currentTimeMillis() - threadStartTime));
	  try {
	    Thread.sleep (200L);
	  } catch (Exception e){}
	  
 	  if (System.currentTimeMillis() - threadStartTime >= TIME_LIMIT){
 	    System.out.println ("search out time passed: " + (System.currentTimeMillis() - threadStartTime));
 	    engine.searchInProgress = false;
	    // Wait for thread to finish
	    while (!engine.searchFinished){
	      try {
		Thread.sleep (200L);
	      } catch (Exception e){}
	    }
 	    break;
 	  }
	}
	//System.out.println ("search out time passed: " + (System.currentTimeMillis() - threadStartTime));

	m = engine.foundMove;
      }

    }
    
    // Apply the move
    if (m == null) {
      System.out.println ("bestMove should not be null");

      // REMOVE THIS OR SET DEBUG TO FALSE BEFORE SUBMITTING
      if (debug)
	throw new RuntimeException ("move returned was null");
      System.out.println (board);
      validMoves = Piece.getValidMoves (board, color);
      m = (Move)validMoves.get(0);
      Piece.makeGeneratedMove (board, m);
      //System.out.println ("applied move: " + m + "\n" + board);
      return (m.toString());
    }
    validMoves = Piece.getValidMoves (board, color);
    // check if the move is valid

    if (!validMoves.contains (m)){
      System.out.println ("move returned by BetterEngine is invalid: " + m);
      // REMOVE THIS OR SET DEBUG TO FALSE BEFORE SUBMITTING
      if (debug)
	throw new RuntimeException ("move returned was invalid: "+m);
      //System.out.println (board);
      m = (Move)validMoves.get(0);
      Piece.makeGeneratedMove (board, m);
      //System.out.println ("applied move: " + m + "\n" + board);
      try{
	Thread.sleep (2000L);
      } catch (Exception e){}
      return (m.toString());

    }
    //System.out.println ("Board Before: \n" + board);
    //System.out.println ("Applying move: " + m);
    Piece.makeGeneratedMove (board, m);
    //System.out.println ("Board After: \n" + board);
    //     long zb = board.zobristCode (opponentColor);
    //     HashKey hashKey = new HashKey (zb);
    
    /**** START A NEW PONDERING THREAD ****/
//     ponderEngine = null;
//     if (toPonder){

//       // TO REMOVE THIS
//       // get the assumed opponent move from the hash table
//       //       System.out.println ("searching for this hashkey in boardtable: " + zb);
//       //       HashEntry hashEntry = (HashEntry) boardTable.get (hashKey);;
//       //       System.out.println ("hashentry: " + hashEntry);

//       assumedOpponentMove = engine.secondMove;
//       System.out.println ("Second move : " + assumedOpponentMove);
      
//       // check if we managed to find the assumed move

      
//       //if (hashEntry == null) {
      
//       if (assumedOpponentMove == null){
// 	assumedOpponentMove = null;
// 	assumedOpponentMoveString = null;
// 	//System.out.println ("Warning: move not found in hashtable for board:\n" + board);
// 	System.out.println ("Warning: secondMove is null");
	
// 	//} else if (hashEntry.getBestMove() != null){
//       } else {
	
// 	// if so we apply 
// 	//assumedOpponentMove = hashEntry.getBestMove();
	
// 	assumedOpponentMoveString = assumedOpponentMove.toString();

// 	// check if the move is valid
// 	validMoves = Piece.getValidMoves (board, opponentColor);

// 	if (validMoves.contains(assumedOpponentMove)){
// 	  // start pondering before we return
	
// 	  // make a copy of the board
// 	  Board ponderBoard = new Board(board);
	  
// 	  // apply the assumed opponent move
// 	  System.out.println ("applying second move: " + assumedOpponentMove);
// 	  Piece.makeGeneratedMove (ponderBoard, assumedOpponentMove);
	  
	  
// 	  // start pondering and return
// 	  // Give the ponderEngine the board, boardTable, depth and color
// 	  // to work on
// 	  System.out.println ("This board goes to ponderEngine\n" + ponderBoard);
// 	  ponderEngine = new BetterEngine(ponderBoard, boardTable,
// 					  level, opponentColor);
	  
// 	  if (debug) System.out.println ("Starting new thread");
// 	  ponderThread = new Thread (ponderEngine);
// 	  ponderThread.start();
// 	} else {
// 	  System.out.println ("secondmove is not valid: " + assumedOpponentMove);
// 	}
//       }
//     }
    
    return m.toString();

  
  }

  /** This is used in four player chess to update every machine player what
   *  some other player did, and to get the next move.
   * @param lastMoves a List of movestrings made by the previous players
   * @return the next move this player makes
   **/

  public String makeMove (List lastMoves){
    boolean debug = false;
    for (int i=0; i<lastMoves.size(); i++){
      String opponentMove = (String) lastMoves.get(i);
      if (opponentMove!=null) {
	Piece.makeMove(board,new Move(opponentMove));
	if (debug) System.out.println ("Machine player made opponent move: " + opponentMove +"\n" + board);
      }
    }
    Move m = NewEngine.randomMove(board,color);
    Piece.makeMove(board, m);
    return m.toString();
    
  }

  /* Gets the board */
  public Board getBoard(){
    return board;
  }
    
    /**For two player game, if there exists an openning book file for the board style,
     *initializes <code>boardTable</code> with this opening book
     *@requires gc!=null
     *@modifies boardTable
     */
    private void loadOpenBook(){

	//if this is not a 2-player game return
	if (gc.getNumberofPlayers()!=2)
	    return;
	
	//if boardTAble is not null
	if (boardTable==null)
	    return;

	//load the necessary file 
	String answer="";

	if (gc.getBoardType()==Board.REGULAR) //for regular board
	    {
		
	    try {
	    BufferedReader in = new BufferedReader(new 
						   InputStreamReader(GameController.class.getResourceAsStream("regular.txt")));
	    // read each line until the end of the file and parse it into the script
	    for (String line = in.readLine(); line != null; line = in.readLine()) 
		answer += line + "\n";
      
	    }
	    catch (Exception e) {
		System.out.println("No regular.txt in where GameConroller.class is");
		//e.printStackTrace();
		return;
	        }
	    
	    }
	else
	    { //for cylinderical board
	    try {
	    BufferedReader in = new BufferedReader(new 
						   InputStreamReader(GameController.class.getResourceAsStream("cylinder.txt")));
	    // read each line until the end of the file and parse it into the script
	    for (String line = in.readLine(); line != null; line = in.readLine()) 
		answer += line + "\n";
      
	    }
	    catch (Exception e) {
		System.out.println("No cylinder.txt in where GameConroller.class is");
		//		e.printStackTrace();
		return;
	        }
	    }

	//now, answer has all the opening book entries
	
      StringTokenizer fileLine = new StringTokenizer (answer, "\n");

      //following values are read from the file
      //and they are necessary to create a HashEntry
      int nextTurn; //next player color to move
      long key;     //key of the Entry
      Move bestMove; //move of hte Entry
      int type;     //type of the Entry      
      int val;      //value of the Entry
      int depth;    //depth of the Entry
      String token; //token we are talking about
      
      HashEntry entry; //specific hashEntry
      


      while(fileLine.hasMoreTokens())
	  //parse each line accordingly
  	  {
	      //take nextTurn
	      token=fileLine.nextToken();

	      if (token.equals("white"))
		  nextTurn=Piece.WHITE;
	      else{ 
		  if (token.equals("black"))
		  nextTurn=Piece.BLACK;
		  else
		      {
			  System.out.println("Error in getting the color");
			  break;
		      }
	          }


	      //take bestMove
	      bestMove=new Move(fileLine.nextToken());

	      //take type
      	      token=fileLine.nextToken();


	      if (token.equals("exact"))
		  type=HashEntry.HASHEXACT;
	      else
		  if (token.equals("alpha"))
		  type=HashEntry.HASHALPHA;
		  else
		      if (token.equals("beta"))
			  type=HashEntry.HASHBETA;
		      else
		      {
			  System.out.println("Error in getting the type");
			  break;
		      }

	      //take val
      	      token=fileLine.nextToken();
	      val=Integer.parseInt(token);
	      
	      //take depth
      	      token=fileLine.nextToken();
	      depth=Integer.parseInt(token);

	      //create the key
	      //read the board string in 
	      token="";
	      for(int i=board.MINROW;i<=board.MAXROW;i++)
		  token+=fileLine.nextToken();
	      key=board.zobristCode(token, nextTurn);
	      
	      //create the hashEntry and put it into the table
	      entry=new HashEntry(key,bestMove,type,val,depth);
	      boardTable.put(new HashKey(key),entry);
	      
	      //System.out.println("Added entry:"+entry);

	  }
      //System.out.println("End of opening file entry");
    }
}
