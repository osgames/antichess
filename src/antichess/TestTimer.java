/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;
import junit.framework.*;
import java.io.*;
import java.util.*;

public class TestTimer
  extends TestCase{
  public TestTimer(String name) {
    super(name);
  }

  protected void setUp(){
  }
	void delay (long num_milliseconds, Timer timer)
	{
		long the_time = System.currentTimeMillis();
		while (( System.currentTimeMillis()- the_time) < num_milliseconds){
			assertTrue ("timer running", timer.getIsTiming());
		};
	}


	public void testTimer(){
		long tolerance = 200;
		Timer timer = new Timer (5000);
		timer.startTimer();
		assertTrue ("timer running", timer.getIsTiming());
		delay (1000, timer);
		timer.stopTimer();
		//System.out.println ("Timeleft "  + timer.getTimeLeft());
		assertEquals ("timer", 4000, timer.getTimeLeft(), tolerance);
		assertTrue ("timer not running", !timer.getIsTiming());

		//mutate
		timer.setTimeLeft( 20000);
		timer.startTimer();
		delay (1000, timer);
		timer.stopTimer();
		assertEquals ("timer", 19000, timer.getTimeLeft(), tolerance);
		assertTrue ("timer not running", !timer.getIsTiming());
	}


  // Tell JUnit what order to run the tests in
  public static Test suite()
  { 
    TestSuite suite = new TestSuite(); 
    return suite;
  }
}
