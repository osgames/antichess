/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import java.util.*;


public class Evaluator {

  /**
   * This class  assigns a numerical value to a board position for a particular player.
   * A positive score for a given player implies that the board is preferable for him to reach.
   * Similarly, a negative  score implies that the board is undesired.
   * A score of 0 suggests that the board does not give an advantage or disadvantage for that specific player.
   * Evaluator is immutable.
   */

  /*
   * @requires board.getColors().contain(colorToMove) && 0<=evalQuality<=2 && board is a valid board position according to rules of antichess
   * @modifies nothing
   * @ returns an integer value that assesses the board for the particular player.
   * More positive values are better for the player.
   */
  private static final int WHITE=0;
  private static final int BLACK=1;
  private static  final int PAWN=0;
  private static  final int ROOK=1;
  private static  final int KNIGHT=2;
  private static  final int BISHOP=3;
  private static  final int QUEEN=4;
  private static final int KING=5;
  private static final int SPECIAL_MOVE_BONUS=50;
  private static final int ISOLATED_PAWN_PENALTY=30;
  private static final int DOUBLED_PAWN_PENALTY=20;
  private static final int ROOK_OPEN_FILE_PENALTY=5;
  private static final int ROOK_SEMI_OPEN_FILE_PENALTY=5;
  private static final int ROOK_ON_SEVENTH_PENALTY=5;
  private static final int PASSED_PAWN_PENALTY=25;
  private static final int BLOCKED_ISOLATED_PAWN_BONUS=50;
  private static final int[] piece_value={200,300,350,425,850,0};
  private static final int[] piece_value1={2,5,3,4,9,0};
/* The "pcsq" arrays are piece/square tables. They're values
   added to the material value of the piece based on the
   location of the piece. */

private static final int[] pawn_pcsq= {
	  0,   0,   0,   0,   0,   0,   0,   0,
	  0,   0,   0,   0,   0,   0,   0,   0,
	  0,   0,   1,   1,   1,   1,   0,   0,
	 -5,   3,   2,   2,   2,   2,   2,  -5,
	  4,   4,   5,   3,   3,   5,   0,   4,
	  9,   8,  10,   8,   8,  12,   8,   9,
	  10, 15,  15,  15,  15,  15,  15,  10,
	  0,   0,   0,   0,   0,   0,   0,   0
};

private static final int[] knight_pcsq= {
	10,  10,  10,   10,   10,   10,  10,  10,
	10,   0,   0,    0,    0,    0,   0,  10,
	10,   0,  -5,   -5,   -5,   -5,   0,  10,
	10,   0,  -5,  -10,  -10,   -5,   0,  10,
	10,   0,  -5,  -10,  -10,   -5,   0,  10,
	10,   0,  -5,   -5,   -5,   -5,   0,  10,
	10,   0,   0,    0,    0,    0,   0,  10,
	10,  20,  10,   10,   10,   10,  20,  10
};

private static final int[] bishop_pcsq= {
	10,  10,  10,  10,  10,  10,  10,  10,
	10,   0,   0,   0,   0,   0,   0,  10,
	10,   0,  -5,  -5,  -5,  -5,   0, 10,
	10,   0,  -5, -10, -10,  -5,   0, 10,
	10,   0,  -5, -10, -10,  -5,   0, 10,
	10,   0,  -5,  -5,  -5,  -5,   0, 10,
	10,   0,   0,   0,   0,   0,   0, 10,
	10,  10,  20,  10,  10,  20,  10, 10
};

private static final int[] queen_pcsq= {
	-10,  -10,  -10,  -10,  -10,  -10,  -10,  -10,
	0,    -5,   -5,   -5,   -5,   -5,   -5,   0,
	0,    -5,  -15,  -15,  -15,  -15,   -5,   0,
	0,    -5,  -15,  -20,  -20,  -15,   -5,   0,
	0,    -5,  -15,  -20,  -20,  -15,   -5,   0,
	0,    -5,  -15,  -15,  -15,  -15,   -5,   0,
	0,    -5,   -5,   -5,   -5,   -5,   -5,   0,
	0,     0,    0,   20,    0,    0,    0,   0
};
  
  // king heuristic is redundant for antichess
  /**private static final int[] king_pcsq= {
     -40, -40, -40, -40, -40, -40, -40, -40,
     -40, -40, -40, -40, -40, -40, -40, -40,
     -40, -40, -40, -40, -40, -40, -40, -40,
     -40, -40, -40, -40, -40, -40, -40, -40,
     -40, -40, -40, -40, -40, -40, -40, -40,
     -40, -40, -40, -40, -40, -40, -40, -40,
     -20, -20, -20, -20, -20, -20, -20, -20,
     0,  20,  40, -20,   0, -20,  40,  20
     };**/

/* The flip array is used to calculate the piece/square
   values for BLACK pieces. The piece/square value of a
   WHITE pawn is pawn_pcsq[sq] and the value of a DARK
   pawn is pawn_pcsq[flip[sq]] */
private static final int[] flip= {
	 56,  57,  58,  59,  60,  61,  62,  63,
	 48,  49,  50,  51,  52,  53,  54,  55,
	 40,  41,  42,  43,  44,  45,  46,  47,
	 32,  33,  34,  35,  36,  37,  38,  39,
	 24,  25,  26,  27,  28,  29,  30,  31,
	 16,  17,  18,  19,  20,  21,  22,  23,
	  8,   9,  10,  11,  12,  13,  14,  15,
	  0,   1,   2,   3,   4,   5,   6,   7
};
  
/* pawn_leastRow[x][y] is the row of the least advanced pawn of color x on column
   y - 1. There are "buffer files" on the left and right to avoid special-case
   logic later. If there's no pawn on a rank, we pretend the pawn is
   impossibly far advanced (0 for WHITE and 7 for BLACK). This makes it easy to
   test for pawns on a rank and it simplifies some pawn evaluation code.
   It is important to note that last rank for white is 0, whereas for black it is 7.
   This naming is counter-intuitive at first, but all arrays above are represented this way.
   This matrix will be setup when evaluator loads up.
*/
  private static int[][] pawn_leastRow=new int[2][10];
  
  // this array stores isolated pawn information
  private static int[][] pawn_isolated=new int[2][10]; 
  
  private static int[] piece_mat=new int[2];  /* the value of a side's pieces */
  private static int[] pawn_mat=new int[2];  /* the value of a side's pawns */

  
  public static int evaluateBoard(Board board,int colorToMove, int evalQuality)
  {if(evalQuality==0)
    {
      int fixedColumn;  /* column added 1 for convention */
      int[] score=new int [2];  /* each side's score. First entry is white as usual. The higher score for a side, the worse for him according to antichess.*/
    
      /* this is the first pass: set up pawn_leastRow, piece_mat, and pawn_mat. */
      for (int i = 0; i < 10; ++i) {
	pawn_leastRow[Evaluator.WHITE][i] = 0;
	pawn_leastRow[Evaluator.BLACK][i] = 7;
	pawn_isolated[Evaluator.WHITE][i] = 0;
	pawn_isolated[Evaluator.BLACK][i] = 0;
      }
      piece_mat[Evaluator.WHITE] = 0;
      piece_mat[Evaluator.BLACK] = 0;
      pawn_mat[Evaluator.WHITE] = 0;
      pawn_mat[Evaluator.BLACK] = 0;
      // now start assigning real values to these matrixes, initialize them first.
      Piece[][] whitePieces=board.getPiecesOnBoard(Piece.WHITE);
      Piece[][] blackPieces=board.getPiecesOnBoard(Piece.BLACK);
      
      // numbers of each type of each on board
      int numWhitePawns=board.getAliveCount(Piece.WHITE,Piece.PAWN);
      int numWhiteRooks=board.getAliveCount(Piece.WHITE,Piece.ROOK);
      int numWhiteBishops=board.getAliveCount(Piece.WHITE,Piece.BISHOP);
      int numWhiteKnights=board.getAliveCount(Piece.WHITE,Piece.KNIGHT);
      int numWhiteQueens=board.getAliveCount(Piece.WHITE,Piece.QUEEN);
      int numWhiteKings=board.getAliveCount(Piece.WHITE,Piece.KING);
      int numBlackPawns=board.getAliveCount(Piece.BLACK,Piece.PAWN);
      int numBlackRooks=board.getAliveCount(Piece.BLACK,Piece.ROOK);
      int numBlackBishops=board.getAliveCount(Piece.BLACK,Piece.BISHOP);
      int numBlackKnights=board.getAliveCount(Piece.BLACK,Piece.KNIGHT);
      int numBlackQueens=board.getAliveCount(Piece.BLACK,Piece.QUEEN);
      int numBlackKings=board.getAliveCount(Piece.BLACK,Piece.KING);
      int numTotalPieces=numWhitePawns+numWhiteRooks+numWhiteBishops+numWhiteKnights+numWhiteQueens+numWhiteKings+
	numBlackPawns+numBlackRooks+numBlackBishops+numBlackKnights+numBlackQueens+numBlackKings;
	
      // piece arrays of each piece on board
      Piece[] whitePawns=whitePieces[Piece.PAWN];
      Piece[] whiteRooks=whitePieces[Piece.ROOK];
      Piece[] whiteBishops=whitePieces[Piece.BISHOP];
      Piece[] whiteKnights=whitePieces[Piece.KNIGHT];
      Piece[] whiteQueens=whitePieces[Piece.QUEEN];
      Piece[] whiteKings=whitePieces[Piece.KING];
      Piece[] blackPawns=blackPieces[Piece.PAWN];
      Piece[] blackRooks=blackPieces[Piece.ROOK];
      Piece[] blackBishops=blackPieces[Piece.BISHOP];
      Piece[] blackKnights=blackPieces[Piece.KNIGHT];
      Piece[] blackQueens=blackPieces[Piece.QUEEN];
      Piece[] blackKings=blackPieces[Piece.KING];

      // maximum length of these piece arrays on board
      int numMaxWhitePawns=board.getPieceCount(Piece.WHITE,Piece.PAWN);
      int numMaxWhiteRooks=board.getPieceCount(Piece.WHITE,Piece.ROOK);
      int numMaxWhiteBishops=board.getPieceCount(Piece.WHITE,Piece.BISHOP);
      int numMaxWhiteKnights=board.getPieceCount(Piece.WHITE,Piece.KNIGHT);
      int numMaxWhiteQueens=board.getPieceCount(Piece.WHITE,Piece.QUEEN);
      int numMaxWhiteKings=board.getPieceCount(Piece.WHITE,Piece.KING);
      int numMaxBlackPawns=board.getPieceCount(Piece.BLACK,Piece.PAWN);
      int numMaxBlackRooks=board.getPieceCount(Piece.BLACK,Piece.ROOK);
      int numMaxBlackBishops=board.getPieceCount(Piece.BLACK,Piece.BISHOP);
      int numMaxBlackKnights=board.getPieceCount(Piece.BLACK,Piece.KNIGHT);
      int numMaxBlackQueens=board.getPieceCount(Piece.BLACK,Piece.QUEEN);
      int numMaxBlackKings=board.getPieceCount(Piece.BLACK,Piece.KING);
      

      
      for (int i=0; i<numMaxWhitePawns && whitePawns[i]!=null;i++)
      {
	Piece currWhitePawn=whitePawns[i];
	pawn_mat[Evaluator.WHITE] += piece_value[Evaluator.PAWN];
	fixedColumn = currWhitePawn.getCol() + 1;  // add 1 because of the extra column in the array
	if (pawn_leastRow[Evaluator.WHITE][fixedColumn] < (7-currWhitePawn.getRow()))
	  {pawn_leastRow[Evaluator.WHITE][fixedColumn] = 7-currWhitePawn.getRow();}
      }
      for (int i=0; i<numMaxBlackPawns && blackPawns[i]!=null;i++)
	{
	  Piece currBlackPawn=blackPawns[i];
	  pawn_mat[Evaluator.BLACK] += piece_value[Evaluator.PAWN];
	  fixedColumn = currBlackPawn.getCol() + 1;  // add 1 because of the extra column in the array
	  if (pawn_leastRow[Evaluator.BLACK][fixedColumn] > (7-currBlackPawn.getRow()))
	    {pawn_leastRow[Evaluator.BLACK][fixedColumn] = 7-currBlackPawn.getRow();}
	}
      
      piece_mat[Evaluator.WHITE] += piece_value[Evaluator.ROOK]*board.getAliveCount(Piece.WHITE,Piece.ROOK);
      piece_mat[Evaluator.WHITE] += piece_value[Evaluator.BISHOP]*board.getAliveCount(Piece.WHITE,Piece.BISHOP);
      piece_mat[Evaluator.WHITE] += piece_value[Evaluator.KNIGHT]*board.getAliveCount(Piece.WHITE,Piece.KNIGHT);
      piece_mat[Evaluator.WHITE] += piece_value[Evaluator.QUEEN]*board.getAliveCount(Piece.WHITE,Piece.QUEEN);
      piece_mat[Evaluator.BLACK] += piece_value[Evaluator.ROOK]*board.getAliveCount(Piece.BLACK,Piece.ROOK);
      piece_mat[Evaluator.BLACK] += piece_value[Evaluator.BISHOP]*board.getAliveCount(Piece.BLACK,Piece.BISHOP);
      piece_mat[Evaluator.BLACK] += piece_value[Evaluator.KNIGHT]*board.getAliveCount(Piece.BLACK,Piece.KNIGHT);
      piece_mat[Evaluator.BLACK] += piece_value[Evaluator.QUEEN]*board.getAliveCount(Piece.BLACK,Piece.QUEEN);
      
      // The following will be the second run that will take square heuristics into account.
      // Depending on the position of the piece, a new value will be added or subtracted from total score.
      score[Evaluator.WHITE] = piece_mat[Evaluator.WHITE] + pawn_mat[Evaluator.WHITE];
      score[Evaluator.BLACK] = piece_mat[Evaluator.BLACK] + pawn_mat[Evaluator.BLACK];

      /*
	System.out.println ("White score: " + score[Evaluator.WHITE]);
	System.out.println ("White piece score: " + piece_mat[Evaluator.WHITE]);
	System.out.println ("White pawn score: " + pawn_mat[Evaluator.WHITE]);
	System.out.println ("Black score: " + score[Evaluator.BLACK]);
	System.out.println ("Black piece score: " + piece_mat[Evaluator.BLACK]);
	System.out.println ("Black pawn score: " + pawn_mat[Evaluator.BLACK]);
      */
      
    for (int i=0; i<numMaxWhitePawns && whitePawns[i]!=null;i++)
      {// this code can be added to the first pawn for loop above to improve performance. Add it later on.
	Piece currWhitePawn=whitePawns[i];
	score[Evaluator.WHITE] += Evaluator.eval_white_pawn(currWhitePawn);
      }
    // System.out.println ("White score after whitepawn heuristic: " + score[Evaluator.WHITE]);

    for (int i=0; i<numMaxBlackPawns && blackPawns[i]!=null;i++)
      {// this code can be added to the first pawn for loop above to improve performance. Add it later on.
	Piece currBlackPawn=blackPawns[i];
	score[Evaluator.BLACK] += Evaluator.eval_black_pawn(currBlackPawn);
      }
     //System.out.println ("Black score after blackpawn heuristic: " + score[Evaluator.BLACK]);
    
    
     for (int i=0; i<numMaxWhiteRooks && whiteRooks[i]!=null;i++)
       {// add open file, semi-open file, and 7th rank penalties
	 Piece currWhiteRook=whiteRooks[i];
	 int currColumn=currWhiteRook.getCol();
	 int currRow=currWhiteRook.getRow();
	 if (pawn_leastRow[Evaluator.WHITE][currColumn+1]==0)
	   {if (pawn_leastRow[Evaluator.BLACK][currColumn+1]==7)
	     {score[Evaluator.WHITE] += ROOK_OPEN_FILE_PENALTY;}
	   else
	     {score[Evaluator.WHITE] += ROOK_SEMI_OPEN_FILE_PENALTY;}
	   }
	 if (currRow==6)
	   {score[Evaluator.WHITE] += ROOK_ON_SEVENTH_PENALTY;}
       }
     
     for (int i=0; i<numMaxWhiteBishops && whiteBishops[i]!=null;i++)
      {
	Piece currWhiteBishop=whiteBishops[i];
	int currColumn=currWhiteBishop.getCol()+1;
	int fixedRow=7-currWhiteBishop.getRow();
	int sq=Evaluator.returnSquareValue(currWhiteBishop);
	score[Evaluator.WHITE] -= bishop_pcsq[sq];
	if ((pawn_isolated[Evaluator.BLACK][currColumn]==1)
	    && pawn_leastRow[Evaluator.BLACK][currColumn]<fixedRow)
	  {score[Evaluator.WHITE]-= BLOCKED_ISOLATED_PAWN_BONUS;}
      }	   

     for (int i=0; i<numMaxWhiteKnights && whiteKnights[i]!=null;i++)
       {
	 Piece currWhiteKnight=whiteKnights[i];
	 int currColumn=currWhiteKnight.getCol()+1;
	 int fixedRow=7-currWhiteKnight.getRow();
	 int sq=Evaluator.returnSquareValue(currWhiteKnight);
	 score[Evaluator.WHITE] -= knight_pcsq[sq];
	 if ((pawn_isolated[Evaluator.BLACK][currColumn]==1)
	     && pawn_leastRow[Evaluator.BLACK][currColumn]>fixedRow)
	   {score[Evaluator.WHITE]-= BLOCKED_ISOLATED_PAWN_BONUS;}
	 
      }
     for (int i=0; i<numMaxWhiteQueens && whiteQueens[i]!=null;i++)
       {
	 Piece currWhiteQueen=whiteQueens[i];
	 int sq=Evaluator.returnSquareValue(currWhiteQueen);
	 score[Evaluator.WHITE] -= queen_pcsq[sq];
      }
     
     for (int i=0; i<numMaxBlackRooks && blackRooks[i]!=null;i++)
       {// add open file, semi-open file, and 7th rank penalties
	 Piece currBlackRook=blackRooks[i];
	 int currColumn=currBlackRook.getCol();
	 int currRow=currBlackRook.getRow();
	 if (pawn_leastRow[Evaluator.BLACK][currColumn+1]==7)
	   {if (pawn_leastRow[Evaluator.WHITE][currColumn+1]==0)
	     {score[Evaluator.BLACK] += ROOK_OPEN_FILE_PENALTY;}
	   else
	     {score[Evaluator.BLACK] += ROOK_SEMI_OPEN_FILE_PENALTY;}
	   }
	 if (currRow==1)
	   {score[Evaluator.BLACK] += ROOK_ON_SEVENTH_PENALTY;}
       }
     
     for (int i=0; i<numMaxBlackBishops && blackBishops[i]!=null;i++)
      {

	Piece currBlackBishop=blackBishops[i];
	int currColumn=currBlackBishop.getCol()+1;
	int fixedRow=7-currBlackBishop.getRow();
	int sq=Evaluator.returnSquareValue(currBlackBishop);
	score[Evaluator.BLACK] -= bishop_pcsq[flip[sq]];
	if ((pawn_isolated[Evaluator.WHITE][currColumn]==1)
	    && pawn_leastRow[Evaluator.WHITE][currColumn]>fixedRow)
	  {score[Evaluator.BLACK]-= BLOCKED_ISOLATED_PAWN_BONUS;}

      }	   

     for (int i=0; i<numMaxBlackKnights && blackKnights[i]!=null;i++)
       {
	 Piece currBlackKnight=blackKnights[i];
	 int currColumn=currBlackKnight.getCol()+1;
	 int fixedRow=7-currBlackKnight.getRow();
	 int sq=Evaluator.returnSquareValue(currBlackKnight);
	 score[Evaluator.BLACK] -= knight_pcsq[flip[sq]];
	 if ((pawn_isolated[Evaluator.WHITE][currColumn]==1)
	    && pawn_leastRow[Evaluator.WHITE][currColumn]>fixedRow)
	  {score[Evaluator.BLACK]-= BLOCKED_ISOLATED_PAWN_BONUS;}
      }
     
     for (int i=0; i<numMaxBlackQueens && blackQueens[i]!=null;i++)
       {
	 Piece currBlackQueen=blackQueens[i];
	 int sq=Evaluator.returnSquareValue(currBlackQueen);
	 score[Evaluator.BLACK] -= queen_pcsq[flip[sq]];
       }

     // add special move heuristics
     
     if (numTotalPieces<16)
       {if (!board.getSpecial(Piece.WHITE))
	 {score[Evaluator.WHITE] -=SPECIAL_MOVE_BONUS;}
       if (!board.getSpecial(Piece.BLACK))
	 {score[Evaluator.BLACK] -=SPECIAL_MOVE_BONUS;}
       }
  
     // score array is finalized, now return the relative answer depending on turn side
   
     if (colorToMove == Piece.WHITE)
       {return score[Evaluator.BLACK]-score[Evaluator.WHITE] ;}
     else 
       {return score[Evaluator.WHITE] - score[Evaluator.BLACK];}
    }


  if (evalQuality==1)
    {
      // a basic weigthed sum of piece values will be returned at the most basic evaluator
      int oppColor=Piece.getOpponentColor(colorToMove);
      int numSelfPawns=board.getAliveCount(colorToMove,Piece.PAWN);
      int numSelfRooks=board.getAliveCount(colorToMove,Piece.ROOK);
      int numSelfBishops=board.getAliveCount(colorToMove,Piece.BISHOP);
      int numSelfKnights=board.getAliveCount(colorToMove,Piece.KNIGHT);
      int numSelfQueens=board.getAliveCount(colorToMove,Piece.QUEEN);
      int numSelfKings=board.getAliveCount(colorToMove,Piece.KING);
      int numOppPawns=board.getAliveCount(oppColor,Piece.PAWN);
      int numOppRooks=board.getAliveCount(oppColor,Piece.ROOK);
      int numOppBishops=board.getAliveCount(oppColor,Piece.BISHOP);
      int numOppKnights=board.getAliveCount(oppColor,Piece.KNIGHT);
      int numOppQueens=board.getAliveCount(oppColor,Piece.QUEEN);
      int numOppKings=board.getAliveCount(oppColor,Piece.KING);
      int numTotalPieces=numSelfPawns+numSelfRooks+numSelfBishops+numSelfKnights+numSelfQueens+numSelfKings+numOppPawns+numOppRooks+numOppBishops+numOppKnights+numOppQueens+numOppKings;
      
      int result=
	piece_value1[Evaluator.PAWN]*(numOppPawns-numSelfPawns)+
	piece_value1[Evaluator.ROOK]*(numOppRooks-numSelfRooks)+
	piece_value1[Evaluator.KNIGHT]*(numOppKnights-numSelfKnights)+
	piece_value1[Evaluator.BISHOP]*(numOppBishops-numSelfBishops)+
	piece_value1[Evaluator.QUEEN]*(numOppQueens-numSelfQueens);
      
      if (numTotalPieces<16)
	{if (!board.getSpecial(colorToMove))
	  {result++;}
	if (!board.getSpecial(oppColor))
	  {result--;}
	}
      // most basic quality of search should return this sum
      return result;
    }
  
  if (evalQuality==2)
    {
      return board.getDifference(colorToMove);
    }
  
  else
    {return board.getDifference(colorToMove);}
  }
  
  
  /**
     @returns an integer representing the location of the piece on a 8by8 chess board. This value
     is consistant with the 64 integer arrays that are used for piece evaluations. Refer to multiplication
     arrays above. They are written how a 2D chess-board looks like. The first 0-7 entries represent
     squares from a8 to h8, the next 8 entries from a7 to h7 and so on.
     a8 is assigned to 0, h1 is assigned to 63.
  **/
  
  private static int returnSquareValue(Piece xpiece)
  {
    return xpiece.getCol()+(7-xpiece.getRow())*8;
  }  
  
  //@requires whitePawn.getColor()==Piece.WHITE &&whitePawn.getType()==Piece.PAWN
  //@returns the evaluation for the pawn using extra pawn heuristics
  private static int eval_white_pawn(Piece whitePawn){
    
    int returnValue;  // the value to return
    int column;  // the pawn's file(column)
    int row; // the pawn's row
    int sq; // pawn's square on matrix representation as explained above
    returnValue = 0;
    column = whitePawn.getCol()+1;// fixing column value since we are using 10by2 array.
    row =7-whitePawn.getRow();
    sq=Evaluator.returnSquareValue(whitePawn);
    //System.out.println ("eval_white_pawn: sq: " + sq);
    
    // initial value assignment from array lookup
    returnValue -= pawn_pcsq[sq];

    //System.out.println ("eval_white_pawn: returnValue: " + returnValue);
    /* if there's a pawn behind this one, it's doubled
     watch out for reserve representation logic again. */
    if (pawn_leastRow[Evaluator.WHITE][column] > row)
      returnValue += DOUBLED_PAWN_PENALTY;
    //System.out.println ("eval_white_pawn: returnValue after doubled_pawn_penalty: " + returnValue);
    
    
    /* if there aren't any friendly pawns on either side of
       this one, it's isolated. This function is the reason why we have made a 10by2 array.
    */
    if ((pawn_leastRow[Evaluator.WHITE][column - 1] == 0) &&
	(pawn_leastRow[Evaluator.WHITE][column + 1] == 0))
      {returnValue += ISOLATED_PAWN_PENALTY;
      pawn_isolated[Evaluator.WHITE][column] = 1;}
    //System.out.println ("eval_white_pawn: returnValue after isolated_pawn_penalty: " + returnValue);
    
    /* add a penalty if the pawn is passed and is going for queen*/
    if ((pawn_leastRow[Evaluator.BLACK][column - 1] >= row) &&
	(pawn_leastRow[Evaluator.BLACK][column] >= row) &&
	(pawn_leastRow[Evaluator.BLACK][column + 1] >= row))
      returnValue += (7 - row) * PASSED_PAWN_PENALTY;
    //System.out.println ("eval_white_pawn: returnValue after passed_pawn_penalty: " + returnValue);
    
    return returnValue;
  } 
  //@requires blackPawn.getColor()==Piece.BLACK &&blackPawn.getType()==Piece.PAWN
  //@returns the evaluation for the pawn using extra pawn heuristics
  private static int eval_black_pawn(Piece blackPawn)
    {
    int returnValue;  // the value to return
    int column;  // the pawn's file(column)
    int row; // the pawn's row
    int sq; // pawn's square on matrix representation as explained above
    returnValue = 0;
    column = blackPawn.getCol()+1;// fixing to adjust to 10by2 array
    row =7-blackPawn.getRow();
    sq=Evaluator.returnSquareValue(blackPawn);
    //System.out.println ("eval_black_pawn: sq: " + sq);
    // initial value assignment from array lookup
    returnValue -= pawn_pcsq[flip[sq]];
    //System.out.println ("eval_black_pawn: returnvalue: " + returnValue);
    
    /* if there's a pawn behind this one, it's doubled
       watch out for reserve representation logic again. */
    if (pawn_leastRow[Evaluator.BLACK][column] < row)
      returnValue += DOUBLED_PAWN_PENALTY;
    //System.out.println ("eval_black_pawn: returnvalue after doubled_pawn_penalty: " + returnValue);
    
    /* if there aren't any friendly pawns on either side of
       this one, it's isolated. This function is the reason why we have made a 10by2 array.
    */
    if ((pawn_leastRow[Evaluator.BLACK][column - 1] == 7) &&
	(pawn_leastRow[Evaluator.BLACK][column + 1] == 7))
      {returnValue += ISOLATED_PAWN_PENALTY;
      pawn_isolated[Evaluator.BLACK][column] = 1;}
    //System.out.println ("eval_black_pawn: returnvalue after isolated_pawn_penalty: " + returnValue);
    
    /* add a penalty if the pawn is passed and is going for queen*/
    if ((pawn_leastRow[Evaluator.WHITE][column - 1] <= row) &&
	(pawn_leastRow[Evaluator.WHITE][column] <= row) &&
	(pawn_leastRow[Evaluator.WHITE][column + 1] <= row))
      // the closer to final line the worse
      returnValue += row * PASSED_PAWN_PENALTY;
    //System.out.println ("eval_black_pawn: returnvalue after passed_pawn_penalty: " + returnValue);
    
    return returnValue;
    }

}
