/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;
import junit.framework.*;
import java.io.*;
import java.util.*;

public class TestMove
  extends TestCase{
  public TestMove(String name) {
    super(name);
  }

  protected void setUp(){
  }

  public void testMove(){
    Move move;
    int startRow, startCol;
    int destRow, destCol;
    // Create from string
    move = new Move ("d5-h1");
    assertEquals ("row = 4", 4, move.getStartRow());
    assertEquals ("col = 3", 3, move.getStartCol());
    assertEquals ("row = 0", 0, move.getDestRow());
    assertEquals ("col = 7", 7, move.getDestCol());
    assertTrue ("d5-h1", move.toString().equals ("d5-h1"));

    // Create from string
    move = new Move ("d5-k13");
    assertEquals ("row = 4", 4, move.getStartRow());
    assertEquals ("col = 3", 3, move.getStartCol());
    assertEquals ("row = 12", 12, move.getDestRow());
    assertEquals ("col = 10", 10, move.getDestCol());
    assertTrue ("d5-k13", move.toString().equals ("d5-k13"));

    // Create from string
    move = new Move ("n20-k13");
    assertEquals ("row = 19", 19, move.getStartRow());
    assertEquals ("col = 13", 13, move.getStartCol());
    assertEquals ("row = 12", 12, move.getDestRow());
    assertEquals ("col = 10", 10, move.getDestCol());
    assertTrue ("n20-k13", move.toString().equals ("n20-k13"));


    // Create from integer
    move = new Move (3, 1, 7, 5);
    assertEquals ("row = 1", 1, move.getStartRow());
    assertEquals ("col = 3", 3, move.getStartCol());
    assertEquals ("row = 5", 5, move.getDestRow());
    assertEquals ("col = 7", 7, move.getDestCol());
    //System.out.println (move.toString() + "/");
    assertTrue ("d2-h6", move.toString().equals ("d2-h6"));

    // Create from integer
    move = new Move (13, 11, 17, 15);
    assertEquals ("row = 11", 11, move.getStartRow());
    assertEquals ("col = 13", 13, move.getStartCol());
    assertEquals ("row = 15", 15, move.getDestRow());
    assertEquals ("col = 17", 17, move.getDestCol());
    //System.out.println (move.toString());
    assertTrue ("n12-r16", move.toString().equals ("n12-r16"));
    
    // Create from integer
    move = new Move (10, 10, 9, 9);
    assertEquals ("row = 10", 10, move.getStartRow());
    assertEquals ("col = 10", 10, move.getStartCol());
    assertEquals ("row = 9", 9, move.getDestRow());
    assertEquals ("col = 9", 9, move.getDestCol());
    //System.out.println (move.toString());
    assertTrue ("k11-j10", move.toString().equals ("k11-j10"));
    
    // Info

    // only normal is true
    assertTrue ("Normal", move.getNormal());
    assertTrue ("Ep", !move.getEp());
    assertTrue ("Pawn", !move.getPawn());
    assertTrue ("Promote", !move.getPromote());
    assertTrue ("Special", !move.getSpecial());
    assertTrue ("Capture", !move.getCapture());
    assertTrue ("Castle", !move.getCastle());

    move.setEp(true);
    assertTrue ("Ep", move.getEp());
    move.setPawn(true);
    assertTrue ("Pawn", move.getPawn());
    move.setPromote(true);
    assertTrue ("Promote", move.getPromote());
    move.setSpecial(true);
    assertTrue ("Special", move.getSpecial());
    move.setCapture(true);
    assertTrue ("Capture", move.getCapture());
    move.setCastle(true);
    assertTrue ("Castle", move.getCastle());
    assertTrue ("Normal", !move.getNormal());

    move.setCapture(false);
    assertTrue ("Capture", !move.getCapture());
    move.setPromote(false);
    assertTrue ("Promote", !move.getPromote());
    move.setEp(false);
    assertTrue ("Ep", !move.getEp());
    move.setSpecial(false);
    assertTrue ("Special", !move.getSpecial());
    move.setCastle(false);
    assertTrue ("Castle", !move.getCastle());
    move.setPawn(false);
    assertTrue ("Pawn", !move.getPawn());
    assertTrue ("Normal", move.getNormal());
  }


  // Tell JUnit what order to run the tests in
  public static Test suite()
  { 
    TestSuite suite = new TestSuite(); 
    return suite;
  }
}
