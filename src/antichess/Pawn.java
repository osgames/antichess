/*
* AntiChess, an antichess game
*
* Copyright (c) 2003 Hooria Komal, Tamer Karatekin, Hongping Lim and Baris Yuksel
* (For copyright info please see COPYRIGHT file under the base directory)
*
* For bugs, contact info:
* barisy@mit.edu or hongping@mit.edu
*/
package antichess;

import java.util.*;

/**
 * A Pawn piece in antichess.
 **/

public class Pawn extends Piece{

  // CONSTRUCTOR
  /** Creates a new piece.
   * @param letter the character representation of the piece:
   * @param board the board the piece will be assigned to
   * @param row starting row of piece
   * @param col starting col of piece
   * @param color color
   * @requires letter = {P | p | K | k | N | n | R | r | Q | q | B | b}
   **/
  public Pawn (char letter, Board b, int col, int row, int color){
    super (Piece.PAWN, letter, b, col, row, color);

    // These vectors are used in canAttackLocation
    if (color == WHITE){
      int[] myDCol = { -1,  1};
      int[] myDRow = {  1,  1};
      dcol = myDCol;
      drow = myDRow;
    } else if (color == BLACK){
      int[] myDCol = { -1,  1};
      int[] myDRow = {  -1,  -1};
      dcol = myDCol;
      drow = myDRow;
    } else if (color == RED){
      int[] myDCol = {   1,   1};
      int[] myDRow = {  -1,   1};
      dcol = myDCol;
      drow = myDRow;
    } else if (color == BLUE){
      int[] myDCol = {  -1,  -1};
      int[] myDRow = {  -1,   1};
      dcol = myDCol;
      drow = myDRow;
      
    }
    slide = false;
    offsets = 2;
  }

  /**
   * Creates a copy to a new board
   */
  public Piece copyPiece (Board b){
    //System.out.println ("Pawn info: " + this);
    Piece p = new Pawn (this.getChar(), b, this.getCol(), this.getRow(), this.getColor());
    p.setLastLocation (this.getLastCol(), this.getLastRow());
    p.setMoved (this.getMoved());
    return p;
  }

  /** Overrides Piece.getNormalMoves **/
  public void getNormalMoves (List captureMoves, List nonCaptureMoves){
    //System.out.println ("Getting pawn moves");

    captureMoves.addAll(getCaptureMoves());
    nonCaptureMoves.addAll(getNonCaptureMoves());
    
    //System.out.println (captureMoves);
    //System.out.println (nonCaptureMoves);
  }


  /** Returns a list of possible noncapture (not necessarily legal) moves for a Pawn
   * The Moves are not necessarily legal. 
   * Calling code should use inCheck after making a move. <br>
   *  @return list of moves the Pawn can make.
   **/
  private List getNonCaptureMoves (){
    // This checks for the two possible nonCaptureMove a Pawn can make.
    // It can move one step forward if it is not blocked.
    // If it has not moved, and is not blocked, it can move two steps.
    Board b = this.getBoard();
    int startRow = this.getRow();
    int startCol = this.getCol();
    int type = this.getType();
    int color = this.getColor();

    int destRow;
    int destCol;

    int i;
		
    int forwardRow = 0, forwardCol = 0;
    Move m;
    List nonCaptureMoves = new ArrayList();

    color = getColor();

    // Check for color and assign appropriate offsets
    if (color == Piece.WHITE){
      // White. Pawn moves up. 
      forwardRow = 1;
      forwardCol = 0;
    } else if (color == Piece.BLACK){
      // Black. Pawn moves down. 
      forwardRow = -1;
      forwardCol = 0;
    } else if (color == Piece.RED){
      forwardRow = 0;
      forwardCol = 1;
    } else if (color == Piece.BLUE){
      forwardRow = 0;
      forwardCol = -1;
    }

    // Check one step and two steps
    destRow = startRow;
    destCol = startCol;
    for (i=0; i<2; i++){

      destRow += forwardRow;
      destCol += forwardCol;

//       if (destRow > b.MAXROW ||
// 	  destRow < b.MINROW ||
// 	  destCol > b.MAXCOL ||
// 	  destCol < b.MINCOL)
// 	break;
      
      if (b.getBoardType() == b.CYLINDER){
	// Cylinder board: we need to wrap around columns
	if (destCol < b.MINCOL)
	  destCol += b.NUMCOLS;
	if (destCol > b.MAXCOL)
	  destCol -= b.NUMCOLS;
	if (destRow > b.MAXROW ||
	    destRow < b.MINROW ||
	    (destCol == startCol && destRow == startRow))
	  break;
      } else {
	if (destRow > b.MAXROW ||
	    destRow < b.MINROW ||
	    destCol > b.MAXCOL ||
	    destCol < b.MINCOL)
	  break;
      }
      Piece destPiece = b.getPieceAt (destCol, destRow);

      // Check if the destination square is within bounds
      if (destPiece == null)
	break;

      // Check if the destination square is empty
      if (destPiece.getType() != Piece.EMPTY) {
	// If first square is not empty, then we are block
	// and need not check second square
	break; 
      }

      // Add move to captureMoves
      m = new Move(startCol, startRow, destCol, destRow);

      // Check if a promotion
      if ((color == Piece.WHITE && destRow == b.MAXROW) ||
	  (color == Piece.BLACK && destRow == b.MINROW))
	m.setPromote(true);
      // m.setPawn(true); // Let super handle normal move instead

      nonCaptureMoves.add (m);

      // Check if the Pawn has moved before. If so, it cannot move 2 steps
      if (getMoved ())
	break;
    }

    return nonCaptureMoves;
  }

  /** Returns a list of possible capture (not necessarily legal) moves for a Pawn
   * The Moves are not necessarily legal. 
   * Calling code should use inCheck after making a move. <br>
   *  @return list of possible capture moves a Pawn can make
   **/
  private List getCaptureMoves (){
    // This checks the forward diagonal to see if the Pawn has anything
    // to capture. It also checks for the special capture move of 
    // en passant (see below).
    Board b = this.getBoard();
    int startRow = this.getRow();
    int startCol = this.getCol();
    int type = this.getType();
    int color = this.getColor();

    int destRow;
    int destCol;

    int i;
    int opponentStartRow; // for en passant
		
    // Offset vectors
    int captureOffsets = 2;
    int[] captureRowOffset = { 0, 0};
    int[] captureColOffset = { 1,-1};

    Move m;
    int epRow, epCol; // en passant square where opponent pawn is
    int prevRow, prevCol; // previous position of opponent pawn
    Piece epPiece = null, destPiece = null;

    List captureMoves = new ArrayList();


    // check for color and assign appropriate offsets
    if (color == Piece.WHITE){
      // White. Pawn moves up. 
      int[] offset = { 1, 1};
      captureRowOffset = offset;
    } else if (color == Piece.BLACK){
      // Black. Pawn moves down. 
      int[] offset = { -1, -1};
      captureRowOffset = offset;
    } else if (color == Piece.RED){
      int[] roffset = {1, -1};
      int[] coffset = {1, 1};
      captureRowOffset = roffset;
      captureColOffset = coffset;
    } else if (color == Piece.BLUE){
      int[] roffset = {1, -1};
      int[] coffset = {-1, -1};
      captureRowOffset = roffset;
      captureColOffset = coffset;
    }

    // iterate through capture moves
    for (i = 0; i < captureOffsets; i++){

      // offset the starting row and column using the offset vector to
      // get the destination row and column
      destRow = startRow + captureRowOffset[i];
      destCol = startCol + captureColOffset[i];

      // Check if the destination square is within bounds, and get it if so
      //if (!b.isValidLocation (destLocation)){
      //	continue;
      //}

      // Check if a piece is on the destination square
//       if (destCol < b.MINCOL || destCol > b.MAXCOL ||
// 	  destRow < b.MINROW || destRow > b.MAXROW){
// 	//System.out.println ("Pawn capture: destpiece is outof bounds");
//       System.out.println ("startCol " + startCol + " startRow " + startRow);
//       System.out.println ("destCol " + destCol + " destRow " + destRow);
// 	continue;
//       };
      if (b.getBoardType() == b.CYLINDER){
	// Cylinder board: we need to wrap around columns
	if (destCol < b.MINCOL)
	  destCol += b.NUMCOLS;
	if (destCol > b.MAXCOL)
	  destCol -= b.NUMCOLS;
	if (destRow > b.MAXROW ||
	    destRow < b.MINROW ||
	    (destCol == startCol && destRow == startRow))
	  continue;
      } else {
	if (destRow > b.MAXROW ||
	    destRow < b.MINROW ||
	    destCol > b.MAXCOL ||
	    destCol < b.MINCOL)
	  continue;
      }
	  
      destPiece = b.getPieceAt(destCol, destRow);
      if (destPiece == null){
	//System.out.println ("Pawn capture: destpiece is null");
	continue;
      }

      if (destPiece.getType() != Piece.EMPTY){

	// Check if the piece is the same color
	if (destPiece.getColor() == color){
	  // If same color, we are blocked.
	  // Done with this direction as we are blocked
	  // Do nothing.
	} else {
	  // If different color, we can capture.
	  // Add move to captureMoves
	  m = new Move(startCol, startRow, destCol, destRow);
	  m.setCapture(true);

	  // Check promotion
	  if ((color == Piece.WHITE && destRow == b.MAXROW) ||
	      (color == Piece.BLACK && destRow == b.MINROW))
	    m.setPromote(true);
// 	  System.out.println ("startCol " + startCol + " startRow " + startRow);
// 	  System.out.println ("destCol " + destCol + " destRow " + destRow);

	  captureMoves.add (m);
	}

	// As long as there is piece in the diagonal square,
	// that means that the opponent Pawn did not advance two steps
	// in the previous move. Hence we need not check for en passant.
	// We will proceed to the next direction.
	continue;
      }

      // Check for 4player board. IF 4player, no enpassant
      if (this.getBoard().getBoardType() == Board.FOURPLAYERS){
	continue;
      }
      // En Passant

      // Check if we are in the correct row
      // opponentStartRow is the initial starting row of the opponent Pawn

//       if (color == Piece.WHITE && startRow == 5){
// 	opponentStartRow = 7;

//       } else if (color == Piece.BLACK && startRow == 4){
// 	opponentStartRow = 2;

      //System.out.println ("Testing EP");
      if (color == Piece.WHITE && startRow == 4){
	opponentStartRow = 6;

      } else if (color == Piece.BLACK && startRow == 3){
	opponentStartRow = 1;
      } else {
	// otherwise skip
	continue;
      }

      // epSq is the square on which the opponent Pawn is
      epRow = startRow;
      epCol = destCol;
      //System.out.println ("Getting eppiece at " + epCol + " " + epRow);

      // Check if a piece is on the ep square
      epPiece = b.getPieceAt (epCol, epRow);
			
      // Check location is valid
      if (epPiece == null) {
	//System.out.println ("eppiece is null");
	continue;
      }

      // Check square is not empty
      if (epPiece.getType() == Piece.EMPTY){
	//System.out.println ("eppiece is empty");
	continue;
      }

      // Check if that piece is the same color
      if (epPiece.getColor() == color) continue;

      // Check if it is a Pawn on the epSq
      if (epPiece.getType () != Piece.PAWN) continue;
				
      // If it is an opponent Pawn, we will check that it was the last piece moved, and that 
      // it moved from its original position to the square using two-step advance. 
      // To do this, we check the following:
      // 1)the opponent Pawn was the last piece moved
      // 2)its last position is its original position (2nd or 7th rank)
      // 3)that last position is empty (if not, it would have been a switch)

      // Check if the opponent Pawn was the last piece moved

      Piece lastPiece = b.getLastPieceMoved(epPiece.getColor());
      if (lastPiece == null)
	continue;

      if (lastPiece.getType() == Piece.EMPTY)
	continue;
      //System.out.println ("Last piece moved: " + lastPiece);
      //System.out.println ("EP piece: " + epPiece);
      
      /******************************************************/
      /************!!!!!***************************************/
      /******************************************************/
      // NEED TO GET LAST PIECE MOVED FROM BOARD

      if (!lastPiece.equals (epPiece))
	continue;

      // Get the previous square of the opponent Pawn
      prevRow = epPiece.getLastRow();
      prevCol = epPiece.getLastCol();

      // Check that the previous square is the initial starting position
      if (prevRow != opponentStartRow)
	// If it didnt come from the starting position, then enpassant not possible
	continue;

      // Check that the initial position is empty
      Piece prevPiece = b.getPieceAt (prevCol, prevRow);
      // If it is not empty, then enpassant is not possible, we're done
      //	continue;
      if (prevPiece.getType() != Piece.EMPTY)
	continue;

      // Add en passant move to captureMoves
      m = new Move(startCol, startRow, destCol, destRow);
      m.setEp(true);
      captureMoves.add (m);
    }

    return captureMoves;
  }

  /** Overrides Piece.makeOtherMove. Implements 2-step pass or En Passant or Promotion**/
  public void makeOtherMove (Move move){
    boolean debug = false;
    if (debug) System.out.println ("Making other move in pawn");
    
    if (move.getEp()){
      makeEpMove(move); // EP
    } else if (move.getPromote()){
      makePromoteMove (move); // Promote
    } else {

      throw new RuntimeException ("Pawn.makeOtherMove: super should handle this");
      
      // Normal move for pawn
      // Make a backup in move to enable undo
      
//       move.resetModifiedPieceCount();
//       move.setLastPiece (this.getBoard().getLastPieceMoved(this.getColor()), this.getColor());
//       move.setLastPiece (this.getBoard().getLastPieceMoved(this.getOpponentColor()), this.getOpponentColor());
//       move.addModifiedPiece (this);
//       move.addModifiedPiece (this.getBoard().getPieceAt(move.getDestCol(), move.getDestRow()));
      
//       swapPieces (move.getDestCol(), move.getDestRow()); // 2-step
    }
    
    if (debug) System.out.println ("Done Making other move in pawn");
  }
  
  /** Overrides Piece.undoOtherMove **/
  public void undoOtherMove (Move move){
    
    if (move.getEp()){
      undoEpMove(move); // EP
    } else if (move.getPromote()){
      //undoPromoteMove (move); // Promote
      throw new RuntimeException ("Pawn.undoOtherMove: Queen should handle undoPromote");
    } else {
      throw new RuntimeException ("Pawn.undoOtherMove: super should handle this");
    }
  }
      

  /** Makes an en passant capture **/
  private void makeEpMove (Move move){
    // To make ep move, we create Empty over Captured,
    // and swap Capturing Pawn with Destination Empty
    // For undo, we keep reference to Captured, and Capturing Pawn info
    // To undo, we swap Capturing Pawn with Empty, and register Captured Pawn,
    // and restore Capturing Pawn info
    boolean debug = false;
    if (debug) System.out.println ("Making ep move");
    int startRow = move.getStartRow();
    int startCol = move.getStartCol();
    int destRow = move.getDestRow();
    int destCol = move.getDestCol();
    Board b = this.getBoard();
		
    int epRow = startRow;
    int epCol = destCol;

    // Keep track of backup information for undo move
    // 1. Store last pieces info
    move.setLastPieces (this.getBoard());

    // 2. Store piece info for Capturing Pawn
    move.storePieceInfo (this, 0);

    // 3. Keep reference to Captured pawn
    Piece p2 = this.getBoard().getPieceAt(epCol, epRow);
    move.modifiedPieces[0] = p2;
    
    // Remove opponent pawn
    Piece emptyPiece = new EmptyPiece(b, epCol, epRow);
    b.setPieceAt (epCol, epRow, emptyPiece);
    emptyPiece.setLocation (epCol, epRow);

    // 4. Advance pawn
    swapPieces (destCol, destRow);

    if (debug) System.out.println ("Done ep move");
    if (debug) System.out.println (b);
  }

  /** Undos en passant Move **/
  private void undoEpMove (Move move){
    int startRow = move.getStartRow();
    int startCol = move.getStartCol();
    int destRow = move.getDestRow();
    int destCol = move.getDestCol();
    Board b = this.getBoard();
		
    int epRow = startRow;
    int epCol = destCol;

    // 4. Swap back pawn to original square
    swapPieces (startCol, startRow);


    // 3. Register captured pawn
    b.registerPiece (epCol, epRow, move.modifiedPieces[0]);

    // 2. Restore capturing pawn pieceinfo
    move.restorePieceInfo (this, 0);
    
    // 1. restore last piece info
    move.restoreLastPieces (b);
    
  }

  /** Makes a noncapture promotion**/
  private void makePromoteMove (Move move){
    // To make a noncapture promote, create Queen over Pawn,
    // and swap Queen and Pawn.
    // For undo, keep reference to Pawn.
    // To undo, swap Queen and Empty, and register Pawn .
    boolean debug = false;

    int startRow = move.getStartRow();
    int startCol = move.getStartCol();
    int destRow = move.getDestRow();
    int destCol = move.getDestCol();
    Board b = this.getBoard();
    int color = this.getColor();
    
    // Keep track of backup information for undo move
    // 1. Store last pieces info
    move.setLastPieces (this.getBoard());

    // 2. Keep reference to pawn
    move.modifiedPieces[0] = this;
    

    // Creates a new queen at start location
    Piece queenPiece = new Queen((this.getColor()==WHITE ? 'Q' : 'q'),
				 b, startCol, startRow,
				 color);
    // queenPiece.setLocation (startCol, startRow);
    // getBoard().setPieceAt (startCol, startRow, queenPiece);

    // Zobrist delete pawn
    Zobrist z = b.getZobrist();
    z.deletePiece (b, Piece.PAWN, color, startCol, startRow);
    
    // Zobrist add queen
    z.addPiece (b, Piece.QUEEN, color, startCol, startRow);

    // 3. Make the queen go into dest location
    queenPiece.swapPieces(destCol, destRow);

    

  }

  /** Undos a noncapture promotion **/
  /** Undo queen promotion is implemented in Queen.java**/
  /********************************************************/

  
  /** Makes a capture move & checks if it is also a promotion.
   * Overrides Piece.makeCaptureMove
   * @modifies this && this.board
   * @effects this.board is updated
   **/
  public void makeCaptureMove (Move move){
    if (move.getPromote()){
      int startRow = move.getStartRow();
      int startCol = move.getStartCol();
      int destRow = move.getDestRow();
      int destCol = move.getDestCol();
      int color = this.getColor();
      Board b= this.getBoard();

      // To make capture promotion, create Queen over Pawn,
      // create Empty over Captured,
      // swap Empty and Queen
      // For undo, keep reference to Pawn and Captured
      // To undo, register Pawn and Captured.
      
      // Keep track of backup information for undo move
      // 1. Store last pieces info
      move.setLastPieces (this.getBoard());

      // 2. Keep reference to pawn
      move.modifiedPieces[0] = this;

      // 3. Keep reference to captured
      Piece p2 = b.getPieceAt(destCol, destRow);
      move.modifiedPieces[1] = p2;

      // Remove the capture piece first
      // Creates a dummy piece at the location being captured
      Piece dummy = null;
      //System.out.println ("Creating empty piece");
      dummy = new EmptyPiece(b, destCol, destRow);
      
      // Sets the dummy piece to be at the target location
      //System.out.println ("Setting location of emptypiece");
      getBoard().setPieceAt (destCol, destRow, dummy);
      dummy.setLocation (destCol, destRow);

      // Create queen over pawn
      Piece queenPiece = new Queen((this.getColor()==WHITE ? 'Q' : 'q'),
				   b, startCol, startRow,
				   this.getColor());


      // Zobrist delete pawn
      Zobrist z = b.getZobrist();
      z.deletePiece (b, Piece.PAWN, color, startCol, startRow);
      
      // Zobrist add queen
      z.addPiece (b, Piece.QUEEN, color, startCol, startRow);

      // Zobrist delete captured
      z.deletePiece (b, p2.getType(), p2.getColor(), destCol, destRow);

      
      // Make the queen go into dest location
      queenPiece.swapPieces(destCol, destRow);
      
    }
    else {
      // Normal capture
      super.makeCaptureMove (move);
    }
  }
  
  /** undo capture promotion move is implemented in Queen **/
  /************************************************************/

  /** undos a normal pawn capture move
  /* * Overrides Piece.undoCaptureMove.
   **/
  public void undoCaptureMove (Move move){
    super.undoCaptureMove (move);
  }
      
      
}
